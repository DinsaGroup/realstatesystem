<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AirConditionerType extends Model
{
    protected $tables = 'air_conditioner_types';

    protected $fillable = ['type', 'visible', 'created_by', 'updated_by'];

    public function status()
    {
    	return $this->belongsTo('App\Status');
    }

    public function property()
    {
    	return $this->belongsTo('App\Property');
    }
}
