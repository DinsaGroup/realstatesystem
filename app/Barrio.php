<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barrio extends Model
{
    protected $tables = 'barrios';

    protected $fillable = ['my_id', 'name', 'municipio_id','latitude','longitude'];

    public function municipio() {
    	return $this->belongsTo('App\Municipio', 'my_id', 'id');
    }

    public function property() {
    	return $this->belongsTo('App\Property');
    }
}
