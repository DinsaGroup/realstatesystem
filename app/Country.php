<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $tables = 'countries';

    protected $fillable = [ 'name'];

    public function departments() {
    	return $this->hasMany('App\Department');
    }
}
