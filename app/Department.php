<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $tables = 'departments';

    protected $fillable = ['my_id', 'name', 'country_id'];

    public function country() {
    	return $this->belongsTo('App\Country');
    }

    public function municipios() {
    	return $this->hasMany('App\Municipio');
    }
}
