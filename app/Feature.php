<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $tables = 'features';

    protected $fillable = [ 'name','description','icon', 'visible', 'created_by', 'updated_by'];

    public function properties(){
    	return $this->belongsToMany('App\Property');
    }

    public function featureproperty(){
    	return $this->hasMany('App\featureproperty');
    }

}
