<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureProperty extends Model
{
    protected $table = 'feature_property';

    // protected $primaryKey = ['property_id', 'feature_id'];

    protected $fillable = ['property_id','feature_id','valor','descriptor'];

    public function property(){
    	return $this->belongsTo('App\Property','property_id','id');
    }

    public function feature(){
    	return $this->belongsTo('App\Feature','feature_id','id');
    }
}