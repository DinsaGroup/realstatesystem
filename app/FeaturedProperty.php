<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeaturedProperty extends Model
{
    protected $table = 'featured_properties';

    protected $fillable = ['property_id','start','end','visible','created_by','updated_by'];

    public function property(){
    	return $this->belongsTo('App\Property');
    }
}
