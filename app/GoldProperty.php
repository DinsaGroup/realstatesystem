<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoldProperty extends Model
{
    protected $tables = 'gold_properties';

    protected $fillable = ['property_id','start','end','rentPrice','soldPrice','status','visible','created_by','updated_by'];

    public function property(){
    	return $this->belongsTo('App\Property');
    }

}
