<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Property;
use App\FeaturedProperty;
use App\GoldProperty;
use App\Testimonial;
use App\PropertyType;
use App\User;
use App\FeatureProperty;
use App\Sign;

use Carbon\Carbon;

use Session;

use Mail;

class AppController extends Controller
{

    public function index(){
        $today = Carbon::now();
        $featured = FeaturedProperty::where('start','<=',$today)->where('end','>=',$today)->where('visible',true)->get();
        $last_properties = Property::where('published',true)->where('visible',true)->where('disponible',true)->orderBy('id','desc')->get()->take(8);
        $gold = GoldProperty::where('start','<',$today)->where('end','>',$today)->where('visible',true)->get();
        $testimonials = Testimonial::where('visible',true)->get();
        $value = true;
        $value2 = true;
        $property_types = PropertyType::where('visible','true')->get()->pluck('name','id');
        $property_types_menu = PropertyType::where('visible','true')->get();

        if(count($featured)<4){
            $number = count($featured);
        }else{
            $number = 4;
        }

        // Validamos si existen propiedades oro, en caso contrario, tomamos propiedades de la lista.
        if(count($gold) == 0 ) {
           $gold = Property::where('published',true)->where('visible',true)->where('disponible',true)->orderBy('id','desc')->get()->take(10);
           $value2 = false;
        }

        if(count($testimonials)==0){
            $testimonials = false;
        }else{
            $testimonials = $testimonials->random(1);
        }


        // Reiniciar todas las variables de session.
        Session::put('value',null);
        Session::put('property_id',null);
        Session::put('ventaRenta',null);
        Session::put('desde',null);
        Session::put('hasta',null);
        Session::put('code',null);

        return view('welcome',[
            'title'=>'Momotombo Real Estate: Compra y Alquila Casas, Oficinas y Más',
            'metadesc'=>'Momotombo Real Estate es la mejor opción en venta y alquiler de inmuebles en toda Nicaragua. Con más de 6,300 propiedades en lista y más de 12 años de experiencia, te mostraremos todo lo que la tierra de lagos y volcanes tiene para ofrecer. ',
            'featured_list' => $value,
            'featured' => $featured->random($number),
            'gold_list' => $value2,
            'gold' => $gold->random(1),
            'testimonials' => $testimonials,
            'last_properties' => $last_properties,
            'property_types' => $property_types,
            'property_types_menu' => $property_types_menu,
            ]);
    }

    public function aboutus(){
        $property_types = PropertyType::where('visible','true')->get()->pluck('name','id');
        $property_types_menu = PropertyType::where('visible','true')->get();
        return view('aboutus',[
            'title'=>'Quiénes Somos',
            'metadesc'=>'Momotombo Real Estate inició en Nicaragua en el año 2005 con el compromiso de contribuir al desarrollo de nuestro país ofreciendo al mercado de bienes raíces una forma diferente de ofertar y adquirir propiedades. Somos una empresa dinámica y ansiosa de retos que ofrece a nuestros clientes la mejor opción en el mercado de bienes inmuebles.',
            'property_types' => $property_types,
            'property_types_menu' => $property_types_menu,
        ]);
    }

    public function contactar(){
        $property_types = PropertyType::where('visible','true')->get()->pluck('name','id');
        $property_types_menu = PropertyType::where('visible','true')->get();
        return view('contactus',[
            'title'=>'Contartar',
            'metadesc'=>'',
            'property_types' => $property_types,
            'property_types_menu' => $property_types_menu,
        ]);
    }

    public function propiedades(){
        $property_types = PropertyType::where('visible','true')->get()->pluck('name','id');
        $property_types_menu = PropertyType::where('visible','true')->get();
        return view('propiedades',[
            'title'=>'Propiedades',
            'metadesc'=>'',
            'property_types' => $property_types,
            'property_types_menu' => $property_types_menu,
        ]);
    }

    public function property($code){

        try{

            $property = Property::where('code',$code)->first();
            $property_types_menu = PropertyType::where('visible','true')->get();

            if(!$property){
                \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
                return redirect('/');
            }

            $valor = FeatureProperty::whereNotNull('valor')->get();

            $properties = Property::where('property_type_id',$property->property_type_id)
                          ->Where('sector',$property->sector)
                          ->where('city',$property->city)
                          ->where('state',$property->state)
                          ->where('visible',true)
                          ->where('published',true)->get();


            $property_types = PropertyType::where('visible','true')->get();

            $min= 2; // Radio de un kilometro
            $near = 4; // Radio maximo de busqueda

            $resultado = array(); // Guardamos las propiedades segun el radio minimo
            $cerca = array(); // Guardamos las propiedades cerca

           // Buscar todas la propiedades cerca
            foreach ($properties as $item) {

                    // Get distance between my position and office position
                    $distancia = $this->getDistanceToCoords($property->latitude, $property->longitude, $item->latitude, $item->longitude);

                    if($distancia < $min){
                        $resultado[] = $item->id;
                    }

                    if($min < $distancia and $distancia < $near){
                        $cerca[] = $item->id;
                    }


            }

            $similars = Property::whereIn('id',$resultado)->get()->take(12);


            if($property){
                $description = '';
                if($property->description!=''){
                    $array = explode('.', $property->description);
                    $start = true;
                    for($i=0;$i<count($array);$i++){
                        if($start){
                            $description = ucfirst(trim($array[$i]));
                            $start = false;
                        }else{
                            $description = $description.'. '.ucfirst(trim($array[$i]));
                        }
                    }
                }

                $show = false;
                foreach ($property->photos as $item) {
                    if($item->visible == 'true'){
                        $show = true;
                    }
                }
                $property_types = PropertyType::where('visible','true')->get()->pluck('name','id');
                return view('property',[
                        'title' => 'Propiedad '.$property->code.' | Momotombo Real Estate',
                        'metadesc'=>'',
                        'property' => $property,
                        'description' => $description,
                        'valor' => $valor,
                        'similars' => $similars,
                        'show' => $show,
                        'property_types' => $property_types,
                        'property_types_menu' => $property_types_menu,
                    ]);

            }else{
                return redirect('/');
            }


         } catch(Exception $e) {
            \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function featured_property(){

        try{
            $today = Carbon::now();

            $featured = FeaturedProperty::where('start','<=',$today)->where('end','>=',$today)->get();
            $property_types = PropertyType::where('visible','true')->get()->pluck('name','id');
            $property_types_menu = PropertyType::where('visible','true')->get();

            return view('featured_property',[
                    'title' => 'Propiedades Destacadas',
                    'metadesc'=>'',
                    'featured' => $featured,
                    'property_types' => $property_types,
                    'property_types_menu' => $property_types_menu,
                ]);

         } catch(Exception $e) {
            \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function gold_property(){

        try{

            $today = Carbon::now();

            $featured = GoldProperty::where('start','<=',$today)->where('end','>=',$today)->get();

            $property_types = PropertyType::where('visible','true')->get()->pluck('name','id');
            $property_types_menu = PropertyType::where('visible','true')->get();

            return view('gold_property',[
                    'title' => 'Propiedades en Ganga',
                    'metadesc'=>'',
                    'featured' => $featured,
                    'property_types' => $property_types,
                    'property_types_menu' => $property_types_menu,
                ]);

         } catch(Exception $e) {
            \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function find_properties(Request $request){


        if($request->value!=""){
            $value = strtolower( trim( $request->value ) );
            Session::put('value',$value);
            Session::put('property_id','');
            Session::put('ventaRenta','venta/renta');
            Session::put('desde',null);
            Session::put('hasta',null);

        }else{
            $value = Session::get('value');
        }

        $code = strtoupper( $value );

        $property_types = PropertyType::where('visible','true')->get()->pluck('name','id');
        $property_types_menu = PropertyType::where('visible','true')->get();

        $properties = Property::whereHas(
                            'sign', function($q) use ($code){
                                $q->where('code', 'like' ,'%'.$code.'%');

                            }
                        )
                    ->where('published',true)
                    ->where('estadoTmp','disponible')
                    ->orWhere('code','like','%'.$code.'%')
                    ->orWhere('address','like','%'.$value.'%')
                    ->orWhere('sector','like','%'.$value.'%')
                    ->orWhere('city','like','%'.$value.'%')
                    ->orWhere('state','like','%'.$value.'%')
                    ->paginate(36);

        $property_types[''] = 'Todas';


         return view('search_result',[
                    'title' => 'Resultado de Busqueda',
                    'metadesc'=>'',
                    'properties' => $properties,
                    'property_types' => $property_types,
                    'property_types_menu' => $property_types_menu,
                    'sector' => strtolower( trim( $request->value ) ),
                    'value' => $value,
                ]);
    }

    public function filtrar_resultado(request $request){

        $property_types = PropertyType::where('visible','true')->get()->pluck('name','id');
        $property_types_menu = PropertyType::where('visible','true')->get();
        $property_types[''] = 'Todas';

        if($request->buscar){

            if($request->auto){
                $full_value = $request->auto;
                $string = explode(',',$full_value);
                $value = strtolower($string[0]);
                Session::put('value', $value);
            }else{
                 Session::put('value',null);
                 $value = '';
            }

            if($request->venta_renta){
                $ventaRenta = $request->venta_renta;
                Session::put('ventaRenta',$ventaRenta);
            }else{
                Session::put('ventaRenta',null);
                $ventaRenta = '';
            }

            if($request->property_type_id){
                $property_type_id = $request->property_type_id;
                Session::put('property_type_id', $property_type_id);
            }else{
                Session::put('property_type_id',null);
                $property_type_id = '';
            }

            if($request->desde){
                $desde = str_replace(',', '',$request->desde);
                Session::put('desde',$desde);
            }else{
                 Session::put('desde',null);
                 $desde = '';
            }

            if($request->hasta){
                $hasta = str_replace(',', '',$request->hasta);
                Session::put('hasta',$hasta);
            }else{
                 Session::put('hasta',null);
                 $hasta = '';
            }

            if($request->code){
                $code = strtoupper($request->code);
                Session::put('code',$code);
            }else{
                Session::put('code',null);
                $code = '';
            }
        }else{
            $value = Session::get('value');
            $ventaRenta = Session::get('ventaRenta');
            $property_type_id = Session::get('property_type_id');
            $desde = Session::get('desde');
            $hasta = Session::get('hasta');
            $code = Session::get('code');
        }

        if($property_type_id ==''){
            $properties_types = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30);
        }else{
            $properties_types = array($property_type_id);
        }

        if($ventaRenta==''){
            $ventaRenta_select = array('venta','renta','venta/renta');
        }else{
            $ventaRenta_select = array($ventaRenta);
        }

        if($desde!=''){ $from = $desde; } else{ $from =0 ;}
        if($hasta!=''){ $to = $hasta;} else{ $to =100000000 ;}
        // dd($to);

        
        if($request->code){

            $code = strtoupper($request->code);

            $properties = Property::whereHas(
                            'sign', function($q) use ($code){
                                $q->where('code', 'like' ,'%'.$code.'%');

                            }
                        )
                    ->orWhere('code','like','%'.$code.'%')
                    ->where('published',true)
                    ->where('estadoTmp','disponible')
                    ->orderBy('id','desc')
                    ->paginate(36);

        }else{


            if($value==''){
                $properties = Property::WhereIn('property_type_id',$properties_types)
                                ->whereIn('ventaRenta',$ventaRenta_select)
                                ->where(function ($query) use ($from,$to){
                                    $query->whereBetween('precioVenta',[$from,$to])
                                            ->orWhere(function ($query2) use($from,$to){
                                                $query2->whereBetween('precioAlquiler',[$from,$to]);
                                            });
                                })
                                ->where('published',true)
                                ->where('estadoTmp','disponible')
                                ->orderBy('id','desc')
                                ->paginate(36);
            }else{
                
                $properties = Property::WhereIn('property_type_id',$properties_types)
                                ->whereIn('ventaRenta',$ventaRenta_select)
                                ->where(function ($query) use ($from,$to){
                                    $query->whereBetween('precioVenta',[$from,$to])
                                            ->orWhere(function ($query2) use($from,$to){
                                                $query2->whereBetween('precioAlquiler',[$from,$to]);
                                            });
                                })
                                ->Where(function ($query) use ($value){
                                    $query->Where('sector','like','%'.$value.'%')
                                            ->orWhere('city','like','%'.$value.'%')
                                            ->orWhere('state','like','%'.$value.'%');
                                })
                                ->where('published',true)
                                ->where('estadoTmp','disponible')
                                ->orderBy('id','desc')
                                ->paginate(36);

            }

        }

        return view('search_result',[
                    'title' => 'Resultado de Busqueda',
                    'metadesc'=>'',
                    'properties' => $properties,
                    'property_types' => $property_types,
                    'property_types_menu' => $property_types_menu,
                    'sector' => strtolower( trim( $request->value ) ),
                    'value' => $value,
                ]);
    }

    public function all_propiedades($slug){

        $property_type = PropertyType::where('slug',$slug)->first();
        $property_types = PropertyType::where('visible','true')->get()->pluck('name','id');
        $property_types_menu = PropertyType::where('visible','true')->get();

        $properties = Property::where('property_type_id',$property_type->id)
                        ->where('published',true)
                        ->where('estadoTmp','disponible')
                        ->orderBy('id','desc')
                        ->paginate(36);

        return view('search_result',[
                    'title' => $property_type->name.' | Momotombo Real Estate',
                    'metadesc'=>'',
                    'properties' => $properties,
                    'property_types' => $property_types,
                    'property_types_menu' => $property_types_menu,
                ]);
    }

    public function find_properties_google(Request $request){
        try{

            // Ajustes de departamentos segun parametros de google

            if($request->auto){

                $city = explode(',', $request->auto);
                $lenght = count($city);

                if($lenght>3){
                    $ciudad = trim($city[$lenght-2]);
                    $sector = trim($city[0]);
                    Session::put('ciudad', $ciudad);
                    Session::put('sector', $sector);
                }else{
                    $ciudad = trim($city[0]);
                    $sector = trim($city[0]);
                    Session::put('ciudad', $ciudad);
                    Session::put('sector', $sector);
                }

            }else{
                if(Session::has('ciudad') && Session::has('sector')){
                    $ciudad = Session::get('ciudad');
                    $sector = Session::get('sector');
                }else{
                    return redirect('/');
                }
            }

            $properties = Property::where('city','like', '%'. strtolower( $ciudad ).'%' )
                         ->orWhere('sector','like', '%'.strtolower ( $sector ).'%' )
                         ->where('visible',true)
                         ->where('published',true)
                         ->paginate(28);

            // $properties = Property::where('visible',true)->where('published',true)->get();
            $property_types = PropertyType::where('visible','true')->get();

            $min = 1; // Radio de un kilometro
            $near = 2; // Radio maximo de busqueda


            $resultado = array(); // Guardamos las propiedades segun el radio minimo
            $cerca = array(); // Guardamos las propiedades cerca
            $distancias = array();

           // Buscar todas la propiedades cerca
            foreach ($properties as $item) {

                    // Get distance between my position and office position
                    $distancia = $this->getDistanceToCoords($request->latitude, $request->longitude, $item->latitude, $item->longitude);
                    // dd($distancia);

                    $distancias[] = $distancia;

                    if($distancia < $min){
                        $resultado[] = $item->id;
                    }

                    if($min < $distancia and $distancia < $near){
                        $cerca[] = $item->id;
                    }

            }

            // $properties = Property::whereIn('id',$resultado)->paginate(28);
            $properties_near = Property::whereIn('id',$cerca)->get()->take(8);

            return view('search_result',[
                    'title' => 'Resultado de Busqueda',
                    'metadesc'=>'',
                    // 'featured' => $featured,
                    'properties' => $properties,
                    'properties_near' => $properties_near,
                    'property_types' => $property_types,
                ]);

         } catch(Exception $e) {
            \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function email(Request $request){
        try {
            $user = User::find($request->agent_id);
            $client_name = $request->name;
            $client_email = $request->email;
            $client_phone = $request->phone;
            $client_message = $request->message;
            $property_code = $request->property_code;

            Mail::send('email.agent_notification', [
                'user' => $user,
                'client_name' => $client_name,
                'client_email' => $client_email,
                'client_phone' => $client_phone,
                'client_message' => $client_message,
                'property_code' => $property_code,
            ], function($m) use($user, $client_name, $client_email) {
                $m->from($client_email, $client_name);
                $m->to('info@momotomborealestate.com');
                // $m->to('catoruno@gmail.com');
            });
            \Session::flash('success_message', '"¡En breves instantes un agente se comunicará con usted!"');
            return redirect()->back();

        } catch(Exception $e) {
            \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function request_information(Request $request){
        try {

            $client_name = $request->name;
            $client_email = $request->email;
            $client_phone = $request->phone;
            $client_message = $request->message;
            $property_type = PropertyType::find($request->property_id);

            Mail::send('email.contact_notification', [
                'message_type' => $request->message_type,
                'client_name' => $client_name,
                'client_email' => $client_email,
                'client_phone' => $client_phone,
                'client_message' => $client_message,
                'property_type' => $property_type->name,

            ], function($m) use($client_name, $client_email) {
                $m->from($client_email, $client_name);
                $m->to('info@momotomborealestate.com');
                // $m->to('catoruno@gmail.com');
            });
            \Session::flash('success_message', '"¡En breves instantes un agente se comunicará con usted!"');
            return redirect()->back();

        } catch(Exception $e) {
            \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function getDistanceToCoords($lat1, $lon1, $lat2, $lon2){

        $R = 6371; // Radius of the earth in km
        $dLat = deg2rad($lat2 - $lat1);  // deg2rad below
        $dLon = deg2rad($lon2 - $lon1);
        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        $d = $R * $c; // Distance in km

        return $d;
    }

    public function sitemap(){
        return file ('sitemap.xml');
    }

}
