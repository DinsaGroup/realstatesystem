<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Sign;
use App\Property;
use App\Testimonial;
use App\FeatureProperty;
use App\Pool;
use App\Room;
use App\AirCondionerType;
use App\PropertyType;
use Auth;
use Input;

class AutoCompleteController extends Controller
{
	public function propertyId() {

        $property_id = Input::get('term');
        $results = array();

        $properties = Property::where('id',$property_id)->limit(10)->get();

        foreach ($properties as $item) {
            $results[] = [
                'value' => $item->id,
            ];
        }

        return response()->json($results);
    }

    public function codigo() {

        $codigo = strtoupper( Input::get('term') );
        $results = array();

        $codes = Property::where('code','like','%'.$codigo.'%')->limit(10)->get();

        foreach ($codes as $item) {
            $results[] = [
                'value' => $item->code,
            ];
        }

        return response()->json($results);
    }

    public function rotulo() {

        $rotulo = strtoupper( Input::get('term') );
        $results = array();

        $codes = Sign::where('code','like','%'.$rotulo.'%')->limit(10)->get();

        foreach ($codes as $item) {
            $results[] = [
                'value' => $item->code,
            ];
        }

        return response()->json($results);
    }

    public function categoria() {

        $categoria = ucfirst( Input::get('term') );
        $results = array();

        $codes = PropertyType::where('name','like','%'.$categoria.'%')->limit(10)->get();
        $results[] = $categoria;
        foreach ($codes as $item) {
            $results[] = [
                'value' => $item->name,
            ];
        }

        return response()->json($results);
    }

    public function sector() {

        $sector = strtolower( Input::get('term') );
        $results = array();

        $codes = Property::where('sector','like','%'.$sector.'%')->limit(10)->get();

        $results[] = $sector;

        foreach ($codes as $item) {
            $results[] = [
                'value' => $item->sector,
            ];
        }

        return response()->json($results);
    }

    public function ciudad() {

        $ciudad = strtolower( Input::get('term') );
        $results = array();

        $codes = Property::where('city','like','%'.$ciudad.'%')->limit(10)->get();
        $results[] = $ciudad;
        foreach ($codes as $item) {
            $results[] = [
                'value' => ucwords($item->city),
            ];
        }

        return response()->json($results);
    }

    public function departamento() {

        $departamento = strtolower( Input::get('term') );
        $results = array();

        $codes = Property::where('state','like','%'.$departamento.'%')->limit(10)->get();
        $results[] = $departamento;
        foreach ($codes as $item) {
            $results[] = [
                'value' => ucwords($item->city),
            ];
        }

        return response()->json($results);
    }

    public function contacto(){

    	$term = strtolower( trim( Input::get('term') ) );
        $results = array();
        
        $users = User::whereHas('roles', function($q) {
            $q->where('name', 'user');
        })->where('name', 'LIKE', '%'.$term.'%')
        ->where('visible','true')
        ->limit(10)
        ->get();

        $results[] = $term;
        
        foreach ($users as $item) {
            $results[] = [
                'value' => $item->name,
            ];
        }

        return response()->json($results);
    }

}