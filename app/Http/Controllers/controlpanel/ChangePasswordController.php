<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;
use Auth;

class ChangePasswordController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try {
            $user = User::find($id);
            $result = $user->id;
            $usuario = Auth::user()->id;
            if ($result == $usuario) {
                // dd('Si');
                return view('controlpanel.changePassword.edit', [
                'title' => 'Cambiar contraseña',
                'user' => $user,
                'menu' => $this->menu(),
                ]);
            } else {
                return redirect('ControlPanel/');
            }
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect('ControlPanel/');
        }
    }

    public function update(Request $request, $id)
    {
        try {
            // $this->validate($request, [
            //     'oldPass' => 'required',
            //     'newPass' => 'required|min:5',
            //     'verPass' => 'required|same:newPass',
            // ]);

            // $data = $request->all();
            // $user = User::find(Auth::user()->id);
            
            // if (!\Hash::check($data['oldPass'], $user->password)) {
            //     \Session::flash('error_message','¡La contraseña que escribió como "Contraseña anterior" no concuerda con nuestra base de datos!');
            //     return redirect()->back();
            // } 

            $update = User::find($id);
            $update->password = bcrypt($request->newPass);
            $update->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $update->update();

            // $role = Role::find($request->role_id);

            // $update->attachRole($role);

            \Session::flash('success_message','¡La contraseña fue actualizada!');
            return redirect('ControlPanel/');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect('ControlPanel/');
        }
    }

    public function destroy($id)
    {
        //
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => '',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
