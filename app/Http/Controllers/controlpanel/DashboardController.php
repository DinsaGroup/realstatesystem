<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use SoapClient;

use App\Property;
use App\FeaturedProperty;
use App\GoldProperty;
use App\User;

class DashboardController extends Controller
{
    public function index()
    {
        try{
            
            $properties = Property::orderBy('id','desc')->first();
            $last_properties = Property::where('published',true)->orderBy('id','desc')->get()->take(16);
            // $despublished = Property::where('published',false)->get();
            $today = Carbon::now();
            $featured = FeaturedProperty::where('start','<',$today)->where('end','>',$today)->get();
            $goldproperty = GoldProperty::where('start','<',$today)->where('end','>',$today)->get();
            $clients = User::whereHas('roles', function($q){
                                $q->where('name', 'user');
                            }
                        )->orderBy('id','desc')->first();

            return view('controlpanel.dashboard.index',[
                'title' => 'Dashboard',
                'last_properties' => $last_properties,
                'total' => $properties->id,
                'despublished' => 0,
                'clientes' => $clients->id,
                'featured' => $featured,
                'gold' => $goldproperty,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'dashboard',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
