<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\FeaturedProperty;
use App\Property;
use App\User;
use Auth;


class FeaturedController extends Controller
{
    public function index()
    {
        try {
            
            if(Auth::user()->hasRole('superadmin')) {
                $featured = FeaturedProperty::all();
            } else {
                $featured = FeaturedProperty::where('visible', 'true')->OrderBy('id', 'asc')->get();
            }

            return view('controlpanel.properties.featured.index',[
                'title' => 'Inmuebles destacados',
                'subtitle' => 'Estos son los inmuebles que se desean vender con mayor prioridad en el sistema, son visibles en los resultados de busqueda y tienen mayor relevancia.<br>Solo los administradores pueden crear nuevos registros en esta área.',
                'description' => 'Lista de propiedades destacadas',
                'featured' => $featured,
                'menu' => $this->menu('propiedades','destacadas'),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{
            // $properties = Property::all()->pluck('code', 'id');
            return view('controlpanel.properties.featured.create',[
                'title' => 'Crear nuevo',
                // 'properties' => $properties,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {

        try{
            $property = Property::where('code', $request->property_id)->first();

            $new = new FeaturedProperty;
            $new->property_id = $property->id;
            $new->start = $request->start;
            $new->end = $request->end;
            $new->note = $request->note;
            $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->save();

            \Session::flash('success_message','¡El registro se ha creado con éxito!');
            return redirect('ControlPanel/propiedades-destacadas');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try{
            $featuredProperty = FeaturedProperty::find($id);
            return view('controlpanel.properties.featured.edit',[
                    'title' => 'Editar',
                    'menu' => $this->menu(),
                    'featuredProperty' => $featuredProperty,
                ]);
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $update = FeaturedProperty::find($id);
            $update->start = $request->start;
            $update->end = $request->end;
            $update->note = $request->note;
            $update->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $update->update();

            \Session::flash('success_message','¡El registro se ha actualizado con éxito!');
            return redirect('ControlPanel/propiedades-destacadas');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
         try{
            $destroy = FeaturedProperty::find($id);
            $destroy->visible = false;
            $destroy->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $destroy->update();

            \Session::flash('success_message','¡El registro se ha eliminado con éxito!');
            return redirect('ControlPanel/propiedades-destacadas');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => 'propiedades',
                'level_2' => 'destacadas',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
