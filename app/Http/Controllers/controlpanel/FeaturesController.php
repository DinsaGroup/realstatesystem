<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Feature;

class FeaturesController extends Controller
{
    public function index()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            if(Auth::user()->hasRole('superadmin')) {
                $feature = Feature::OrderBy('id', 'asc')->get();
            } else {
                $feature = Feature::where('visible','true')->OrderBy('name','asc')->get();
            }
            return view('controlpanel.features.index',[
                    'title' => 'Características',
                    'menu' => $this->menu(),
                    'feature' => $feature,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            return view('controlpanel.features.create',[
                    'title' => 'Crear nueva característica',
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{
            $new = new Feature;
            $new->name = $request->name;
            $new->description = $request->description;
            $new->icon = $request->icon;
            $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->save();

            \Session::flash('success_message','¡La unidad se ha creado con éxito!');
            return redirect('ControlPanel/caracteristicas');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            $feature = Feature::find($id);
            return view('controlpanel.features.edit',[
                    'title' => 'Editar unidad de medida',
                    'menu' => $this->menu(),
                    'feature' => $feature,
                ]);
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $update = Feature::find($id);
            $update->name = $request->name;
            $update->description = $request->description;
            $update->icon = $request->icon;
            $update->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $update->update();

            \Session::flash('success_message','¡La unidad se ha actualizado con éxito!');
            return redirect('ControlPanel/caracteristicas');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try{
            $destroy = Feature::find($id);
            $destroy->visible = 'false';
            $destroy->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $destroy->update();

            \Session::flash('success_message','¡La unidad se ha eliminado con éxito!');
            return redirect('ControlPanel/unidades-de-medida');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => 'caracteristicas',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
