<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\GoldProperty;
use App\Property;
use Auth;


class GoldPropertyController extends Controller
{
    public function index()
    {
        try {
            
            if (Auth::user()->hasRole('superadmin')) {
                $goldproperties = GoldProperty::all();
            } else {
                $goldproperties = GoldProperty::where('visible', 'true')->OrderBy('id', 'asc')->get();
            }

            return view('controlpanel.properties.gold.index',[
                'title' => 'Inmuebles oro',
                'subtitle' => 'Estos son los inmuebles que se desean vender con mayor prioridad en el sistema, son visibles en los resultados de busqueda y tienen mayor relevancia.<br>Solo los administradores pueden crear nuevos registros en esta área.',
                'description' => 'Lista de inmuebles destacados',
                'goldproperties' => $goldproperties,
                'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {   
        try{
            // $properties = Property::all()->pluck('code', 'id');
            return view('controlpanel.properties.gold.create',[
                    'title' => 'Crear nueva',
                    // 'properties' => $properties,
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        // dd($request);
        try{
            $property = Property::where('code', $request->property_id)->first();

            $new = new GoldProperty;
            $new->property_id = $property->id;
            $new->type = $request->type;
            $new->start = $request->start;
            $new->end = $request->end;
            $new->rentPrice = number_format($request->rentPrice,2,'.','');
            $new->soldPrice = number_format($request->soldPrice,2,'.','');
            $new->status = $request->status;
            $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->save();

            \Session::flash('success_message','¡El registro se ha creado con éxito!');
            return redirect('ControlPanel/propiedades-oro');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
       try{
            $goldProperty = GoldProperty::find($id);
            return view('controlpanel.properties.gold.edit',[
                    'title' => 'Editar',
                    'menu' => $this->menu(),
                    'goldProperty' => $goldProperty,
                ]);
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $update = GoldProperty::find($id);
            $update->type = $request->type;
            $update->start = $request->start;
            $update->end = $request->end;
            $update->rentPrice = $request->rentPrice;
            $update->soldPrice = $request->soldPrice;
            $update->status = $request->status;
            $update->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $update->update();

            \Session::flash('success_message','¡El registro se ha actualizado con éxito!');
            return redirect('ControlPanel/propiedades-oro');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
         try{
            $destroy = GoldProperty::find($id);
            $destroy->visible = false;
            $destroy->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $destroy->update();

            \Session::flash('success_message','¡El registro se ha eliminado con éxito!');
            return redirect('ControlPanel/propiedades-oro');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => 'propiedades',
                'level_2' => 'oro',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
