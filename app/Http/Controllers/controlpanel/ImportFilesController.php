<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Country;
use App\Department;
use App\Municipio;
use App\Barrio;
use App\Pool;
use App\Role;
use App\Room;
use App\User;
use App\Property;
use App\Restroom;
use App\PropertyType;
use App\FeatureProperty;
use App\WaterHeaterType;
use App\AirConditionerType;
use App\PropertyUser;
use App\Photo;
use App\Sign;

use Auth;
use Image;
use Carbon\Carbon;

class ImportFilesController extends Controller
{
   
    public function contactos(Request $request) {
    	// $role = Role::where('name','user')->first();

    	if($request->file('imported-file')){
                    $path = $request->file('imported-file')->getRealPath();
                    // $data = \Excel::load($path, function($reader) {})->get(); 
                    $data = \Excel::load($path, false, 'ISO-8859-1')->get();

                    $role = Role::find(5); 

                if(!empty($data) && $data->count()){
                    $data = $data->toArray();
                    $dataImported = array();

                    for($i=0;$i<count($data);$i++){

                            $user = User::where('referenced_id',$data[$i]['idcontacto'])->first();

                            if($user){
                                $user->name 		    = strtolower($data[$i]['nombre1']); 
                                $user->update();  
                            }else{
                                $new = new User;
                                $new->identification = $data[$i]['identification'];
                                $new->referenced_id = $data[$i]['idcontacto'];
                                $new->name          = strtolower($data[$i]['nombre1']); 
                                $new->email 	    = mb_strtolower($data[$i]['email'], 'utf-8');
                                $new->photo 	    = 'default.jpg';
                                $new->house 	    = mb_strtolower($data[$i]['teldirecto'], 'utf-8');
                                $new->office 	    = mb_strtolower($data[$i]['teloficina'].' '.$data[$i]['extoficina'], 'utf-8');
                                $new->phone 	    = mb_strtolower($data[$i]['celular'], 'utf-8');
                                $new->comments 	    = mb_strtolower($data[$i]['notas'],'utf-8'). ' '.mb_strtolower($data[$i]['notas2'], 'utf-8'). ' ' .mb_strtolower($data[$i]['notas3'], 'utf-8'). ' ' .mb_strtolower($data[$i]['notas4'], 'utf-8'). ' '.mb_strtolower($data[$i]['notas5'], 'utf-8');
                                $new->save();

                                $new->attachRole($role);
                            }

                    }

                    

                }
        }

        return back();
    }

    public function cedulas(Request $request) {
        

        if($request->file('imported-file')){
                    $path = $request->file('imported-file')->getRealPath();
                    $data = \Excel::load($path, function($reader) {
                })->get(); 

                if(!empty($data) && $data->count()){
                    $data = $data->toArray();
                    $dataImported = array();


                    for($i=0;$i<count($data);$i++){

                        $user = User::where('referenced_id',$data[$i]['idcontacto'])->first();
                        if($user){
                            $user->identification = $data[$i]['identificacion'];
                            // $user->identification = '';
                            $user->update();
                            
                        }

                    }

                }

        }

        return back();
    }

    public function usuarios(Request $request) {
        // $role = Role::where('name','admin')->first();

        if($request->file('imported-file')){
                $path = $request->file('imported-file')->getRealPath();
                $data = \Excel::load($path, function($reader) {
            })->get(); 

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();

                for($i=0;$i<count($data);$i++){
                    // dd($data[$i]);

                    $new                = new User;
                    $new->name          = mb_strtolower($data[$i]['login'], 'utf-8');
                    $new->referenced_id = $data[$i]['idusuario'];
                    $new->email         = 'prueba@prueba.com';
                    $new->photo         = 'default.jpg';
                    $new->password      = bcrypt('tempo*01');
                    // $new->house         = mb_strtolower($data[$i]['teldirecto'], 'utf-8');
                    // $new->office        = mb_strtolower($data[$i]['teloficina'].' '.$data[$i]['extoficina'], 'utf-8');
                    // $new->phone         = mb_strtolower($data[$i]['celular'], 'utf-8');
                    // $new->comments      = mb_strtolower($data[$i]['notas'],'utf-8'). ' '.mb_strtolower($data[$i]['notas2'], 'utf-8'). ' ' .mb_strtolower($data[$i]['notas3'], 'utf-8'). ' ' .mb_strtolower($data[$i]['notas4'], 'utf-8'). ' '.mb_strtolower($data[$i]['notas5'], 'utf-8');
                    $new->save();
                    if ($data[$i]['idrol'] == 1) {
                        $role = Role::where('name', 'superadmin')->first();
                    } elseif ($data[$i]['idrol'] == 2) {
                        $role = Role::where('name', 'admin')->first();
                    } else {
                        $role = Role::where('name', 'agent')->first();
                    }

                    $new->attachRole($role);
                }
            }
        }

        return back();
    }

    public function propiedades(Request $request) {

        ini_set('memory_limit', '-1');
        
        if($request->file('imported-file')){
                $path = $request->file('imported-file')->getRealPath();
                $data = \Excel::load($path, false, 'ISO-8859-1')->get(); 
            //     $data = \Excel::load($path, function($reader) {
            // })->get(); 

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();
                
                for($i=0;$i<count($data);$i++){
                    
                    $new                    = new Property;
                    $new->status_id         = 40;
                    $new->code_id           = $data[$i]['idinmueble'];
                    $new->agent_id          = $data[$i]['agenteid'];

                    if ($data[$i]['idcategoriainmueble'] == 1) {
                        $new->property_type_id = 1;
                    } elseif ($data[$i]['idcategoriainmueble'] == 5) {
                        $new->property_type_id = 9;
                    } elseif ($data[$i]['idcategoriainmueble'] == 6) {
                        $new->property_type_id = 11;
                    } elseif ($data[$i]['idcategoriainmueble'] == 7) {
                        $new->property_type_id = 2;
                    } elseif ($data[$i]['idcategoriainmueble'] == 12) {
                        $new->property_type_id = 3;
                    } elseif ($data[$i]['idcategoriainmueble'] == 30) {
                        $new->property_type_id = 5;
                    } elseif ($data[$i]['idcategoriainmueble'] == 31) {
                        $new->property_type_id = 7;
                    } elseif ($data[$i]['idcategoriainmueble'] == 33) {
                        $new->property_type_id = 8;
                    } elseif ($data[$i]['idcategoriainmueble'] == 39 || $data[$i]['idcategoriainmueble'] == 41) {
                        $new->property_type_id = 9;
                    } elseif ($data[$i]['idcategoriainmueble'] == 42) {
                        $new->property_type_id = 10;
                    } elseif ($data[$i]['idcategoriainmueble'] == 44 || $data[$i]['idcategoriainmueble'] == 48) {
                        $new->property_type_id = 11;
                    } elseif ($data[$i]['idcategoriainmueble'] == 50) {
                        $new->property_type_id = 14;
                    } elseif ($data[$i]['idcategoriainmueble'] >= 52 && $data[$i]['idcategoriainmueble'] <= 55) {
                        $new->property_type_id = 15;
                    } elseif ($data[$i]['idcategoriainmueble'] == 56) {
                        $new->property_type_id = 18;
                    } elseif ($data[$i]['idcategoriainmueble'] == 57) {
                        $new->property_type_id = 19;
                    } elseif ($data[$i]['idcategoriainmueble'] == 58) {
                        $new->property_type_id = 11;
                    } elseif ($data[$i]['idcategoriainmueble'] >= 59 && $data[$i]['idcategoriainmueble'] <= 70) {
                        $new->property_type_id = 20;
                    } elseif ($data[$i]['idcategoriainmueble'] == 72) {
                        $new->property_type_id = 22;
                    } elseif ($data[$i]['idcategoriainmueble'] == 73) {
                        $new->property_type_id = 3;
                    } elseif ($data[$i]['idcategoriainmueble'] == 74) {
                        $new->property_type_id = 1;
                    } elseif ($data[$i]['idcategoriainmueble'] == 77 || $data[$i]['idcategoriainmueble'] == 78) {
                        $new->property_type_id = 20;
                    } elseif ($data[$i]['idcategoriainmueble'] == 79) {
                        $new->property_type_id = 16;
                    } elseif ($data[$i]['idcategoriainmueble'] == 80) {
                        $new->property_type_id = 3;
                    } elseif ($data[$i]['idcategoriainmueble'] == 81) {
                        $new->property_type_id = 21;
                    } elseif ($data[$i]['idcategoriainmueble'] == 82) {
                        $new->property_type_id = 1;
                    } elseif ($data[$i]['idcategoriainmueble'] == 84) {
                        $new->property_type_id = 21;
                    } elseif ($data[$i]['idcategoriainmueble'] == 86) {
                        $new->property_type_id = 4;
                    } elseif ($data[$i]['idcategoriainmueble'] == 89) {
                        $new->property_type_id = 17;
                    } elseif ($data[$i]['idcategoriainmueble'] == 90) {
                        $new->property_type_id = 13;
                    } elseif ($data[$i]['idcategoriainmueble'] == 91) {
                        $new->property_type_id = 22;
                    } elseif ($data[$i]['idcategoriainmueble'] == 92) {
                        $new->property_type_id = 16;
                    } elseif ($data[$i]['idcategoriainmueble'] == 93) {
                        $new->property_type_id = 12;
                    } elseif ($data[$i]['idcategoriainmueble'] == 94 || $data[$i]['idcategoriainmueble'] == 95) {
                        $new->property_type_id = 14;
                    } elseif ($data[$i]['idcategoriainmueble'] == 96) {
                        $new->property_type_id = 11;
                    } else {
                        $new->property_type_id = 23;
                    }
                    $new->measure_unit_id   = 1;
                    $propertyType = PropertyType::where('id', $new->property_type_id)->first();
                    
                    $new->exclusividad      = $data[$i]['exclusividad'];
                    $new->disponible        = $data[$i]['disponible'];
                    if ($data[$i]['paraventa'] == 'true') {
                        $new->ventaRenta       = 'venta';
                        $new->precioVenta       = $data[$i]['precioventa'];
                        $code = $propertyType->code . 'V-';
                    } 
                    if ($data[$i]['paraalquiler'] == 'true') {
                        $new->ventaRenta       = 'renta';
                        $new->precioAlquiler       = $data[$i]['precioalquiler'];
                        $code = $propertyType->code . 'R-';
                    }
                    if ($data[$i]['paraventa'] == 'true' && $data[$i]['paraalquiler'] == 'true') {
                        $new->ventaRenta       = 'venta/renta';
                        $new->precioVenta       = $data[$i]['precioventa'];
                        $new->precioAlquiler       = $data[$i]['precioalquiler'];
                        $code = $propertyType->code . 'VR-';
                    }
                    if ($data[$i]['paraventa'] == 'false' && $data[$i]['paraalquiler'] == 'false') {
                        $new->ventaRenta       = 'ninguna';
                    }
                    // $new->code              = $data[$i]['codigo'];
                    $new->precioPorV2       = $data[$i]['precioporv2'];
                    $new->areaTotal         = $data[$i]['areatotal'];
                    $new->areaConstruccion  = $data[$i]['areaconstruccion'];
                    $new->antiguedad        = $data[$i]['antiguedad'];
                    $new->areaConstruccion  = $data[$i]['areaconstruccion'];
                    $new->acceso            = $data[$i]['acceso'];
                    $new->topografia        = $data[$i]['topografia'];
                    $new->description       = $data[$i]['notas'];
                    $new->address           = $data[$i]['ubicacion'];
                    $sector                 = Barrio::where('my_id', $data[$i]['idbarrio'])->first();
                    $new->sector            = strtolower( trim($sector->name) );
                    $new->latitude          = $sector->latitude;
                    $new->longitude         = $sector->longitude;                    
                    $city                   = Municipio::where('my_id', $sector->municipio_id)->first();
                    $new->city              = strtolower( trim($city->name));
                    $state                  = Department::where('my_id', $city->department_id)->first();
                    $new->state             = strtolower( trim($state->name));
                    $country                = Country::where('id', $state->country_id)->first();
                    $new->country           = strtolower( trim($country->name));
                    $new->estadoTmp         = strtolower( trim($data[$i]['estadotmp']) );
                    $new->areaMz            = $data[$i]['areamz'];
                    $new->notes             = $data[$i]['observaciones']. ' ' .$data[$i]['notasprecio']. ' ' .$data[$i]['notasantiguedad']. ' - Ingresada en el sistema por ' .$data[$i]['enlistadapor'];
                    $new->metaTitleEs       = $data[$i]['metatitlees'];
                    $new->metaTitleEn       = $data[$i]['metatitleen'];
                    $new->metaKeywordEs     = $data[$i]['metakeywordses'];
                    $new->metaKeywordEn     = $data[$i]['metakeywordsen'];
                    $new->created_at        = Carbon::parse($data[$i]['fechaingreso']);

                    $new->created_by        = $data[$i]['enlistadapor'];
                    $new->updated_by        = $data[$i]['enlistadapor'];
                    $new->published         = true;
                    $new->save();

                    $new->code              = $code.$new->id;
                    $new->update();

                    if($data[$i]['sala'] == 'true') {
                        $fp = new FeatureProperty;
                        $fp->property_id    = $new->id;
                        $fp->feature_id     = 1;
                        $fp->save();
                    }

                    if($data[$i]['comedor'] == 'true') {
                        $fp1 = new FeatureProperty;
                        $fp1->property_id    = $new->id;
                        $fp1->feature_id     = 2;
                        $fp1->save();
                    }

                    if($data[$i]['salacomedor'] == 'true') {
                        $fp2 = new FeatureProperty;
                        $fp2->property_id    = $new->id;
                        $fp2->feature_id     = 3;
                        $fp2->save();
                    }

                    if($data[$i]['cocina'] == 'true') {
                        $fp3 = new FeatureProperty;
                        $fp3->property_id    = $new->id;
                        $fp3->feature_id     = 4;
                        $fp3->save();
                    }

                    if($data[$i]['alacena'] == 'true') {
                        $fp4 = new FeatureProperty;
                        $fp4->property_id    = $new->id;
                        $fp4->feature_id     = 5;
                        $fp4->save();
                    }

                    if($data[$i]['cuartos'] != '') {
                        $fp5 = new FeatureProperty;
                        $fp5->property_id    = $new->id;
                        $fp5->feature_id     = 6;
                        $fp5->valor          = $data[$i]['cuartos'];
                        $fp5->save();

                        $cuarto = new Room;
                        $cuarto->property_id = $new->id;
                        $cuarto->quantity    = $data[$i]['cuartos'];
                        $cuarto->save();
                    }

                    if($data[$i]['banios'] != '') {
                        $fp6 = new FeatureProperty;
                        $fp6->property_id    = $new->id;
                        $fp6->feature_id     = 7;
                        $fp6->valor          = $data[$i]['banios'];
                        $fp6->save();

                        $cuarto = new Restroom;
                        $cuarto->property_id = $new->id;
                        $cuarto->quantity    = $data[$i]['banios'];
                        $cuarto->save();
                    }

                    if($data[$i]['salafamiliar'] == 'true') {
                        $fp7 = new FeatureProperty;
                        $fp7->property_id    = $new->id;
                        $fp7->feature_id     = 8;
                        $fp7->save();
                    }

                    if($data[$i]['terraza'] == 'true') {
                        $fp8 = new FeatureProperty;
                        $fp8->property_id    = $new->id;
                        $fp8->feature_id     = 9;
                        $fp8->save();
                    }

                    if($data[$i]['patio'] == 'true') {
                        $fp9 = new FeatureProperty;
                        $fp9->property_id    = $new->id;
                        $fp9->feature_id     = 10;
                        $fp9->save();
                    }

                    if($data[$i]['jardinesexternos'] == 'true') {
                        $fp10 = new FeatureProperty;
                        $fp10->property_id    = $new->id;
                        $fp10->feature_id     = 11;
                        $fp10->save();
                    }

                    if($data[$i]['jardinesinternos'] == 'true') {
                        $fp11 = new FeatureProperty;
                        $fp11->property_id    = $new->id;
                        $fp11->feature_id     = 12;
                        $fp11->save();
                    }

                    if($data[$i]['areaservicio'] == 'true') {
                        $fp12 = new FeatureProperty;
                        $fp12->property_id    = $new->id;
                        $fp12->feature_id     = 13;
                        $fp12->save();
                    }

                    if($data[$i]['estacionamiento'] == 'true') {
                        $fp13 = new FeatureProperty;
                        $fp13->property_id    = $new->id;
                        $fp13->feature_id     = 14;
                        $fp13->save();
                    }

                    if($data[$i]['bodega'] == 'true') {
                        $fp14 = new FeatureProperty;
                        $fp14->property_id    = $new->id;
                        $fp14->feature_id     = 15;
                        $fp14->save();
                    }

                    if($data[$i]['tanqueagua'] == 'true') {
                        $fp15 = new FeatureProperty;
                        $fp15->property_id    = $new->id;
                        $fp15->feature_id     = 16;
                        $fp15->save();
                    }

                    if($data[$i]['idtipocalentadoragua'] == 1) {
                        $fp16 = new FeatureProperty;
                        $fp16->property_id    = $new->id;
                        $fp16->feature_id     = 19;
                        $fp16->valor          = 'gas';
                        $fp16->save();

                        $wh = new WaterHeaterType;
                        $wh->property_id      = $new->id;
                        $wh->type             = 'gas';
                        $wh->save();
                    } elseif ($data[$i]['idtipocalentadoragua'] == 2) {
                        $fp16 = new FeatureProperty;
                        $fp16->property_id    = $new->id;
                        $fp16->feature_id     = 19;
                        $fp16->valor          = 'electrico';
                        $fp16->save();

                        $wh = new WaterHeaterType;
                        $wh->property_id      = $new->id;
                        $wh->type             = 'electrico';
                        $wh->save();
                    }

                    if($data[$i]['idtipoac'] == 1) {
                        $fp17 = new FeatureProperty;
                        $fp17->property_id    = $new->id;
                        $fp17->feature_id     = 18;
                        $fp17->valor          = 'split';
                        $fp17->save();

                        $ac = new WaterHeaterType;
                        $ac->property_id      = $new->id;
                        $ac->type             = 'split';
                        $ac->save();
                    } elseif ($data[$i]['idtipoac'] == 2) {
                        $fp17 = new FeatureProperty;
                        $fp17->property_id    = $new->id;
                        $fp17->feature_id     = 18;
                        $fp17->valor          = 'ventana';
                        $fp17->save();

                        $ac = new WaterHeaterType;
                        $ac->property_id      = $new->id;
                        $ac->type             = 'ventana';
                        $ac->save();
                    } elseif ($data[$i]['idtipoac'] == 3) {
                        $fp17 = new FeatureProperty;
                        $fp17->property_id    = $new->id;
                        $fp17->feature_id     = 18;
                        $fp17->valor          = 'central';
                        $fp17->save();

                        $ac = new WaterHeaterType;
                        $ac->property_id      = $new->id;
                        $ac->type             = 'central';
                        $ac->save();
                    }

                    if($data[$i]['piscina'] == 'true') {
                        $fp18 = new FeatureProperty;
                        $fp18->property_id    = $new->id;
                        $fp18->feature_id     = 17;
                        $fp18->save();

                        $pool = new Pool;
                        $pool->property_id    = $new->id;
                        $pool->save();
                    }

                    if($data[$i]['jacuzzi'] == 'true') {
                        $fp19 = new FeatureProperty;
                        $fp19->property_id    = $new->id;
                        $fp19->feature_id     = 20;
                        $fp19->save();
                    }

                    if($data[$i]['telefono'] == 'true') {
                        $fp20 = new FeatureProperty;
                        $fp20->property_id    = $new->id;
                        $fp20->feature_id     = 21;
                        $fp20->save();
                    }

                    if($data[$i]['lineastelefonicas'] != '') {
                        $fp21 = new FeatureProperty;
                        $fp21->property_id    = $new->id;
                        $fp21->feature_id     = 22;
                        $fp21->valor          = $data[$i]['lineastelefonicas'];
                        $fp21->save();
                    }

                    if($data[$i]['plantaelectrica'] == 'true') {
                        $fp22 = new FeatureProperty;
                        $fp22->property_id    = $new->id;
                        $fp22->feature_id     = 23;
                        $fp22->save();
                    }

                    if($data[$i]['voltaje'] != '') {
                        $fp23 = new FeatureProperty;
                        $fp23->property_id    = $new->id;
                        $fp23->feature_id     = 24;
                        $fp23->valor          = $data[$i]['voltaje'];
                        $fp23->save();
                    }

                    if($data[$i]['lineablanca'] == 'true') {
                        $fp24 = new FeatureProperty;
                        $fp24->property_id    = $new->id;
                        $fp24->feature_id     = 25;
                        $fp24->save();
                    }

                    if($data[$i]['amueblado'] == 'true') {
                        $fp25 = new FeatureProperty;
                        $fp25->property_id    = $new->id;
                        $fp25->feature_id     = 26;
                        $fp25->save();
                    }

                    if($data[$i]['habitada'] == 'true') {
                        $fp26 = new FeatureProperty;
                        $fp26->property_id    = $new->id;
                        $fp26->feature_id     = 27;
                        $fp26->valor          = 'si';
                        $fp26->save();
                    }

                    if($data[$i]['poseeescrituras'] == 'true') {
                        $fp27 = new FeatureProperty;
                        $fp27->property_id    = $new->id;
                        $fp27->feature_id     = 28;
                        $fp27->valor          = 'si';
                        $fp27->save();
                    }

                    if($data[$i]['poseeavaluos'] == 'true') {
                        $fp28 = new FeatureProperty;
                        $fp28->property_id    = $new->id;
                        $fp28->feature_id     = 29;
                        $fp28->valor          = 'si';
                        $fp28->save();
                    }

                    if($data[$i]['poseeplanos'] == 'true') {
                        $fp29 = new FeatureProperty;
                        $fp29->property_id    = $new->id;
                        $fp29->feature_id     = 30;
                        $fp29->valor          = 'si';
                        $fp29->save();
                    }

                    if($data[$i]['poseelibertadgravamen'] == 'true') {
                        $fp30 = new FeatureProperty;
                        $fp30->property_id    = $new->id;
                        $fp30->feature_id     = 31;
                        $fp30->valor          = 'si';
                        $fp30->save();
                    }

                    if($data[$i]['poseehistorialregistral'] == 'true') {
                        $fp31 = new FeatureProperty;
                        $fp31->property_id    = $new->id;
                        $fp31->feature_id     = 32;
                        $fp31->valor          = 'si';
                        $fp31->save();
                    }

                    if($data[$i]['poseecertifcatastral'] == 'true') {
                        $fp32 = new FeatureProperty;
                        $fp32->property_id    = $new->id;
                        $fp32->feature_id     = 33;
                        $fp32->valor          = 'si';
                        $fp32->save();
                    }

                    if($data[$i]['poseefirma'] == 'true') {
                        $fp33 = new FeatureProperty;
                        $fp33->property_id    = $new->id;
                        $fp33->feature_id     = 34;
                        $fp33->valor          = 'si';
                        $fp33->save();
                    }
                }
            }
        }
        return back();
    }

    public function country(Request $request) {
        if($request->file('imported-file')){
                $path = $request->file('imported-file')->getRealPath();
                $data = \Excel::load($path, function($reader) {
            })->get(); 

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();

                for($i=0;$i<count($data);$i++){
                    $new                = new Country;
                    $new->name          = $data[$i]['nombrepais'];
                    // $new->table_id      = $data[$i]['idpais'];
                    $new->save();
                }
            }
        }
        return back();
    }

    public function department(Request $request) {
        if($request->file('imported-file')){
                $path = $request->file('imported-file')->getRealPath();
                $data = \Excel::load($path, function($reader) {
            })->get(); 

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();

                for($i=0;$i<count($data);$i++){
                    $new                = new Department;
                    $new->name          = $data[$i]['nombredepartamento'];
                    $new->my_id         = $data[$i]['iddepartamento'];
                    $new->country_id    = $data[$i]['idpais'];
                    $new->save();
                }
            }
        }
        return back();
    }

    public function municipio(Request $request) {
        if($request->file('imported-file')){
                $path = $request->file('imported-file')->getRealPath();
                $data = \Excel::load($path, function($reader) {
            })->get(); 

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();

                for($i=0;$i<count($data);$i++){
                    $new                = new Municipio;
                    $new->name          = $data[$i]['nombremunicipio'];
                    $new->my_id         = $data[$i]['idmunicipio'];
                    $new->department_id = $data[$i]['iddepartamento'];
                    $new->save();
                }
            }
        }
        return back();
    }

    public function barrio(Request $request) {
        if($request->file('imported-file')){
                $path = $request->file('imported-file')->getRealPath();
                $data = \Excel::load($path, function($reader) {
            })->get(); 

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();

                for($i=0;$i<count($data);$i++){
                    $new                = new Barrio;
                    $new->name          = $data[$i]['nombrebarrio'];
                    $new->my_id         = $data[$i]['idbarrio'];
                    $new->municipio_id  = $data[$i]['idmunicipio'];
                    $new->latitude      = $data[$i]['latitude'];
                    $new->longitude     = $data[$i]['longitud'];
                    $new->save();
                }
            }
        }
        return back();
    }

    public function property_user(Request $request){
        if($request->file('imported-file')){
                $path = $request->file('imported-file')->getRealPath();
                $data = \Excel::load($path, function($reader) {
            })->get(); 

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();

                for($i=0;$i<count($data);$i++){
                    $new                = new PropertyUser;
                    $property = Property::where('code_id',$data[$i]['idinmueble'])->first();
                    if(!$property){
                        return $data[$i];
                    }
                    $new->property_id   = $property->id;
                    $contact = User::where('referenced_id',$data[$i]['idcontacto'])->first();
                    if(!$contact){
                        return $data[$i];
                    }
                    $new->user_id       = $contact->id;
                    $new->usertype_id   = $data[$i]['idtipocontacto']+1;
                    $new->save();
                }
            }
        }
        return back();
    }

    public function photos(Request $request) {
        if($request->file('imported-file')){
                $path = $request->file('imported-file')->getRealPath();
                $data = \Excel::load($path, function($reader) {
            })->get(); 

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();

                for($i=0;$i<count($data);$i++){
                    $property = Property::where('code_id',$data[$i]['idinmueble'])->first();
                    $new                = new Photo;
                    $new->property_id   = $property->id;
                    $new->filename      = $data[$i]['nombrearchivo'];
                    $new->visible       = 'true';
                    $new->created_by    = 'superadmin';
                    $new->updated_by    = 'superadmin';
                    $new->save();
                }
            }
        }
        return back();
    }

    public function signs(Request $request) {
        if($request->file('imported-file')){
                $path = $request->file('imported-file')->getRealPath();
                $data = \Excel::load($path, function($reader) {
            })->get(); 

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();

                for($i=0;$i<count($data);$i++){
                    $property = Property::where('code_id',$data[$i]['idinmueble'])->first();
                    if($property){

                        $new                = new Sign;
                        $new->property_id   = $property->id;
                        $new->code          = $data[$i]['codigo'];
                        $new->status        = 'verificar';
                        $new->visible       = 'true';
                        $new->created_by    = 'superadmin';
                        $new->updated_by    = 'superadmin';
                        $new->save();

                    }
                }
            }
        }
        return back();
    }

    public function notes(Request $request) {
        if($request->file('imported-file')){
            $path = $request->file('imported-file')->getRealPath();
            // $data = \Excel::load($path, function($reader) {},'UTF-8')->get();
            $data = \Excel::load($path, false, 'ISO-8859-1')->get();  

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();
                
                for($i=0;$i<count($data);$i++){

                    $property = Property::where('code_id',$data[$i]['idinmueble'])->first();

                    if($property){
                        // $notas = mb_strtolower( strtolower($data[$i]['notas']) , 'utf-8');
                        $property->description = $data[$i]['notas'];
                        $property->notes = $data[$i]['observaciones'];
                        $property->update();

                    }
                }
            }
        }
        return back();
    }

    public function correcciones(Request $request) {
        
        if($request->file('imported-file')){
            $path = $request->file('imported-file')->getRealPath();
            // $data = \Excel::load($path, function($reader) {},'UTF-8')->get();
            $data = \Excel::load($path, false, 'ISO-8859-1')->get();  

            if(!empty($data) && $data->count()){
                $data = $data->toArray();
                $dataImported = array();
                
                for($i=0;$i<count($data);$i++){

                    $property = Property::where('code_id',$data[$i]['idinmueble'])->first();

                    if($property){
                        $property->created_at = Carbon::parse($data[$i]['fechaingreso']);
                        if($data[$i]['fechaactualizacion']==''){
                            $property->updated_at = Carbon::parse($data[$i]['fechaingreso']);
                        }else{
                            $property->updated_at = Carbon::parse($data[$i]['fechaactualizacion']);
                        }
                        // $property->estadoTmp = strtolower($data[$i]['estadotmp']);
                        // $property->agent_id = $data[$i]['idagente'];
                        
                        $property->update();

                    }
                }
            }
        }
        return back();
    }

    public function agentes(Request $request) {

        $agents = User::whereHas('roles', function($q){
                                $q->where('name', 'agent')->orWhere('name','admin')->orWhere('name','superadmin');
                            }
                        )->get();

        if($request->file('imported-file')){
                    $path = $request->file('imported-file')->getRealPath();
                    // $data = \Excel::load($path, function($reader) {},'UTF-8')->get(); 
                    $data = \Excel::load($path, false, 'ISO-8859-1')->get();

                if(!empty($data) && $data->count()){
                    $data = $data->toArray();
                    $dataImported = array();

                    for($i=0;$i<count($data);$i++){
                        
                        $inmueble = Property::where('code_id',trim( $data[$i]['idinmueble'] ) )->first();
 
                        if($inmueble){
                            $agente = strtolower( trim($data[$i]['agente']) );
                            $identificado = false;
                            foreach($agents as $item){
                                if($item->name == $agente ){
                                    $inmueble->agent_id = $item->id;
                                    $identificado = true;
                                }// end if
                            }// end foreach

                            if(!$identificado){
                                $inmueble->agent_id = 10086;
                            }

                            $inmueble->update();

                        }
                    }

                    

                }
        }

        return back();
    }

    public function getCode() {
    }

}