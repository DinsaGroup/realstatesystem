<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Sign;
use App\Property;
use App\Testimonial;
use App\FeatureProperty;
use App\Pool;
use App\Room;
use App\AirCondionerType;
use Auth;
use Input;

class JsonController extends Controller
{
    public function checkPass() {
        try{
            $oldPass = Input::get('oldPass');
            $user = User::find(Auth::user()->id);

            if (\Hash::check($oldPass, $user->password)) {
              $result = 1; 
            } else {
              $result = 0;
            }
            return response()->json($result);

        }catch(Exception $e){
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function getIdClient() {
        try{
            $user_id = strtoupper( trim( Input::get('term') ) );
            $results = array();
            
            $users = User::whereHas('roles', function($q) {
                $q->where('name', 'user');
            })->where('identification', 'LIKE', '%'.$user_id.'%')->limit(10)->get();

            foreach ($users as $item) {
                $results[] = [
                    'value' => $item->identification,
                ];
            }

            return response()->json($results);

        }catch(Exception $e){
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function getClient() {
        try{
            $term = strtolower( trim( Input::get('term') ) );
            $results = array();
            
            $users = User::whereHas('roles', function($q) {
                $q->where('name', 'user');
            })->where('name', 'LIKE', '%'.$term.'%')
            ->where('visible','true')
            ->orWhere('email','LIKE','%'.$term.'%')
            ->orWhere('house','LIKE','%'.$term.'%')
            ->orWhere('office','LIKE','%'.$term.'%')
            ->orWhere('phone','LIKE','%'.$term.'%')
            ->limit(10)
            ->get();

            foreach ($users as $item) {
                $results[] = [
                    'value' => $item->name,
                ];
            }

            return response()->json($results);

        }catch(Exception $e){
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function getUsers() {
        try{

            $value = strtolower( trim( Input::get('value') ) );
            $results = array();

            $users = User::whereHas('roles', function($q) {
                $q->where('name', 'user');
            })->where('name', 'LIKE', '%'.$value.'%')
              ->where('visible','true')
              ->orWhere('identification', 'LIKE', '%'.$value.'%')
              ->orWhere('email', 'LIKE', '%'.$value.'%')
              ->orWhere('phone', 'LIKE', '%'.$value.'%')
              ->orWhere('office', 'LIKE', '%'.$value.'%')
              ->orWhere('house', 'LIKE', '%'.$value.'%')
              ->get();

            foreach ($users as $item) {
                if($item->checked){
                    $checked = 'Verificado';
                    $class ='';
                }else{
                    $checked = 'Pendiente';
                    $class = 'despublished';
                }
                if($item->identification==null){
                    $identification = '';
                }else{
                    $identification = $item->identification;
                }
                $results[] = [
                    'id' => $item->id,
                    'identification' => $identification,
                    'name' => $item->name,
                    'email'=> $item->email,
                    'phone'=>$item->phone,
                    'office'=>$item->office,
                    'house'=>$item->house,
                    'checked' => $checked,
                    'class' => $class,
                ];
            }

            return response()->json($results);

        }catch(Exception $e){
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function resetUsers() {
        try{

            $value = strtolower( trim( Input::get('value') ) );
            $results = array();

            $users = User::whereHas('roles', function($q) {
                $q->where('name', 'user');
            })->where('name', 'LIKE', '%'.$value.'%')
              ->where('visible','true')
              ->orWhere('identification', 'LIKE', '%'.$value.'%')
              ->orWhere('email', 'LIKE', '%'.$value.'%')
              ->orWhere('phone', 'LIKE', '%'.$value.'%')
              ->orWhere('office', 'LIKE', '%'.$value.'%')
              ->orWhere('house', 'LIKE', '%'.$value.'%')
              ->limit(250)
              ->get();

            foreach ($users as $item) {
                if($item->checked){
                    $checked = 'Verificado';
                    $class ='';
                }else{
                    $checked = 'Pendiente';
                    $class = 'despublished';
                }
                if($item->identification==null){
                    $identification = '';
                }else{
                    $identification = $item->identification;
                }
                $results[] = [
                    'id' => $item->id,
                    'identification' => $identification,
                    'name' => $item->name,
                    'email'=> $item->email,
                    'phone'=>$item->phone,
                    'office'=>$item->office,
                    'house'=>$item->house,
                    'checked' => $checked,
                    'class' => $class,
                ];
            }

            return response()->json($results);

        }catch(Exception $e){
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function getEmail() {
        
            $email = strtolower( trim( Input::get('term') ) );
            $results = array();
            
            $users = User::whereHas('roles', function($q) {
                $q->where('name', 'user');
            })->where('email', 'LIKE', '%'.$email.'%')->limit(10)->get();

            foreach ($users as $item) {
                $results[] = [
                    'value' => $item->email,
                ];
            }

            return response()->json($results);
    }

    public function getProperties_by_keyword_bk(){

        $code = strtoupper( trim( Input::get('value') ) );
        $value = strtolower( trim( Input::get('value') ) );
        $propertyType = Input::get('propertyType');
        $price_from = trim(Input::get('price_from'));
        $price_at = trim(Input::get('price_at'));
        $rotulo = trim(Input::get('rotulo'));
        $cuartos = trim(Input::get('cuartos'));
        $banos = trim(Input::get('banos'));
        $amoblado = trim(Input::get('amoblado'));
        $lineablanca = trim(Input::get('lineablanca'));
        $piscina = trim(Input::get('piscina'));
        $status = trim(Input::get('status'));

        $results = array();

        if($price_from ==''){
            $price_from = 0;
        }
        if($price_at == ''){
            $price_at = 10000000;
        }

        $properties = Property::where('code','like','%'.$code.'%')
                      ->orWhere('description','like','%'.$value.'%')
                      ->orWhere('address','like','%'.$value.'%')
                      ->orWhere('sector','like','%'.$value.'%')
                      ->orWhere('city','like','%'.$value.'%')
                      ->orWhere('state','like','%'.$value.'%')
                      ->get();

        $properties2 = Sign::where('code','like','%'.$code.'%')->get();

        if($properties2){
            foreach ($properties2 as $item) {
                $properties[]=$item->property;
            }
        }

        // Filtrar por tipo de propiedad y precio
        $resultado =array();

        // Si es diferente de null, significa que el usuario selecciono filtrar por renta o venta las propiedades
        if($status != ''){

            if($status == 'renta'){

                // Filtrar las propiedades por renta
                foreach ($properties as $item) {
                    if($propertyType!=""){
                        if(  $item->property_type_id == $propertyType && $price_from <= $item->precioAlquiler && $item->precioAlquiler <= $price_at  ){

                            $resultado[] = $item;
                        }
                        
                    }else{
                        if( $price_from <= $item->precioAlquiler && $item->precioAlquiler <= $price_at ){

                            $resultado[] = $item;
                        }
                    }
                }

            }

            if($status == 'venta'){

                // Filtrar las propiedades por venta
                foreach ($properties as $item) {
                    if($propertyType!=""){
                        if(  $item->property_type_id == $propertyType && $price_from <= $item->precioVenta && $item->precioVenta <= $price_at  ){

                            $resultado[] = $item;
                        }
                        
                    }else{
                        if( $price_from <= $item->precioVenta && $item->precioVenta <= $price_at ){

                            $resultado[] = $item;
                        }
                    }
                }

            }

        }else{
            
            // Filtrar las propiedades por venta o renta
            foreach ($properties as $item) {
                if($propertyType!=""){
                    if( ( $item->property_type_id == $propertyType ) && ( ( ($price_from <= $item->precioVenta ) && ($item->precioVenta<=$price_at)) || ( ($price_from<=$item->precioAlquiler) && ($item->precioAlquiler<=$price_at) ) ) ){

                        $resultado[] = $item;
                    }
                    
                }else{
                    if(  ( ( ($price_from <= $item->precioVenta ) && ($item->precioVenta<=$price_at)) || ( ($price_from<=$item->precioAlquiler) && ($item->precioAlquiler<=$price_at) ) ) ){

                        $resultado[] = $item;
                    }
                }
            }

        }

        // Filtrar por cantidad de cuartos
        $resultado2 = array();
        if($cuartos != ''){
            foreach($resultado as $item){
                foreach($item->features_qty as $detail){
                    if($detail->feature->name == 'Cuartos' and $detail->valor==$cuartos){
                        $resultado2[] = $item;
                    }
                }
            }            
        }else{
            $resultado2 = $resultado;
        }

        // Filtrar por cantidad de baños
        $resultado3 = array();
        if($banos != ''){
            foreach($resultado2 as $item){
                foreach($item->features_qty as $detail){

                    if($detail->feature->name == 'Baños' and $detail->valor == $banos){
                        $resultado3[] = $item;
                    }
                }
            }          
        }else{
            $resultado3 = $resultado2;
        }

        // Filtrar si esta amoblado
        $resultado4 = array();
        if($amoblado == 'si'){
             foreach($resultado3 as $item){
                foreach($item->features_qty as $detail){

                    if($detail->feature->name == 'Amueblado'){
                        $resultado4[] = $item;
                    }
                }
            }            
        }else{
            $resultado4 = $resultado3;
        }

        // Filtrar si esta si tiene lineablanca
        $resultado5 = array();
        if($lineablanca == 'si'){
             foreach($resultado4 as $item){
                foreach($item->features_qty as $detail){

                    if($detail->feature->name == 'Línea Blanca'){
                        $resultado5[] = $item;
                    }
                }
            }            
        }else{
            $resultado5 = $resultado4;
        }

        // Filtrar si esta si tiene piscina
        $resultado6 = array();
        if($piscina == 'si'){
             foreach($resultado5 as $item){
                foreach($item->features_qty as $detail){

                    if($detail->feature->name == 'Piscina'){
                        $resultado6[] = $item;
                    }
                }
            }            
        }else{
            $resultado6 = $resultado5;
        }


        // Preformar la salida
        foreach($resultado6 as $item){
            if($item->exclusividad){
                $exclusividad = 'Si';
            }else{
                $exclusividad = 'No';
            }
            if($item->checked){
                $checked = 'Revisado';
                
            }else{
                $checked = 'Pendiente';
                
            }
            if($item->published){
                $status = 'Publicado';
               
            }else{
                $status = 'Despublicado';
                
            }
            $class ="";

            if( ( date('Y', strtotime($item->created_at) ) >= 2018 ) && ( !$item->published || !$item->checked )){
                $class = "despublished-green";
            }

            if( ( date('Y', strtotime($item->created_at) ) < 2018 ) && ( !$item->published || !$item->checked )){
                $class = "despublished";
            }

            if($item->estadoTmp=='vendida'){
                $class = 'vendida';
            }

            if($item->sign){
                $sign = strtoupper( array_get($item->sign,'0.code') );
            }else{
                $sign = '';
            }

            if(count($item->user)>0){
                $contact = ucwords( $item->user->get(0)->user->name );
            }else{
                $contact = 'No contact';
            }

            $results[]=[
                'id' => $item->id,
                'code' => strtoupper( $item->code ),
                'inmueble' => $item->property_type->name.' en '.strtolower( $item->ventaRenta ). ' en el sector de '.ucfirst( $item->sector).'<br>'. ucfirst($item->city).', '.ucfirst($item->state),
                'exclusivo' => $exclusividad,
                'pv' => number_format($item->precioVenta,2,'.',','),
                'pr' => number_format($item->precioAlquiler,2,'.',','),
                'rotulo' => $sign,
                'checked' => $checked,
                'status' => $status,
                'estadoTmp' => ucfirst($item->estadoTmp),
                'class' => $class,
                'created' => $item->created_at,
                'updated' => $item->updated_at,
                'contacto'=> $contact,
            ];
        }

        return response()->json($results);
    }

    public function getProperty() {
        try{
            $code_id = strtoupper( trim( Input::get('term') ) );
            $results = array();

            $codes = Property::where('code', 'LIKE', '%'.$code_id.'%')->where('visible',true)->where('published',true)->where('checked',true)->get()->take(10);

            foreach ($codes as $item) {
                $results[] = [
                    'value' => $item->code,
                ];
            }

            return response()->json($results);

        }catch(Exception $e){
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function checkTestimonial() {
        try{
            $testimonial = Testimonial::find(Input::get('id'));
            $testimonial->notes = strtolower(trim(Input::get('notes')));
            $testimonial->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $testimonial->update();

            return response()->json('¡Registro actualizado!');

        }catch(Exception $e){
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return response()->json($e);
        }
    }

    public function saveTestimonial() {
        try{
            $testimonial = new Testimonial;
            $testimonial->user_id = Input::get('user_id');
            $testimonial->notes = strtolower(trim(Input::get('notes')));
            $testimonial->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $testimonial->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $testimonial->save();

            return response()->json('¡Registro creado!');

        }catch(Exception $e){
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return response()->json($e);
        }
    }

    public function getPropertyDetail(){
        try{
            
            $id = Input::get('id');

            $property = Property::find($id);
            // dd($property->user->get(0)->user);

            if($property){
                $result = array();

                $contact=array();
                
                foreach($property->user as $item){
                    $contact[] = [
                        'customer_id'          => $item->user_id,
                        'name'                 => ucwords($item->user->name),
                        'house'                => $item->user->house,
                        'office'               => $item->user->office,
                        'phone'                => $item->user->phone,
                        'email'                => $item->user->email,
                        // 'relacion'             => $item->user->landowner_type->code,
                        'comments'             => ucfirst($item->user->comments),
                    ];
                };

                $result['contacts'] = $contact;

                $result['code']                 = $property->code;

                if(count($property->sign)>0){
                    $result['sign']  = array_get($property->sign,'0.code');
                }else{
                    $result['sign'] = '---';
                }

                if(empty($property->code_id)){
                    $result['sismobilia'] = '---';
                }else{
                    $result['sismobilia'] = $property->code_id;
                } 

                $result['property_type']        = ucfirst($property->property_type->name);
                $result['venta_renta']          = ucfirst($property->ventaRenta);
                if($property->exclusividad == 'true') {
                    $result['exclusividad']     = 'Si';
                } else {
                    $result['exclusividad']     = 'No'; 
                }
                if($property->disponible == 'true') {
                    $result['disponible']     = 'Si';
                } else {
                    $result['disponible']     = 'No';
                }
                if($property->acceso){
                    $acceso = 'Facil acceso';
                }else{
                    $acceso = 'Dificil acceso';
                }


                // Preformar el sector
                $sector = explode('/',$property->sector);
                $cadena = explode(' ',$sector[0]);
                $string = '';

                for($i=0;$i<count($cadena);$i++){

                    if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                        $string .= $cadena[$i].' ';
                    }else{
                        $string .= ucfirst($cadena[$i]).' ';
                    }

                }
                $result['estadoTmp']            = ucwords($property->estadoTmp);
                $result['precioVenta']          = 'U$' . number_format($property->precioVenta,2,'.',',');
                $result['precioRenta']          = 'U$' . number_format($property->precioAlquiler,2,'.',',');
                $result['precioxV2']            = 'U$' . number_format($property->precioPorV2,2,'.',',');
                $result['areaTotal']            = isset($property->areaTotal)? $property->areaTotal : 'No definido';
                $result['areaConstruida']       = isset($property->areaConstruccion)? $property->areaConstruccion: 'No definido';
                $result['antiguedad']           = isset($property->antiguedad) ? $property->antiguedad : 'No definido';
                $result['acceso']               = isset($acceso)? $acceso : 'No definido';
                if(empty($property->topografia)){
                    $result['topografia']  = 'No definido';
                }else{
                    $result['topografia']  = $property->topografia;
                }
                $result['description']          = isset($property->description) ? $property->description : 'Sin descripción';
                $result['address']              = isset($property->address) ? $property->address : 'No definida';
                $result['sector']               = isset($string) ? $string : 'No definido';
                $result['city']                 = isset($property->city) ? ucfirst($property->city): 'No definido';
                $result['state']                = isset($property->state) ? ucfirst($property->state): 'No definido';
                $result['country']              = isset($property->country) ? ucfirst($property->country): 'No definido';
                $result['latitude']             = $property->latitude;
                $result['longitude']            = $property->longitude;
                $result['status']               = isset($property->status->name)? ucfirst($property->status->name) : 'No definido';
                $result['notes']                = ucfirst($property->notes);
                $result['comision']             = isset($property->comision)? $property->comision.'%': 'No definido';

                $result['agente']               = ucwords($property->agent->name);
                $result['photos']               = $property->photos;
                $result['created']              = date($property->created_at);

                if( date($property->created_at) == date($property->updated_at)){
                    $result['updated']              = 'Sin actualización';
                }else{
                    $result['updated']              = date($property->updated_at);
                    
                }

                $features = FeatureProperty::where('property_id',$id)->get();
                // dd($features);
                $caracteristicas = array();

                foreach($features as $item){
                    $caracteristicas[] = [
                        'feature' => $item->feature->name,
                        'valor' => $item->valor, 
                    ];
                }

                // $features = Room::where('property_id', $property->id)->get();
                // // dd($features);
                // foreach($features as $item){
                //     $caracteristicas[] = [
                //         'feature' => 'Cuartos',
                //         'valor' => $item->quantity, 
                //     ];
                // }

                
                $result['features']             = $caracteristicas;

            }else{
                $result ="not_found";

            }

            return response()->json($result);

        }catch(Exception $e){
            return response()->json($e);
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    public function getProperties_by_keyword(){

        $id         = trim( Input::get('id'));
        $codigo     = strtoupper( trim( Input::get('codigo') ) );
        $rotulo     = strtoupper( trim( Input::get('rotulo') ) );
        $categoria  = ucfirst( trim( Input::get('categoria') ) );
        $sectorArray     = explode ( ',' , strtolower( trim( Input::get('sector') ) ) );
        if (count($sectorArray) > 0){
            $sector     = $sectorArray[0];
        }else{
            $sector =  strtolower( trim( Input::get('sector') ) );
        }
        $ciudad     = strtolower( trim( Input::get('ciudad') ) );
        $departamento = strtolower( trim( Input::get('departamento') ) );
        $contacto   = trim( Input::get('contacto'));
        $ventaRenta = trim( Input::get('ventaRenta'));
        $pventa     = trim( Input::get('pventa'));
        $prenta     = trim( Input::get('prenta'));
        $pvara      = trim( Input::get('pvara'));
        $cuartos    = trim( Input::get('cuartos'));
        $banos      = trim( Input::get('banos'));
        // $amueblado  = trim( Input::get('amueblado'));
        // $piscina    = trim( Input::get('piscina'));
        // $lineablanca = trim( Input::get('lineablanca'));
        $atotal     = trim( Input::get('atotal'));
        $aconstruida = trim( Input::get('aconstruida'));
        $status     = trim( Input::get('status'));
        $created    = trim( Input::get('created'));
        $updated    = trim( Input::get('updated'));

        // Venta/Renta
        if($ventaRenta == ''){
            $ventaRenta_select = array('venta','renta','venta/renta');
        }else{
            $ventaRenta_select = array($ventaRenta,'venta/renta');
        }

        // Precio venta
        if($pventa == ""){
            $pventa_from = 0;
            $pventa_at = 10000000;
        }else{
            $precio = explode('-', $pventa);
            $pventa_from = $precio[0];
            $pventa_at = $precio[1];
        }

        // Precio renta
        if($prenta == ""){
            $prenta_from = 0;
            $prenta_at = 1000000;
        }else{
            $precio = explode('-', $prenta);
            $prenta_from = $precio[0];
            $prenta_at = $precio[1];
        }

        // Precio vara cuadrada
        if($pvara == ""){
            $pv2_from = 0;
            $pv2_at = 100000;
        }else{
            $pv2_from = $pvara;
            $pv2_at = $pvara;
        }
        

        // Area total
        if($atotal == ''){
            $atotal_from = 0;
            $atotal_at = 1000000000;
        }else{
            $atotal_from = $atotal;
            $atotal_at = $atotal;
        }


        $results = array();

        if( $id != "" ){
            $properties = Property::where('id', $id)->get();
        }elseif( $codigo != "" ){
            $properties = Property::where('code','like','%'.$codigo.'%')->get();
        }elseif( $rotulo != "" ){
            $properties = Property::whereHas(
                            'sign', function($q) use ($rotulo){
                                $q->where('code', 'like' ,'%'.$rotulo.'%');
                            }
                        )->get();
        }else{


            if($pventa != ""){

                $properties = Property::whereHas('property_type',function($q) use($categoria){
                                    $q->where('name','like', '%'.$categoria.'%');
                                }
                            )
                            ->Where(function ($query) use ($sector, $ciudad, $departamento){
                                        $query->Where('sector','like','%'.$sector.'%')
                                                ->Where('city','like','%'.$ciudad.'%')
                                                ->Where('state','like','%'.$departamento.'%')
                                                ->orwhere('address','like','%'.$sector.'%')
                                                ->orwhere('address','like',$sector); 
                                    })
                            ->whereIn('ventaRenta',$ventaRenta_select)
                            ->whereBetween('precioVenta',[$pventa_from, $pventa_at])
                            // ->where('estadoTmp','like','disponible%')
                            ->orderBy('id','desc')
                            ->get();

            }elseif($prenta != ""){

                $properties = Property::whereHas('property_type',function($q) use($categoria){
                                    $q->where('name','like', '%'.$categoria.'%');
                                }
                            )
                            ->Where(function ($query) use ($sector, $ciudad, $departamento){
                                        $query->Where('sector','like','%'.$sector.'%')
                                                ->Where('city','like','%'.$ciudad.'%')
                                                ->Where('state','like','%'.$departamento.'%')
                                                ->orwhere('address','like','%'.$sector.'%')
                                                ->orwhere('address','like',$sector); 
                                    })
                            ->whereIn('ventaRenta',$ventaRenta_select)
                            ->whereBetween('precioAlquiler',[$prenta_from, $prenta_at])
                            // ->where('estadoTmp','like','disponible%')
                            ->orderBy('id','desc')
                            ->get();

            }elseif($pvara != ""){

                $properties = Property::whereHas('property_type',function($q) use($categoria){
                                    $q->where('name','like', '%'.$categoria.'%');
                                }
                            )
                            ->Where(function ($query) use ($sector, $ciudad, $departamento){
                                        $query->Where('sector','like','%'.$sector.'%')
                                                ->Where('city','like','%'.$ciudad.'%')
                                                ->Where('state','like','%'.$departamento.'%')
                                                ->orwhere('address','like','%'.$sector.'%')
                                                ->orwhere('address','like',$sector); 
                                    })
                            ->whereIn('ventaRenta',$ventaRenta_select)
                            ->whereBetween('precioPorV2',[$pv2_from, $pv2_at])
                            // ->where('estadoTmp','like','disponible%')
                            ->orderBy('id','desc')
                            ->get();
                            
            }else{

                $properties = Property::whereHas('property_type',function($q) use($categoria){
                                    $q->where('name','like', '%'.$categoria.'%');
                                }
                            )
                            ->Where(function ($query) use ($sector, $ciudad, $departamento){
                                        $query->Where('sector','like','%'.$sector.'%')
                                                ->Where('city','like','%'.$ciudad.'%')
                                                ->Where('state','like','%'.$departamento.'%')
                                                ->orwhere('address','like','%'.$sector.'%')
                                                ->orwhere('address','like',$sector); 
                                    })
                            ->whereIn('ventaRenta',$ventaRenta_select)
                            // ->where('estadoTmp','like','disponible%')
                            ->orderBy('id','desc')
                            ->get();
            }
        }


        // Preformar la salida
        foreach($properties as $item){

            if($item->checked){
                $checked = 'Revisado';
                
            }else{
                $checked = 'Pendiente';   
            }

            if($item->published){
                $status = 'Publicado';
               
            }else{
                $status = 'Despublicado';
                
            }

            $class ="";

            if( ( date('Y', strtotime($item->created_at) ) >= 2018 ) && ( !$item->published || !$item->checked )){
                $class = "despublished-green";
            }

            if( ( date('Y', strtotime($item->created_at) ) < 2018 ) && ( !$item->published || !$item->checked )){
                $class = "despublished";
            }

            if($item->estadoTmp=='vendida'){
                $class = 'vendida';
            }

            if($item->sign){
                $sign = strtoupper( array_get($item->sign,'0.code') );
            }else{
                $sign = '';
            }

            if(count($item->user)>0){
                $contact = ucwords( $item->user->get(0)->user->name );
            }else{
                $contact = 'No contact';
            }

            if($item->precioVenta>0){ $pventa = 'U$ '. number_format($item->precioVenta,2,'.',','); }else{ $pventa = ''; };
            if($item->precioAlquiler>0){ $prenta = 'U$ '. number_format($item->precioAlquiler,2,'.',','); }else{ $prenta = ''; };
            if($item->precioPorV2>0){ $pvara = 'U$ '. number_format($item->precioPorV2,2,'.',','); }else{ $pvara = ''; };

            
            if($item->room){
                $rooms = $item->room->quantity;
            }else{
                $rooms = "";
            }

            $bathrooms='';
            $amueblado='';
            $lineab = '';
            $piscina = '';

            $features = \DB::table('feature_property')->where('property_id',$item->id)->get();
            if($features){
                foreach($features as $feature){
                    if($feature->feature_id==7){
                        $bathrooms=$feature->valor;
                    }
                    if($feature->feature_id==26){
                        $amueblado='Si';
                    }
                    if($feature->feature_id==25){
                        $lineab='Si';
                    }
                    if($feature->feature_id==17){
                        $piscina='Si';
                    }
                }
            }

            $results[]=[
                'id'        => $item->id,
                'code'      => strtoupper( $item->code ),
                'rotulo'    => $sign,
                'categoria' => $item->property_type->name,
                'sector'    => ucfirst( $item->sector),
                'ciudad'    => ucfirst($item->city),
                'departamento' => ucfirst($item->state),
                'contacto'  => $contact,
                'ventaRenta' => strtolower( $item->ventaRenta ),
                'pventa'    => $pventa,
                'prenta'    => $prenta,
                'pvara'     => $pvara,
                'cuartos'   => $rooms,
                'banos'     => $bathrooms,
                'amueblado' => $amueblado,
                'piscina'   => $piscina,
                'lineab'    => $lineab,
                'atotal'    => isset($item->areaTotal) ? $item->areaTotal ." ". $item->measure_unit->short_name : "" ,
                'aconstruida' => isset($item->areaConstruccion) ? $item->areaConstruccion ." ". $item->measure_unit->short_name : "",
                'estadoTmp' => ucfirst($item->estadoTmp),
                'created'   => date($item->created_at),
                'updated'   => date($item->updated_at),
                                
                'checked' => $checked,
                'status' => $status,
                'class' => $class,
                'sectorValue' => Input::get('sector')
            ];

        }

        return response()->json($results);
        // return response()->json(Input::get('sector'));
    }
}