<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;
use App\Testimonial;
use App\Property;

use Mail;
use Image;

class LandOwnerController extends Controller
{
    public function index()
    {
        try {
            $owners = User::whereHas('roles', function($q){
                                $q->where('name', 'user');
                            }
                        )->where('visible','true')->orderBy('id','desc')->paginate(250);

            return view('controlpanel.landowner.index',[
                'title' => 'Propietarios',
                'description' => 'Listado de propietarios o contacto asociado a un inmueble',
                'owners' => $owners,
                'menu' => $this->menu('clientes'),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{

            return view('controlpanel.landowner.create',[
                'title' => 'Crear nuevo',
                'menu' => $this->menu('create'),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        // Verificar si el usuario o el correo ya existe en el sistema
        $identification = str_replace(' ','', trim($request->identification));

        $user = User::where('name',strtolower(trim($request->name)))->orWhere('identification',$identification)->first();
        // dd($user);
            if($user){
                \Session::flash('error_message','¡El registro ya existe, favor verifique!');
                return redirect('ControlPanel/clientes/'.$user->id);
            }

            $pass = str_random(25);
            $e_pass = bcrypt($pass);

            $password = $e_pass;

            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $filename = time().'.'.$avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300,300)->save(public_path('/img/user/'.$filename));

            }else{
                $filename = 'default.jpg';
            }

            $new            = new User;
            $new->name      = strtolower(trim($request->name));
            $new->email     = trim($request->email);
            $new->checked   = $request->checked;
            $new->identification = $identification;
            $new->password  = $e_pass;
            $new->house     = $request->house;
            $new->office    = $request->office;
            $new->phone     = $request->phone;
            $new->comments  = strtolower(trim($request->comments));
            $new->photo     = $filename;
            $new->save();

            $role = Role::where('name', 'user')->first();

            $new->attachRole($role);

            // Mail::send('email.user_notification',[
            //         'user' => $new,
            //         'pass' => $pass,
            //     ], function($m) use($new, $pass) {
            //         $m->from('cortega@dinsagroup.com', 'Momotombo Real State');
            //         $m->to($new->email, $new->name);
            //     });

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            return redirect('ControlPanel/clientes/'.$new->id);
    }

    public function show($id)
    {
        try{

            $owner = User::find($id);
            $testimonial = Testimonial::where('user_id', $id)->first();
            
            // if(!\Session::has('user_id')) \Session::put('user_id', $id);
            // dd($owner->properties->get(0)->property);            
            return view('controlpanel.landowner.show',[
                'title' => $owner->name,
                'owner' => $owner,
                'testimonial' => $testimonial,
                'properties' => $owner->properties,
                'menu' => $this->menu('clientes'),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try{

            $user = User::find($id);

            return view('controlpanel.landowner.edit',[
                'title' => 'Editar registro',
                'user' => $user,
                'menu' => $this->menu('clientes'),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{

            $update = User::find($id);
            $identification = str_replace(' ','', trim($request->identification));
            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $filename = time().'.'.$avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300,300)->save(public_path('/img/user/'.$filename));
                $update->photo = $filename;
            }

            $pass = str_random(25);
            $e_pass = bcrypt($pass);

            $email = $request->email;
            $user_email = $update->email;
            if($email != $user_email) {
                $update->email = $request->email;
                $update->password = $e_pass;
                // Mail::send('email.user_notification',[
                //     'user' => $update,
                //     'pass' => $pass,
                // ], function($m) use($update, $pass) {
                //     $m->from('cortega@dinsagroup.com', 'Momotombo Real State');
                //     $m->to($update->email, $update->name);
                // });
            }
            $update->identification = $identification;
            $update->name       = strtolower(trim($request->name));
            $update->house      = $request->house;
            $update->office     = $request->office;
            $update->phone      = $request->phone;
            $update->comments   = strtolower(trim($request->comments));
             $update->checked   = $request->checked;
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            return redirect('ControlPanel/clientes/'.$id);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
         try{

            $user = User::find($id);
            $user->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/clientes');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu($level2){
        $menu = [
                'level_1' => 'clientes',
                'level_2' => $level2,
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
