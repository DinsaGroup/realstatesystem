<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\LandownerType;

class LandownerTypeController extends Controller
{
    
    public function index()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            $landownertypes = LandownerType::where('visible','true')->OrderBy('code','asc')->get();
            return view('controlpanel.landownertypes.index',[
                    'title' => 'Tipo de contacto',
                    'menu' => $this->menu(),
                    'landownertypes' => $landownertypes,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            return view('controlpanel.landownertypes.create',[
                    'title' => 'Crear nuevo',
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{
            $new = new landownertype;
            $new->code = $request->code;
            $new->description = $request->description;
            $new->save();

            \Session::flash('success_message','¡La unidad se ha creado con éxito!');
            return redirect('ControlPanel/tipo-contacto');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            $landownertypes = LandownerType::find($id);
            return view('controlpanel.landownertypes.edit',[
                    'title' => 'Editar tipo contacto',
                    'menu' => $this->menu(),
                    'landownertypes' => $landownertypes,
                ]);
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $update = LandownerType::find($id);
            $update->name = $request->name;
            $update->short_name = $request->short_name;
            $update->slug = str_slug($request->name,"-");
            $update->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $update->update();

            \Session::flash('success_message','¡La unidad se ha actualizado con éxito!');
            return redirect('ControlPanel/tipo-contacto');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try{
            $destroy = LandownerType::find($id);
            $destroy->visible = 'false';
            $destroy->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $destroy->update();

            \Session::flash('success_message','¡La unidad se ha eliminado con éxito!');
            return redirect('ControlPanel/tipo-contacto');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => 'tipo-contacto',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
