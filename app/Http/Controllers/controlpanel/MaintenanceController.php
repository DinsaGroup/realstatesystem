<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

use App\Property;
use App\User;

class MaintenanceController extends Controller
{
    
    public function index()
    {
        
        if(Auth::user()->hasRole('superadmin')){

            return view('controlpanel.maintenance', [
                'title' => 'Mantimiento sistema',
                'menu' => $this->menu('configuraciones','mantenimiento'),
            ]);

        }else{
            return redirect('ControlPanel');
        }
    }

    public function maintenance($tabla){

        if($tabla ==='properties'){
        
            $properties = Property::all();

            // $properties = Property::whereBetween('id',[1001,3000])->get();
            
            foreach($properties as $item){
                $item->ventaRenta = strtolower(trim($item->ventaRenta));
                $item->topografia = strtolower(trim($item->topografia));
                $item->description = strtolower(trim($item->description));
                $item->address = strtolower(trim($item->address));
                $item->sector = strtolower(trim($item->sector));
                $item->city = strtolower(trim($item->city));
                $item->state = strtolower(trim($item->state));
                $item->country = strtolower(trim($item->country));
                $item->notes = strtolower(trim($item->notes));
                $item->estadoTmp = strtolower(trim($item->estadoTmp));
                $item->metaTitleEs = strtolower(trim($item->metaTitleEs));
                $item->metaTitleEn = strtolower(trim($item->metaTitleEn));
                $item->metaKeywordEs = strtolower(trim($item->metaKeywordEs));
                $item->metaKeywordEn = strtolower(trim($item->metaKeywordEn));
                $item->update();
            }
        }

        if($tabla === 'users'){

            $owners = User::whereHas('roles', function($q){
                                $q->where('name', 'user');
                            }
                        )->where('visible','true')->get();

            foreach ($owners as $item) {

                if( count($item->properties) == 0){
                    $item->visible = 'false';
                    $item->update();
                }

            }

        }

        if($tabla === 'optimizacion_users'){
            $users = User::all();
            foreach($users as $item){
                $item->name = strtolower(trim($item->name));
                $item->update();
            }
        }


        // foreach($properties as $item){
            
        //     if (strpos($item->estadoTmp, 'alquilado') || strpos($item->estadoTmp, 'alquilada') > -1) {
        //         $item->estadoTmp = 'alquilado';
        //         $item->published = false;
        //     }else if( strpos($item->estadoTmp, 'vendida') || strpos($item->estadoTmp, 'vendido') > -1){
        //         $item->estadoTmp = 'vendida';
        //         $item->published = false;
        //     }else{
        //         $item->estadoTmp = 'disponible';
        //         $item->published = true;
        //     }
        //     $item->update();
        // }

        \Session::flash('success_message','¡El último registo se guardo correctamente!');
        return redirect('ControlPanel/mantenimiento');

    }

    private function menu($level1,$level2){
        $menu = [
                'level_1' => $level1,
                'level_2' => $level2,
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }

}
