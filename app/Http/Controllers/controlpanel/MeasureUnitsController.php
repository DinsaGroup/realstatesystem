<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\MeasureUnit;

class MeasureUnitsController extends Controller
{
    public function index()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            $measureunit = MeasureUnit::where('visible','true')->OrderBy('name','asc')->get();
            return view('controlpanel.measureunits.index',[
                    'title' => 'Unidades de Medida',
                    'menu' => $this->menu(),
                    'measureunit' => $measureunit,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            return view('controlpanel.measureunits.create',[
                    'title' => 'Crear nueva unidad de medida',
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{
            $new = new MeasureUnit;
            $new->name = $request->name;
            $new->short_name = $request->short_name;
            $new->slug = str_slug($request->name,"-");
            $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->save();

            \Session::flash('success_message','¡La unidad se ha creado con éxito!');
            return redirect('ControlPanel/unidades-de-medida');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            $measureunits = MeasureUnit::find($id);
            return view('controlpanel.measureunits.edit',[
                    'title' => 'Editar unidad de medida',
                    'menu' => $this->menu(),
                    'measureunits' => $measureunits,
                ]);
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $update = MeasureUnit::find($id);
            $update->name = $request->name;
            $update->short_name = $request->short_name;
            $update->slug = str_slug($request->name,"-");
            $update->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $update->update();

            \Session::flash('success_message','¡La unidad se ha actualizado con éxito!');
            return redirect('ControlPanel/unidades-de-medida');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try{
            $destroy = MeasureUnit::find($id);
            $destroy->visible = 'false';
            $destroy->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $destroy->update();

            \Session::flash('success_message','¡La unidad se ha eliminado con éxito!');
            return redirect('ControlPanel/unidades-de-medida');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => 'unidades-de-medida',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
