<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Photo;
use App\Property;

use Auth;

use Image;

class PhotosController extends Controller
{
    public function index(){
        try{
            $photos = Photo::where('visible','true')->OrderBy('id','asc')->get();
            return view('controlpanel.photos.index',[
                    'title' => 'Fotos',
                    'menu' => $this->menu(),
                    'photos' => $photos,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create($id){
        try{
            
            return view('controlpanel.photos.create',[
                    'title' => 'Agregar fotos',
                    'menu' => $this->menu(),
                    'id' => $id,
                    
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request){
        try{

            $count = 1;

            if($request->hasFile('file1')){
                $photo = $request->file('file1');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file2')){
                $photo = $request->file('file2');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file3')){
                $photo = $request->file('file3');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file4')){
                $photo = $request->file('file4');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file5')){
                $photo = $request->file('file5');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file6')){
                $photo = $request->file('file6');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file7')){
                $photo = $request->file('file7');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file8')){
                $photo = $request->file('file8');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file9')){
                $photo = $request->file('file9');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file10')){
                $photo = $request->file('file10');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file11')){
                $photo = $request->file('file11');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }

            if($request->hasFile('file12')){
                $photo = $request->file('file12');
                $filename = time().$count.'.'.strtolower($photo->getClientOriginalExtension());
                #Image::make($photo)->resize(720, 405)->save(public_path('/img/properties/'.$filename));
                Image::make($photo)->resize(720, 405)->save('img/properties/'.$filename);
                $new = new Photo;
                $new->property_id = $request->property_id;
                $new->filename = $filename;
                $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new->save();
                $count++;
            }


            $property = Property::find($request->property_id);
            // dd($property->user->get(0)->user_id);

            \Session::flash('success_message','¡La unidad se ha creado con éxito!');
            return redirect('ControlPanel/clientes/'.$property->user->get(0)->user_id);

        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        $photo = Photo::find($id);
        $photo->visible = 'false';
        $photo->update();

        \Session::flash('success_message','¡La unidad se ha creado con éxito!');
        return redirect()->back();
    }

    public function deletephoto()
    {
        $id = \Input::get('id');
        $photo = Photo::find($id);
        $photo->visible = 'false';
        $photo->update();

        return response()->json('ok');
        // return redirect()->back();
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => '',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
