<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ImageUploadRequest;

use Auth;
use Image;
use App\Property;
use App\Feature;
use App\PropertyType;
use App\Status;
use App\MeasureUnit;
use App\User;
use App\FeatureProperty;
use App\Pool;
use App\AirConditionerType;
use App\WaterHeaterType;
use App\Room;
use App\Restroom;
use App\LandownerType;
use App\PropertyUser;
use App\Sign;


class PropertiesController extends Controller
{
    public function index()
    {
        try{
            
            $properties = Property::OrderBy('id', 'desc')->paginate(100);
            // $properties = Property::where('visible', true)->where('published', true)->where('checked',true)->OrderBy('id', 'asc')->get();
            $propertyTypes = PropertyType::where('visible','true')->OrderBy('name','asc')->pluck('name','id');

            return view('controlpanel.properties.index',[
                    'title' => 'Propiedades publicadas',
                    'subtitle' => 'Estas son las propiedades publicas y revisadas, que todos los usuarios del sistema puede ver, tanto en la web como dentro del sistema.<br> En caso que una propiedad no se encuentre en esta lista, es posible que deba ser buscada en la lista de las propiedades por revisar o se encuentre despublicada.',
                    'menu' => $this->menu('propiedades','todas'),
                    'properties' => $properties,
                    'propertyTypes' => $propertyTypes,
                    
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function vendidas()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            $properties = Property::where('estadoTmp', 'vendida')->OrderBy('id', 'asc')->paginate(100);
            // $properties = Property::where('published', false)->OrderBy('id', 'asc')->get();
            $propertyTypes = PropertyType::where('visible','true')->OrderBy('name','asc')->pluck('name','id');
            
            return view('controlpanel.properties.index',[
                    'title' => 'Propiedades vendidas',
                    'subtitle' => 'Estas propiedades no son visibles en los resultados de busqueda, ni están disponibles a los agentes del sistema.<br> Solo son visibles para los administradores.',
                    'menu' => $this->menu('propiedades','vendidas'),
                    'properties' => $properties,
                    'propertyTypes' => $propertyTypes,
                    
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function alquiladas()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            $properties = Property::where('estadoTmp', 'alquilado')->OrderBy('id', 'asc')->paginate(100);
            // $properties = Property::where('published', false)->OrderBy('id', 'asc')->get();
            $propertyTypes = PropertyType::where('visible','true')->OrderBy('name','asc')->pluck('name','id');
            
            return view('controlpanel.properties.index',[
                    'title' => 'Propiedades alquiladas',
                    'subtitle' => 'Estas propiedades no son visibles en los resultados de busqueda, ni están disponibles a los agentes del sistema.<br> Solo son visibles para los administradores.',
                    'menu' => $this->menu('propiedades','alquiladas'),
                    'properties' => $properties,
                    'propertyTypes' => $propertyTypes,
                    
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function despublished()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            $properties = Property::where('published', false)->OrderBy('id', 'asc')->paginate(100);
            // $properties = Property::where('published', false)->OrderBy('id', 'asc')->get();
            $propertyTypes = PropertyType::where('visible','true')->OrderBy('name','asc')->pluck('name','id');
            
            return view('controlpanel.properties.index',[
                    'title' => 'Propiedades despublicadas',
                    'subtitle' => 'Estas propiedades no son visibles en los resultados de busqueda, ni están disponibles a los agentes del sistema.<br> Solo son visibles para los administradores.',
                    'menu' => $this->menu('propiedades','despublicadas'),
                    'properties' => $properties,
                    'propertyTypes' => $propertyTypes,
                    
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function checked()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            // $properties = Property::where('checked',false)->OrderBy('id', 'asc')->paginate(100);
            $properties = Property::where('checked',false)->OrderBy('id', 'asc')->paginate(100);
            $propertyTypes = PropertyType::where('visible','true')->OrderBy('name','asc')->pluck('name','id');
            
            return view('controlpanel.properties.index',[
                    'title' => 'Propiedades en revisión',
                    'subtitle' => 'Estas son todas las propiedades que son ingresadas al sistema, las cuales necesitan revisión para su publicación.<br>Solo son visibles a los administradores. ',
                    'menu' => $this->menu('propiedades','revision'),
                    'properties' => $properties,
                    'propertyTypes' => $propertyTypes,
                    
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function myproperties(){

        $properties = Property::where('agent_id',Auth::id())->OrderBy('id', 'asc')->paginate(100);
            // $properties = Property::where('checked',false)->OrderBy('id', 'asc')->get();
         $propertyTypes = PropertyType::where('visible','true')->OrderBy('name','asc')->pluck('name','id');    
        return view('controlpanel.properties.index',[
                'title' => 'Mis propiedades ingresadas',
                'subtitle' => 'Estas son todas las propiedades que han sido ingresadas al sistema por usted, posiblemente necesiten revisión y aprobación por su supervisor. ',
                'menu' => $this->menu('propiedades','myproperties'),
                'properties' => $properties,
                'propertyTypes' => $propertyTypes,
                
            ]);
    }

    public function create(){

        try{

            $properties = Property::where('visible','true')->OrderBy('id','asc')->get();
            $features = Feature::where('visible', 'true')->OrderBy('id', 'asc')->get();
            $propertyTypes = PropertyType::where('visible','true')->OrderBy('id','asc')->pluck('name','id');
            $status = Status::where('type','propiedad')->OrderBy('id','asc')->pluck('name','id');
            $measureUnits = MeasureUnit::where('visible','true')->OrderBy('id','asc')->pluck('name','id');
            $landowner_types = LandownerType::where('visible','true')->OrderBy('description','asc')->pluck('description','id');
            $published = 'Pendiente';
            $count = count($features)/4;
            $anos = array();
            $thisyear = date('Y');
            $cuenta = 0;

            $agents = User::whereHas('roles', function($q) {
                $q->where('name', 'agent')->orWhere('name','superadmin')->orWhere('name','admin')->orWhere('name','supervisor');
            })->orderBy('name','asc')->get();

            foreach ($agents as $item) {
                $agentes[$item->id] = ucwords( $item->name );
            }

            for($i=0; $i<100; $i++){
                $year = $thisyear - $i;
                $anos[$year] = $year;
            }

            return view('controlpanel.properties.create',[
                    'title' => 'Agregar nueva propiedad',
                    'title2' => 'Seleccionar características',
                    'title3' => 'SEO',
                    'title4' => 'Notas',
                    'subtitle' => 'Busque su posición actual',
                    'subtitle2' => 'Busque la posición de la propiedad',
                    'menu' => $this->menu('propiedades','create'),
                    'properties' => $properties,
                    'features' => $features,
                    'propertyTypes' => $propertyTypes,
                    'status' => $status,
                    'measureUnits' => $measureUnits,
                    'published' => $published,
                    'count' => $count,
                    'years' => $anos,
                    'cuenta' => $cuenta,
                    'landownerTypes' => $landowner_types,
                    'agents' => $agentes
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{

            $user = User::where('name',strtolower(trim($request->user_name)))->first();

            if(Auth::user()->hasRole('agent')){
                $published = false;
            }else{
                $published = $request->published;
            }

            if(!$user){
                \Session::flash('error_message','¡El usuario no existe, favor revise y vuelva a intentar!');
                return redirect()->back();
            }

            $propertyType = PropertyType::find($request->property_type_id);

            if($request->ventaRenta == 'venta'){
                $code = $propertyType->code . 'V-';
            }elseif($request->ventaRenta == 'renta'){
                $code = $propertyType->code . 'R-';
            }else{
                $code = $propertyType->code . 'VR-';
            };

            if($request->agent_id){
                $agent_id = $request->agent_id;
            }else{
                $agent_id = Auth::id();
            }

            $property = new Property;
            $property->status_id            = $request->status_id;
            $property->agent_id             = $agent_id;
            $property->property_type_id     = $request->property_type_id;
            $property->measure_unit_id      = isset($request->measure_unit_id)? $request->measure_unit_id : 1;
            $property->exclusividad         = $request->exclusividad;
            $property->disponible           = $request->disponible;
            $property->ventaRenta           = strtolower($request->ventaRenta);
            $property->precioVenta          = isset($request->precioVenta)? number_format(str_replace(',', '', $request->precioVenta),'2','.','') : number_format(0,2,'.','') ;
            $property->precioAlquiler       = isset($request->precioAlquiler)? number_format(str_replace(',', '', $request->precioAlquiler),'2','.','') : number_format(0,2,'.','') ;
            $property->precioPorV2          = isset($request->precioPorV2)? number_format(str_replace(',', '', $request->precioPorV2),'2','.','') : number_format(0,2,'.','') ;
            $property->areaTotal            = isset($request->areaTotal)? number_format(str_replace(',', '', $request->areaTotal),'2','.','') : number_format(0,2,'.','') ;
            $property->areaConstruccion     = isset($request->areaConstruccion)? number_format(str_replace(',', '', $request->areaConstruccion),'2','.','') : number_format(0,2,'.','') ;
            $property->antiguedad           = $request->antiguedad;
            $property->acceso               = $request->acceso;
            $property->topografia           = strtolower($request->topografia);
            $property->description          = trim($request->description);
            $property->address              = trim($request->address);
            $property->sector               = strtolower(trim($request->sector));
            $property->city                 = strtolower(trim($request->city));
            $property->state                = strtolower(trim($request->state));
            $property->country              = strtolower(trim($request->country));
            $property->latitude             = $request->latitude;
            $property->longitude            = $request->longitude;
            $property->estadoTmp            = $request->estadoTmp;
            $property->notes                = strtolower(trim($request->notes));
            $property->metaTitleEs          = strtolower(trim($request->metaTitleEs));
            $property->metaTitleEn          = strtolower(trim($request->metaTitleEn));
            $property->metaKeywordEs        = strtolower(trim($request->metaKeywordEs));
            $property->metaKeywordEn        = strtolower(trim($request->metaKeywordEn));
            $property->comision             = $request->comision;
            $property->visible              = true;
            $property->published            = $published;
            $property->created_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $property->updated_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $property->save();

            if( trim($request->rotulo)!='' ){

                $sign = new Sign;
                $sign->property_id = $property->id;
                $sign->code = strtoupper( $request->rotulo );
                $sign->save();
                
            }

            $new_relation               = new PropertyUser;
            $new_relation->property_id  = $property->id;
            $new_relation->user_id      = $user->id;
            $new_relation->usertype_id  = $request->landowner_type_id;
            $new_relation->save();

            // Generar y salvar el codigo de la propiedad
            $property->code = $code.$property->id;
            $property->update();

            //Características
            if($request->feature){

                $feat = Input::get('feature');
                list($keys, $values) = array_divide($feat);

                $caract = Feature::whereIn('id',$keys)->get();

                $largo = $request->largo;
                $ancho = $request->ancho;
                $alto = $request->alto;

                foreach ($caract as $item) {
                    $features = new FeatureProperty;
                    $features->feature_id = $item->id;
                    $features->property_id = $property->id;
                    if ($item->id == 6) {
                        $features->valor = $request->rooms;
                    } 
                    if ($item->id == 7) {
                        $features->valor = $request->restrooms;
                    }
                    if ($item->id == 14) {
                        $features->valor = $request->estacionamiento;
                    }
                    if ($item->id == 17) {
                        $features->descriptor = $largo.' x '.$ancho.' x '.$alto;
                    }

                    if ($item->id == 18) {
                        $features->valor = $request->ac;
                    }

                    if ($item->id == 19) {
                        $features->valor = $request->water_heater;
                    }

                    $features->save();
                }

                $new_pool = new Pool;
                $new_pool->property_id = $property->id;
                $new_pool->largo = $largo;
                $new_pool->ancho = $ancho;
                $new_pool->alto = $alto;
                $new_pool->capacidad = $largo * $ancho * $alto;
                $new_pool->created_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new_pool->updated_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new_pool->save();

                $new_ac = new AirConditionerType;
                $new_ac->property_id = $property->id;
                $new_ac->type = $request->ac;
                $new_ac->created_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new_ac->updated_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new_ac->save();

                $new_water_heater = new WaterHeaterType;
                $new_water_heater->property_id = $property->id;
                $new_water_heater->type = $request->water_heater;
                $new_water_heater->created_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new_water_heater->updated_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new_water_heater->save();

                $new_room = new Room;
                $new_room->property_id = $property->id;
                $new_room->quantity = $request->rooms;
                $new_room->created_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new_room->updated_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new_room->save();

                $new_restroom = new Restroom;
                $new_restroom->property_id = $property->id;
                $new_restroom->quantity = $request->restrooms;
                $new_restroom->created_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new_restroom->updated_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                $new_restroom->save();

            }

            \Session::flash('success_message','¡La unidad se ha creado con éxito!');
            // return redirect('ControlPanel/propiedades');
            return redirect('ControlPanel/clientes/'.$user->id);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try{

            $property = Property::find($id);
            $features = Feature::where('visible', 'true')->OrderBy('id', 'asc')->get();
            $propertyTypes = PropertyType::where('visible','true')->OrderBy('id','asc')->pluck('name','id');
            $status = Status::where('type','propiedad')->OrderBy('id','asc')->pluck('name','id');
            $measureUnits = MeasureUnit::where('visible','true')->OrderBy('id','asc')->pluck('name','id');
            $landowner_types = LandownerType::where('visible','true')->OrderBy('description','asc')->pluck('description','id');
            $published = 'Pendiente';
            $count = count($features)/4;
            $anos = array();
            $thisyear = date('Y');
            $result = DB::table('feature_property')->where('property_id', $id)->get(); 

            for($i=0; $i<100; $i++){
                $year = $thisyear - $i;
                $anos[$year] = $year;
            }

            $estacionamiento = FeatureProperty::where('property_id',$id)->where('feature_id',14)->first();

            $pool = Pool::where('property_id', $property->id)->get();
            $ac = AirConditionerType::where('property_id', $property->id)->get();
            $wh = WaterHeaterType::where('property_id', $property->id)->get();
            $room = Room::where('property_id', $property->id)->get();
            $restroom = Restroom::where('property_id', $property->id)->get();
            $agents = User::whereHas('roles', function($q) {
                $q->where('name', 'agent')->orWhere('name','superadmin')->orWhere('name','admin')->orWhere('name','supervisor');
            })->orderBy('name','asc')->get();

            foreach ($agents as $item) {
                $agentes[$item->id] = ucwords( $item->name );
            }

            return view('controlpanel.properties.edit',[
                    'title' => 'Editar propiedad',
                    'title2' => 'Seleccionar características',
                    'title3' => 'SEO',
                    'title4' => 'Notas',
                    'subtitle' => 'Busque su posición actual',
                    'subtitle2' => 'Busque la posición de la propiedad',
                    'menu' => $this->menu('propiedades','create'),
                    'property' => $property,
                    'features' => $features,
                    'propertyTypes' => $propertyTypes,
                    'status' => $status,
                    'measureUnits' => $measureUnits,
                    'published' => $published,
                    'count' => $count,
                    'years' => $anos,
                    'result' => $result,
                    'pool' => $pool,
                    'ac' => $ac,
                    'wh' => $wh,
                    'room' => $room,
                    'estacionamiento' => isset($estacionamiento->valor)? $estacionamiento->valor : '',
                    'restroom' => $restroom,
                    'landownerTypes' => $landowner_types,
                    'agents' => $agentes,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        // dd($request->landowner_type_id);
        try{
            // dd($request->user_id);
            // $user = User::where('name',strtolower(trim($request->user_name)))->where('visible','true')->orderBy('id','asc')->first();
            $user = User::find($request->user_id);

            $propertyType = PropertyType::find($request->property_type_id);

            if($request->ventaRenta == 'venta'){
                $code = $propertyType->code . 'V-'.$id;
            }elseif($request->ventaRenta == 'renta'){
                $code = $propertyType->code . 'R-'.$id;
            }else{
                $code = $propertyType->code . 'VR-'.$id;
            };

            if( $request->rotulo !='' ){

                $sign = Sign::where('property_id',$id)->first();
                
                if($sign){
                    $sign->code = strtoupper( $request->rotulo );
                    $sign->update();
                }else{
                    $sign = new Sign;
                    $sign->property_id = $id;
                    $sign->code = strtoupper( $request->rotulo );
                    $sign->save();
                }
            }else{
                $sign = Sign::where('property_id',$id)->first();
                if($sign){
                    $sign->delete();
                }
            }

            if($request->agent_id){
                $agent_id = $request->agent_id;
            }else{
                $agent_id = Auth::id();
            }

            $property = Property::find($id);
            $property->status_id            = $request->status_id;
            $property->property_type_id     = $request->property_type_id;
            $property->agent_id             = $agent_id;
            $property->code                 = $code;
            $property->measure_unit_id      = $request->measure_unit_id;
            $property->exclusividad         = $request->exclusividad;
            $property->disponible           = $request->disponible;
            $property->ventaRenta           = strtolower($request->ventaRenta);
            $property->precioVenta          = isset($request->precioVenta)? number_format(str_replace(',', '', $request->precioVenta),'2','.','') : number_format(0,2,'.','') ;
            $property->precioAlquiler       = isset($request->precioAlquiler)? number_format(str_replace(',', '', $request->precioAlquiler),'2','.','') : number_format(0,2,'.','') ;
            $property->precioPorV2          = isset($request->precioPorV2)? number_format(str_replace(',', '', $request->precioPorV2),'2','.','') : number_format(0,2,'.','') ;
            $property->areaTotal            = isset($request->areaTotal)? number_format(str_replace(',', '', $request->areaTotal),'2','.','') : number_format(0,2,'.','') ;
            $property->areaConstruccion     = isset($request->areaConstruccion)? number_format(str_replace(',', '', $request->areaConstruccion),'2','.','') : number_format(0,2,'.','') ;
            $property->antiguedad           = $request->antiguedad;
            $property->acceso               = strtolower(trim($request->acceso));
            $property->topografia           = strtolower(trim($request->topografia));
            $property->description          = trim($request->description);
            $property->address              = trim($request->address);
            $property->sector               = strtolower(trim($request->sector));
            $property->city                 = strtolower(trim($request->city));
            $property->state                = strtolower(trim($request->state));
            $property->country              = strtolower(trim($request->country));
            $property->latitude             = $request->latitude;
            $property->longitude            = $request->longitude;
            $property->estadoTmp            = $request->estadoTmp;
            $property->metaTitleEs          = strtolower(trim($request->metaTitleEs));
            $property->metaTitleEn          = strtolower(trim($request->metaTitleEn));
            $property->metaKeywordEs        = strtolower(trim($request->metaKeywordEs));
            $property->metaKeywordEn        = strtolower(trim($request->metaKeywordEn));
            $property->visible              = true;
            $property->comision             = $request->comision ;
            $property->checked              = $request->checked;
            $property->published            = $request->published;
            $property->notes                = strtolower(trim($request->notes));
            $property->created_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $property->updated_by           = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $property->update();

            $propertyuser = PropertyUser::where('user_id',$user->id)->where('property_id',$id)->first();

            if($propertyuser){

                $propertyuser->usertype_id = isset($request->landowner_type_id) ? $request->landowner_type_id : 1;
                $propertyuser->update();
                
            }else{
                $new = New PropertyUser;
                $new->property_id = $property->id;
                $new->user_id = $user->id;
                $new->usertype_id = isset($request->landowner_type_id) ? $request->landowner_type_id : 1;
                $new->save();
            }

            //Características
            if($request->feature){

                DB::table('feature_property')->where('property_id', $id)->delete();
                $feat = Input::get('feature');
                list($keys, $values) = array_divide($feat);

                $caract = Feature::whereIn('id',$keys)->get();
                $largo = $request->largo;
                $ancho = $request->ancho;
                $alto = $request->alto;

                foreach ($caract as $item) {
                    $features = new FeatureProperty;
                    $features->feature_id = $item->id;
                    $features->property_id = $property->id;
                    if ($item->id == 6) {
                        $features->valor = $request->rooms;
                    } 
                    if ($item->id == 7) {
                        $features->valor = $request->restrooms;
                    }
                    if ($item->id == 14) {
                        $features->valor = $request->estacionamiento;
                    }

                    if ($item->id == 17) {
                        $features->descriptor = $largo.' x '.$ancho.' x '.$alto;
                    }

                    if ($item->id == 18) {
                        $features->valor = $request->ac;
                    }

                    if ($item->id == 19) {
                        $features->valor = $request->water_heater;
                    }
                    $features->save();
                }

                $new_pool = Pool::where('property_id', $id)->first();
                
                if(count($new_pool)==0) {
                        $pool_new = new Pool;
                        $pool_new->property_id = $property->id;
                        $pool_new->largo = $largo;
                        $pool_new->ancho = $ancho;
                        $pool_new->alto = $alto;
                        $pool_new->capacidad = $largo * $ancho * $alto;
                        $pool_new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                        $pool_new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                        $pool_new->save();
                    } else {
                        $new_pool->largo = $largo;
                        $new_pool->ancho = $ancho;
                        $new_pool->alto = $alto;
                        $new_pool->capacidad = $largo * $ancho * $alto;
                        $new_pool->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                        $new_pool->update();
                    }

                $new_ac = AirConditionerType::where('property_id', $property->id)->first();
                if (count($new_ac)==0) {
                    $ac_new = new AirConditionerType;
                    $ac_new->property_id = $property->id;
                    $ac_new->type = $request->ac;
                    $ac_new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $ac_new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $ac_new->save();
                } else {
                    $new_ac->type = $request->ac;
                    $new_ac->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $new_ac->update();
                }

                $new_water_heater = WaterHeaterType::where('property_id', $property->id)->first();
                if(count($new_water_heater)==0) {
                    $water_heater_new = new WaterHeaterType;
                    $water_heater_new->property_id = $property->id;
                    $water_heater_new->type = $request->water_heater;
                    $water_heater_new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $water_heater_new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $water_heater_new->save();
                } else {
                    $new_water_heater->type = $request->water_heater;
                    $new_water_heater->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $new_water_heater->update();
                }

                $new_room = Room::where('property_id', $property->id)->first();
                if (count($new_room)==0) {
                    $room_new = new Room;
                    $room_new->property_id = $property->id;
                    $room_new->quantity = $request->rooms;
                    $room_new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $room_new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $room_new->save();
                } else {
                    $new_room->quantity = $request->rooms;
                    $new_room->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $new_room->update();
                }

                $new_restroom = Restroom::where('property_id', $property->id)->first();
                if(count($new_restroom)==0) {
                    $restroom_new = new Restroom;
                    $restroom_new->property_id = $property->id;
                    $restroom_new->quantity = $request->restrooms;
                    $restroom_new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $restroom_new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $restroom_new->save();
                } else {
                    $new_restroom->quantity = $request->restrooms;
                    $new_restroom->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
                    $new_restroom->update();
                }

            }else{
                DB::table('feature_property')->where('property_id', $id)->delete();
            }
            
            //Fin de características

            \Session::flash('success_message','¡La unidad se ha creado con éxito!');
            // return redirect('ControlPanel/propiedades');
            return redirect('ControlPanel/clientes/'.$user->id);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try{
            $destroy = Property::find($id);
            $destro->visible = 'false';
            $destroy->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $destroy->update();

            $new_pool = Pool::where('property_id', $property->id)->first();
            $new_pool->visible = 'false';
            $new_pool->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new_pool->update();

            $new_ac = AirConditionerType::where('property_id', $property->id)->first();
            $new_ac->visible = 'false';
            $new_ac->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new_ac->update();

            $new_water_heater = WaterHeaterType::where('property_id', $property->id)->first();
            $new_water_heater->visible = 'false';
            $new_water_heater->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new_water_heater->update();

            $new_room = Room::where('property_id', $property->id)->first();
            $new_room->visible = 'false';
            $new_room->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new_room->update();

            $new_restroom = Restroom::where('property_id', $property->id)->first();
            $new_restroom->visible = 'false';
            $new_restroom->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new_restroom->update();

            } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function createWithUserID($id) {
        try{
            

            // $properties = Property::where('visible','true')->OrderBy('id','asc')->get();
            $features = Feature::where('visible', 'true')->OrderBy('id', 'asc')->get();
            $propertyTypes = PropertyType::where('visible','true')->OrderBy('id','asc')->pluck('name','id');
            $status = Status::where('type','propiedad')->OrderBy('id','asc')->pluck('name','id');
            $measureUnits = MeasureUnit::where('visible','true')->OrderBy('id','asc')->pluck('name','id');
            $landowner_types = LandownerType::where('visible','true')->OrderBy('description','asc')->pluck('description','id');
            $published = 'Pendiente';
            $count = count($features)/4;
            $anos = array();
            $thisyear = date('Y');
            
            $agents = User::whereHas('roles', function($q) {
                $q->where('name', 'agent')->orWhere('name','superadmin')->orWhere('name','admin')->orWhere('name','supervisor');
            })->orderBy('name','asc')->get();

            foreach ($agents as $item) {
                $agentes[$item->id] = ucwords( $item->name );
            }

            $user = User::find($id);
            $cuenta = 1; 

            for($i=0; $i<100; $i++){
                $year = $thisyear - $i;
                $anos[$year] = $year;
            }


            return view('controlpanel.properties.create',[
                    'title' => 'Agregar nueva propiedad',
                    'title2' => 'Seleccionar características',
                    'title3' => 'SEO',
                    'title4' => 'Notas',
                    'subtitle' => 'Busque su posición actual',
                    'subtitle2' => 'Busque la posición de la propiedad',
                    'menu' => $this->menu('propiedades','create'),
                    // 'properties' => $properties,
                    'features' => $features,
                    'propertyTypes' => $propertyTypes,
                    'status' => $status,
                    'measureUnits' => $measureUnits,
                    'published' => $published,
                    'count' => $count,
                    'years' => $anos,
                    'user' => $user,
                    'cuenta' => $cuenta,
                    'landownerTypes' => $landowner_types,
                    'agents' => $agentes,
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function importar() {
        return view('controlpanel.properties.import', [
            'title' => 'Importar',
            'menu' => $this->menu('propiedades','todas'),
        ]);
    }

    public function notChecked() {
        $properties = Property::where('checked', 'false')->get();

        return view('controlpanel.properties.index', [
            'title' => 'Propiedades no revisadas',
            'properties' => $properties,
            'menu' => $this->menu('propiedades','todas'),
        ]);
    }

    public function googlemaps($latitude, $longitude){
        return view('controlpanel.properties.googlemaps',[
                    'title' => 'Google Maps',
                    'subtitle' => 'Ubicación de la propiedad',
                    'menu' => $this->menu('propiedades','todas'),
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                ]);
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    public function revisado($id){
        $property = Property::find($id);

        if($property){
            $property->checked = true;
            $property->published = true;
            $property->update();
        }

        return redirect()->back();
    }

    public function alquilada($id){
        $property = Property::find($id);

        if($property){
            $property->estadoTmp = 'alquilado';
            $property->published = false;
            $property->update();
        }

        return redirect()->back();
    }

    public function disponible($id){
        $property = Property::find($id);
        
        if($property){
            $property->estadoTmp = 'disponible';
            $property->published = true;
            $property->update();
        }

        return redirect()->back();
    }

    public function nodisponible($id){
        $property = Property::find($id);
        
        if($property){
            $property->estadoTmp = 'No Disponible';
            $property->published = false;
            $property->update();
        }

        return redirect()->back();
    }

    public function vendida($id){
        $property = Property::find($id);
        
        if($property){
            $property->estadoTmp = 'vendida';
            $property->published = false;
            $property->update();
        }

        return redirect()->back();
    }

    private function menu($level_1,$level_2){
        $menu = [
                'level_1' => $level_1,
                'level_2' => $level_2,
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }


}
