<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\PropertyType;

class PropertyTypesController extends Controller
{
    public function index()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            $propertyTypes = PropertyType::where('visible','true')->OrderBy('id','asc')->get();
            return view('controlpanel.propertyTypes.index',[
                    'title' => 'Tipos de Propiedades',
                    'menu' => $this->menu(),
                    'propertyTypes' => $propertyTypes,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            return view('controlpanel.propertyTypes.create',[
                    'title' => 'Crear nuevo tipo de propiedad',
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{
            $new = new PropertyType;
            $new->name = $request->name;
            $new->code = $request->code;
            $new->description = $request->description;
            $new->slug = str_slug($request->name,"-");
            $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->save();

            \Session::flash('success_message','¡El registro se ha creado con éxito!');
            return redirect('ControlPanel/tipo-de-propiedades');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try{
            if(Auth::user()->hasRole('agent')){
                return redirect('ControlPanel');
            }
            $propertyType = PropertyType::find($id);
            return view('controlpanel.propertyTypes.edit',[
                    'title' => 'Editar tipo de propiedad',
                    'menu' => $this->menu(),
                    'propertyType' => $propertyType,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $update = PropertyType::find($id);
            $update->name = $request->name;
            $update->code = $request->code;
            $update->description = $request->description;
            $update->slug = str_slug($request->name,"-");
            $update->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $update->update();

            \Session::flash('success_message','¡El registro se ha actualizado con éxito!');
            return redirect('ControlPanel/tipo-de-propiedades');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try{
            $destroy = PropertyType::find($id);
            $destroy->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/tipo-de-propiedades');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => 'tipo-de-propiedades',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
