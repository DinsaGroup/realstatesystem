<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class QueryController extends Controller
{
    public function email($email){
    	$user = User::where('email',$email)->get();
    	if(count($user)){
	    	return response()->json('true');
    	}else{
	    	return response()->json('false');
    	}
    }
}
