<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Role;

class RolesController extends Controller
{
    public function index()
    {
        try {
            $roles = Role::all();

            return view('controlpanel.roles.index',[
                'title' => 'Roles',
                'roles' => $roles,
                'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => 'roles-administrativos',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
