<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Sign;
use App\Property;

class SignsController extends Controller
{
    public function index()
    {
        try {

            if (Auth::user()->hasRole('superadmin')) {
                $signs = Sign::all();
            } else {
                $signs = Sign::where('visible', 'true')->OrderBy('id', 'asc')->get();
            }

            return view('controlpanel.signs.index', [
                'title' => 'Rótulos',
                'signs' => $signs,
                'path' => 'agentes',
                'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function create()
    {
        try{
            return view('controlpanel.signs.create',[
                    'title' => 'Crear nuevo rótulo',
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function store(Request $request)
    {
        try{
            $property = Property::where('code', $request->code_id)->first();

            if(trim($request->notes)!=''){
                $note = $request->notes;
            }else{
                $note='';
            }

            $new = new Sign;
            $new->code = $request->code;
            $new->notes = $note;
            $new->property_id = $property->id;
            $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->save();

            \Session::flash('success_message','¡El registro se ha creado con éxito!');
            return redirect('ControlPanel/rotulos');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }
    
    public function show($id)
    {
        //
    }
    
    public function edit($id)
    {
        try{
            $sign = Sign::find($id);
            return view('controlpanel.signs.edit',[
                    'title' => 'Editar rótulo',
                    'menu' => $this->menu(),
                    'sign' => $sign,
                ]);
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }
    
    public function update(Request $request, $id)
    {
        try{
            $property = Property::where('code', $request->code_id)->first();

            $update = Sign::find($id);
            $update->property_id = $property->id;
            $update->notes = $request->notes;
            $update->status = $request->status;
            $update->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $update->update();

            \Session::flash('success_message','¡El registro se ha actualizado con éxito!');
            return redirect('ControlPanel/rotulos');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function destroy($id)
    {
        try{
            $destroy = Sign::find($id);
            $destroy->visible = 'false';
            $destroy->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $destroy->update();

            \Session::flash('success_message','¡El registro se ha eliminado con éxito!');
            return redirect('ControlPanel/rotulos');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => 'propiedades',
                'level_2' => 'rotulos',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
