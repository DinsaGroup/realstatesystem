<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Status;
use App\PropertyType;

class StatusesController extends Controller
{
    public function index()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                // \Session::flash('error_message', '¡No tiene acceso a esa area!');
                return redirect('ControlPanel');
            }
            $statuses = Status::where('visible','true')->OrderBy('id','asc')->get();
            $propertyTypes = PropertyType::where('visible','true')->OrderBy('id','asc')->get();
            return view('controlpanel.status.index',[
                    'title' => 'Status',
                    'menu' => $this->menu(),
                    'statuses' => $statuses,
                    'propertyTypes' => $propertyTypes,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{
            if(Auth::user()->hasRole('agent')){
                // \Session::flash('error_message', '¡No tiene acceso a esa area!');
                return redirect('ControlPanel');
            }
            return view('controlpanel.status.create',[
                    'title' => 'Crear nuevo status',
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{
            $new = new Status;
            $new->name = $request->name;
            $new->description = $request->description;
            $new->type = $request->type;
            $new->slug = str_slug($request->name,"-");
            $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->save();

            \Session::flash('success_message','¡El status se ha creado con éxito!');
            return redirect('ControlPanel/status');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try{
            if(Auth::user()->hasRole('agent')){
                // \Session::flash('error_message', '¡No tiene acceso a esa area!');
                return redirect('ControlPanel');
            }
            $status = Status::find($id);
            return view('controlpanel.status.edit',[
                    'title' => 'Editar status',
                    'menu' => $this->menu(),
                    'status' => $status,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $update = Status::find($id);
            $update->name = $request->name;
            $update->description = $request->description;
            $update->type = $request->type;
            $update->slug = str_slug($request->name,"-");
            $update->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $update->update();

            \Session::flash('success_message','¡El status se ha actualizado con éxito!');
            return redirect('ControlPanel/status');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try{
            $destroy = Status::find($id);
            $destroy->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            return redirect('ControlPanel/status');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => 'configuraciones',
                'level_2' => 'status',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
