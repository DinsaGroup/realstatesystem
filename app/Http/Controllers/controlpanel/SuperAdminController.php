<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;

use Auth;
use Mail;
use Image;

class SuperAdminController extends Controller
{
    public function index()
    {
        try {

            if(Auth::user()->hasRole('agent')){
                // \Session::flash('error_message', '¡No tiene acceso a esa area!');
                return redirect('ControlPanel');
            }

            $administrators = User::whereHas(
                            'roles', function($q){
                                $q->where('name', 'superadmin');
                            }
                        )->get();

            return view('controlpanel.user.index', [
                'title' => 'Administrators',
                'administrators' => $administrators,
                'path'=> 'super-administradores',
                'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try {
            if(Auth::user()->hasRole('agent')){
                // \Session::flash('error_message', '¡No tiene acceso a esa area!');
                return redirect('ControlPanel');
            }

            $roles = Role::all()->pluck('display_name','id');

            return view('controlpanel.user.create', [
                'title' => 'New administrator',
                'menu' => $this->menu(),
                'roles' => $roles,
                'path'=> 'super-administradores',
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{

            $user = User::where('email', $request->name)->get();

            if(count($user)){
                \Session::flash('error_message','¡El registro ya existe, favor verifique!');
                return redirect('ControlPanel/agentes');
            }

            if($request->hasFile('logo')){
                $logo1 = $request->file('logo');
                $filename = time().'.'.$logo1->getClientOriginalExtension();
                Image::make($logo1)->resize(300,300)->save(public_path('/img/user/'.$filename));

            }else{
                $filename = 'default.jpg'; 
            }

            // $pass = str_random(25);
            // $e_pass = bcrypt($pass);

            $new = new User;
            $new->name = strtolower($request->name);
            $new->email = $request->email;
            $new->password = bcrypt($request->password);
            // $new->country_id = $request->country_id;
            $new->house = $request->house;
            $new->office = $request->office;
            $new->phone = $request->phone;
            $new->photo = $filename;
            $new->save();

            $role = Role::where('name','superadmin')->first();

            $new->attachRole($role);

            // Mail::send('email.user_notification',[
            //         'user' => $new,
            //         'pass' => $pass,
            //     ], function($m) use($new, $pass) {
            //         $m->from('cortega@dinsagroup.com', 'Momotombo Real State');
            //         $m->to($new->email, $new->name);
            //     });

            

            \Session::flash('success_message','New record you have been successfully saved!');
            return redirect('ControlPanel/super-administradores');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try {
            if(Auth::user()->hasRole('agent')){
                // \Session::flash('error_message', '¡No tiene acceso a esa area!');
                return redirect('ControlPanel');
            }
            $administrator = User::find($id);
            $roles = Role::all()->pluck('display_name','id');

            return view('controlpanel.user.edit', [
                'title' => 'Edit record',
                'administrator' => $administrator,
                'path'=> 'super-administradores',
                'roles' => $roles,
                'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{

            $update = User::find($id);

            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $filename = time().'.'.$avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300,300)->save(public_path('/img/user/'.$filename));
                $update->photo = $filename;
            }

            // $pass = str_random(25);
            // $e_pass = bcrypt($pass);

            $email = $request->email;
            $user_email = $update->email;
            
            // if($email != $user_email) {
            //     $update->email = $request->email;
            //     $update->password = $e_pass;
            //     Mail::send('email.user_notification',[
            //         'user' => $update,
            //         'pass' => $pass,
            //     ], function($m) use($update, $pass) {
            //         $m->from('cortega@dinsagroup.com', 'Momotombo Real State');
            //         $m->to($update->email, $update->name);
            //     });
            // }

            $update->name = strtolower($request->name);
            
            if($request->password != ""){
                $update->password = bcrypt($request->password);
            }
            
            
            // $update->country_id = $request->country_id;
            $update->house = $request->house;
            $update->office = $request->office;
            $update->phone = $request->phone;
            $update->update();

            $role = Role::where('name','superadmin')->first();
            $update->roles()->detach($role->id);

            $role = Role::find($request->role_id);
            $update->attachRole($role);

            \Session::flash('success_message','Record updated!');
            return redirect('ControlPanel/super-administradores');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try {
            $delete = User::find($id);
            $delete->visible = 'false';
            $delete->updated_by = Auth::user()->id .' | '. Auth::user()->first_name .' '. Auth::user()->last_name .' | '. $this->ip_address();
            $delete->update();
            \Session::flash('success_message','¡El usuario se ha eliminado con éxito!');
            return redirect('MyAdmin/users');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => 'staff',
                'level_2' => 'super-administradores',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
