<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Testimonial;
use App\User;
use Auth;

class TestimonialController extends Controller
{
    
    public function index()
    {
        try {

            if (Auth::user()->hasRole('superadmin')) {
                $testimonials = Testimonial::all();
            } else {
                $testimonials = Testimonial::where('visible', 'true')->OrderBy('id', 'asc')->get();
            }

            return view('controlpanel.testimonials.index', [
                'title' => 'Testimonios',
                'testimonials' => $testimonials,
                'path' => 'agentes',
                'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function create()
    {
        try{
            return view('controlpanel.testimonials.create',[
                    'title' => 'Crear nuevo testimonio',
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function store(Request $request)
    {
        try{
            $user = User::where('name', $request->user_id)->first();

            $new = new Testimonial;
            $new->notes = $request->notes;
            $new->user_id = $user->id;
            $new->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $new->save();

            \Session::flash('success_message','¡El registro se ha creado con éxito!');
            return redirect('ControlPanel/testimonios');
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        try{
            $testimonial = Testimonial::find($id);
            return view('controlpanel.testimonials.edit',[
                    'title' => 'Editar testimonio',
                    'menu' => $this->menu(),
                    'testimonial' => $testimonial,
                ]);
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function update(Request $request, $id)
    {
        try{
            $update = Testimonial::find($id);
            $update->notes = $request->notes;
            $update->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $update->update();

            \Session::flash('success_message','¡El registro se ha actualizado con éxito!');
            return redirect()->back();
        } catch(Exception $e) {
             \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function destroy($id)
    {
        try{
            $destroy = Testimonial::find($id);
            $destroy->visible = 'false';
            $destroy->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $destroy->update();

            \Session::flash('success_message','¡El registro se ha eliminado con éxito!');
            return redirect('ControlPanel/testimonios');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => 'clientes',
                'level_2' => 'testimonios',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
