<?php

namespace App\Http\Controllers\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Mail;

use App\User;
use App\Country;
use App\Role;

use Image;

class UserController extends Controller
{

    public function index()
    {
        try{

            $users = User::all();

            return view('controlpanel.user.index',[
                'title' => 'Administradores',
                'users' => $users,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function create()
    {
        try{

            $roles = Role::all()->pluck('display_name','id');

            $newRol = Role::whereNotIn('name',['superadmin'])->pluck('display_name', 'id');


            return view('controlpanel.user.create',[
                'title' => 'Crear nuevo',
                // 'countries' => $countries,
                'roles' => $roles,
                'newRol' => $newRol,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function store(Request $request)
    {
        try{

            $user = User::where('name',$request->name)->get();

            if(count($user)){
                \Session::flash('error_message','¡El registro ya existe, favor verifique!');
                return redirect('ControlPanel/administradores');
            }

            if($request->hasFile('logo')){
                $logo1 = $request->file('logo');
                $filename = time().'.'.$logo1->getClientOriginalExtension();
                Image::make($logo1)->resize(300,300)->save(public_path('/img/user/'.$filename));

            }else{
                $filename = 'default.jpg'; 
            }

            $pass = str_random(25);
            $e_pass = bcrypt($pass);

            $new = new User;
            $new->name = strtolower(trim($request->name));
            $new->email = trim($request->email);
            $new->password = $e_pass;
            $new->house = $request->house;
            $new->office = $request->office;
            $new->phone = $request->phone;
            $new->photo = $filename;
            $new->save();

            $role = Role::find($request->role_id);

            $new->attachRole($role);

            Mail::send('email.user_notification',[
                    'user' => $new,
                    'pass' => $pass,
                ], function($m) use($new, $pass) {
                    $m->from('cortega@dinsagroup.com', 'Momotombo Real State');
                    $m->to($new->email, $new->name);
                });

            \Session::flash('success_message','¡El último registo se guardo correctamente!');
            return redirect('ControlPanel/administradores');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try{

            $user = User::find($id);
            // $countries = Country::all()->pluck('name','id');
            $roles = Role::all()->pluck('display_name','id');
            // $rol = Role::whereNotIn('name',['donante'])->pluck('display_name', 'id');

            $newRol = Role::whereNotIn('name',['superadmin'])->pluck('display_name', 'id');

            return view('controlpanel.user.edit',[
                'title' => 'Editar registro',
                'user' => $user,
                'roles' => $roles,
                'newRol' => $newRol,
                'menu' => $this->menu(),
                ]);

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function update(Request $request, $id)
    {
        try{

            $update = User::find($id);

            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $filename = time().'.'.$avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300,300)->save(public_path('/img/user/'.$filename));
                $update->photo = $filename;
            }

            $pass = str_random(25);
            $e_pass = bcrypt($pass);

            $email = $request->email;
            $user_email = $update->email;
            if($email != $user_email) {
                $update->email = $request->email;
                $update->password = $e_pass;
                Mail::send('email.user_notification',[
                    'user' => $update,
                    'pass' => $pass,
                ], function($m) use($update, $pass) {
                    $m->from('cortega@dinsagroup.com', 'Momotombo Real State');
                    $m->to($update->email, $update->name);
                });
            }

            $update->name = $request->name;
            
            // empty($request->password) ?  : $update->password = bcrypt($request->password);
            
            // $update->country_id = $request->country_id;
            $update->house = $request->house;
            $update->office = $request->office;
            $update->phone = $request->phone;
            $update->update();

            \Session::flash('success_message','¡El registro fue actualizado!');
            // return redirect('ControlPanel/administradores');
            return redirect('ControlPanel/administradores');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        try{

            $user = User::find($id);
            $user->delete();

            \Session::flash('success_message','¡El registro fue borrado!');
            // return redirect('ControlPanel/administradores');
            return redirect()->back();

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function menu(){
        $menu = [
                'level_1' => 'administradores',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
