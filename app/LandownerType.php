<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandownerType extends Model
{
    protected $tables = 'landowner_types';

    protected $fillable = ['code','description'];

    public function users(){
    	return $this->hasMany('App\PropertyUser','usertype_id','id');
    }
}
