<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeasureUnit extends Model
{
    protected $tables = 'measure_units';

    protected $fillable = ['name','short_name', 'symbol', 'slug', 'visible', 'created_by', 'updated_by'];

    public function properties()
    {
    	return $this->hasMany('App\Property');
    }

    public function pools()
    {
    	return $this->hasMany('App\Pool');
    }
}
