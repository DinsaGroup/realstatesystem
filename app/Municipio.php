<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $tables = 'municipios';

    protected $fillable = ['my_id', 'name', 'department_id'];

    public function department() {
    	return $this->belongsTo('App\Deparment', 'my_id', 'id');
    }

    public function barrios() {
    	return $this->hasMany('App\Barrio');
    }
}