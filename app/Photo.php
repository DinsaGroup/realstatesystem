<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $tables = 'photos';

    protected $fillable = ['property_id','filename', 'visible', 'created_by', 'updated_by'];

    public function property(){
	    return $this->belongsTo('App\Property');
    }
}
