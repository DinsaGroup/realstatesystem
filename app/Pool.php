<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pool extends Model
{
    protected $tables = 'pools';

    protected $fillable = ['largo', 'ancho', 'alto', 'capacidad', 'notes', 'visible', 'created_by', 'updated_by'];

    public function measure_unit()
    {
    	return $this->belongsTo('App\MeasureUnit');
    }

    public function status() 
    {
    	return $this->belongsTo('App\Status');
    }

    public function properties()
	{
		return $this->belongsTo('App\Property');
	}
}
