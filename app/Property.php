<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $tables = 'properties';

    protected $fillable = ['status_id','agent_id','property_type_id','measure_unit_id','code','exclusividad','disponible','ventaRenta','precioVenta','precioAlquiler','precioPorV2','areaTotal','areaConstruccion','antiguedad','acceso','topografia','description','address','sector','city','state','country','latitude','longitude','estadoTmp','areaMz','metaTitleEs','metaTitleEn','metaKeywordEs','metaKeywordEn','slug','visible','published','created_by','updated_by','comision','notes'];

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function user()
    {
        return $this->hasMany('App\PropertyUser','property_id','id');
    }

    public function property_type()
    {
        return $this->belongsTo('App\PropertyType');
    }

    public function measure_unit()
    {
    	return $this->belongsTo('App\MeasureUnit');
    }

    public function pool()
    {
    	return $this->hasOne('App\Pool');
    }

    public function water_heater_type()
    {
        return $this->hasOne('App\WaterHeaterType');
    }

    public function air_conditioner_type()
    {
        return $this->hasOne('App\AirConditionerType');
    }

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }

    public function featured(){
        return $this->belongsTo('App\FeaturedProperty');
    }

    public function features(){
        // return $this->belongsToMany('App\Feature')->using('App\FeatureProperty');
        // return $this->morphToMany('App\Feature','FeatureProperty');
        return $this->belongsToMany('App\Feature');
    }

    public function features_qty(){
        return $this->hasMany('App\FeatureProperty');
    }

    public function gold(){
        return $this->belongsTo('App\Gold');
    }

    public function agent(){
        return $this->belongsTo('App\User','agent_id','id');
    }

    public function sign()
    {
        return $this->hasMany('App\Sign');
    }

    public function room()
    {
        return $this->hasOne('App\Room');
    }

    public function restroom()
    {
        return $this->hasOne('App\Room');
    }

    public function barrio()
    {
        return $this->hasOne('App\Barrio');
    }

}
