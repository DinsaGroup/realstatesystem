<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    protected $tables = 'property_types';

    protected $fillable = ['code','name','description','slug', 'visible', 'created_by', 'updated_by'];

    public function status()
    {
    	return $this->belongsTo('App\Status');
    }

    public function properties()
    {
    	return $this->hasMany('App\Property');
    }
}
