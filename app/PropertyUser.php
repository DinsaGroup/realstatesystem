<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyUser extends Model
{
    protected $tables = 'property_users';

    protected $fillable = ['property_id','user_id','usertype_id'];

    public function property()
    {
    	return $this->belongsTo('App\Property','property_id','id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User','user_id','id');
    }

    public function user_type()
    {
        return $this->belongsTo('App\LandownerType','usertype_id','id');
    }

}
