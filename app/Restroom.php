<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restroom extends Model
{
    protected $tables = 'restrooms';

    protected $fillable = ['property_id', 'quantity', 'visible', 'created_by', 'updated_by'];

    public function property()
    {
        return $this->belongsTo('App\Property');
    }
}
