<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $tables = 'rooms';

    protected $fillable = ['property_id', 'quantity', 'visible', 'created_by', 'updated_by'];

    public function property()
    {
        return $this->belongsTo('App\Property');
    }
}
