<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sign extends Model
{
    protected $tables = 'signs';

    protected $fillable = ['property_id', 'code', 'status', 'notes', 'visible', 'created_by', 'updated_by'];

    public function property()
    {
        return $this->belongsTo('App\Property');
    }
}
