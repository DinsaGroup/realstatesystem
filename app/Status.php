<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $tables = 'statuses';

    protected $fillable = ['name','description','slug', 'type'];

    public function property_types()
	{
		return $this->hasMany('App\PropertyType');
	}

	public function pools()
	{
		return $this->hasMany('App\Pool');
	}

	public function water_heater_types()
	{
		return $this->hasMany('App\WaterHeatherType');
	}

	public function properties()
	{
		return $this->hasMany('App\Property');
	}

	public function air_conditioner_types()
	{
		return $this->hasMany('App\AirConditionerType');
	}
}
