<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $tables = 'testimonials';

    protected $fillable = ['user_id','notes', 'visible', 'created_by', 'updated_by'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
