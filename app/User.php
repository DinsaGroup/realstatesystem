<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable
{
    use Notifiable;

    use EntrustUserTrait;

    protected $fillable = [
        'name', 'email', 'password', 'photo', 'last_login', 'ip','house','office','phone','comments','identification','checked','usertype_id','referenced_id'
    ];
   
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsToMany('App\Role');
    }

    public function testimonials()
    {
        return $this->hasMany('App\Testimonial');
    }

    public function agent_properties(){
        return $this->hasMay('App\User');
    }

    public function landowner_type(){
        return $this->belongsToMany('App\LandownerType');
    }

    public function properties(){
        return $this->hasMany('App\PropertyUser','user_id','id');
    }
}
