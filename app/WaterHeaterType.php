<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaterHeaterType extends Model
{
    protected $tables = 'water_heater_types';

    protected $fillable = ['type', 'visible', 'created_by', 'updated_by'];

    public function status()
    {
    	return $this->belongsTo('App\Status');
    }

    public function property()
    {
    	return $this->belongsTo('App\Property');
    }
}
