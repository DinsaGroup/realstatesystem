<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code_id')->nullable();
            //Llaves foráneas
            $table->integer('status_id')->default(1);
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->integer('agent_id')->default(1);
            $table->foreign('agent_id')->references('id')->on('users');
            $table->integer('property_type_id')->default(1);
            $table->foreign('property_type_id')->references('id')->on('property_types');
            $table->integer('measure_unit_id')->default(1);
            $table->foreign('measure_unit_id')->references('id')->on('measure_units');
            //Terminan llaves foráneas
            $table->string('code')->nullable();
            $table->boolean('exclusividad')->default('false');
            $table->boolean('disponible')->default('true');
            $table->string('ventaRenta')->nullable();
            
            $table->float('precioVenta')->nullable();
            $table->float('precioAlquiler')->nullable();
            $table->float('precioPorV2')->nullable();
            $table->float('areaTotal')->nullable();
            $table->float('areaConstruccion')->nullable();
            
            $table->integer('antiguedad')->nullable();
            $table->string('acceso')->nullable();
            $table->string('topografia')->nullable();
            $table->longText('description')->nullable();
            $table->longText('address')->nullable();
            $table->string('sector')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            
            $table->decimal('latitude',20,16)->nullable();
            $table->decimal('longitude',20,16)->nullable();
            
            $table->longText('estadoTmp')->nullable();
            
            $table->float('areaMz')->nullable();

            $table->longText('notes')->nullable();
            
            $table->longText('metaTitleEs')->nullable();
            $table->longText('metaTitleEn')->nullable();
            $table->longText('metaKeywordEs')->nullable();
            $table->longText('metaKeywordEn')->nullable();

            $table->boolean('checked')->default('false');

            $table->string('slug')->nullable();
            $table->boolean('visible')->default('true');
            $table->boolean('published')->default('false');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
