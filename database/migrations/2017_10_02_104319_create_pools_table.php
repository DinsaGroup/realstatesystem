<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoolsTable extends Migration
{
    public function up()
    {
        Schema::create('pools', function (Blueprint $table) {
            $table->increments('id');
            //Llave foráneas
            $table->integer('measure_unit_id')->default(1);
            $table->foreign('measure_unit_id')->references('id')->on('measure_units');
            $table->integer('status_id')->default(31);
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->integer('property_id');
            $table->foreign('property_id')->references('id')->on('properties');
            //Terminan llaves foráneas
            $table->float('largo')->nullable();
            $table->float('ancho')->nullable();
            $table->float('alto')->nullable();
            $table->float('capacidad')->nullable();
            $table->longText('notes')->nullable();
            $table->string('visible')->default('true');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pools');
    }
}
