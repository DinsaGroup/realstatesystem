<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaterHeaterTypesTable extends Migration
{
    public function up()
    {
        Schema::create('water_heater_types', function (Blueprint $table) {
            $table->increments('id');
            //Llave foráneas
            $table->integer('status_id')->default(34);
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->integer('property_id');
            $table->foreign('property_id')->references('id')->on('properties');
            //Terminan llaves foráneas
            $table->string('type')->nullable();
            $table->string('visible')->default('true');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('water_heater_types');
    }
}
