<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirConditionerTypesTable extends Migration
{
    public function up()
    {
        Schema::create('air_conditioner_types', function (Blueprint $table) {
            $table->increments('id');
            //Llave foráneas
            $table->integer('status_id')->default(34);
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->integer('property_id');
            $table->foreign('property_id')->references('id')->on('properties');
            //Terminan llaves foráneas
            $table->string('type')->nullable();
            $table->string('description')->nullable();
            $table->string('visible')->default('true');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('air_conditioner_types');
    }
}
