<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturePropertyTable extends Migration
{
    public function up()
    {
        Schema::create('feature_property', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned();
            $table->integer('feature_id')->unsigned();
            $table->string('valor')->nullable();
            $table->string('descriptor')->nullable();

            $table->foreign('property_id')->references('id')->on('properties')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('feature_id')->references('id')->on('features')
                ->onUpdate('cascade')->onDelete('cascade');

            // $table->primary(['property_id', 'feature_id']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('feature_property');
    }
}
