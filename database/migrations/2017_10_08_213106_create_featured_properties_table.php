<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturedPropertiesTable extends Migration
{
    
    public function up()
    {
        Schema::create('featured_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->foreign('property_id')->references('id')->on('properties');
            $table->date('start');
            $table->date('end');
            $table->longText('note')->nullable();
            $table->boolean('visible')->default('true');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('featured_properties');
    }
}
