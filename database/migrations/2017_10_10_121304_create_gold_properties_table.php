<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoldPropertiesTable extends Migration
{

    public function up()
    {
        Schema::create('gold_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->foreign('property_id')->references('id')->on('properties');
            $table->string('type');
            $table->date('start');
            $table->date('end');
            $table->float('rentPrice')->nullable();
            $table->float('soldPrice')->nullable();
            $table->string('status')->nullable();
            $table->boolean('visible')->default(true);
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

  
    public function down()
    {
        Schema::dropIfExists('gold_properties');
    }
}
