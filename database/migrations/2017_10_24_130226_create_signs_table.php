<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignsTable extends Migration
{public function up()
    {
        Schema::create('signs', function (Blueprint $table) {
            $table->increments('id');
            //Llave foráneas
            $table->integer('property_id');
            $table->foreign('property_id')->references('id')->on('properties');
            //Terminan llaves foráneas
            $table->string('code')->nullable();
            $table->string('status')->default('disponible');
            $table->longText('notes')->default('disponible');
            $table->boolean('visible')->default('true');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('signs');
    }
}
