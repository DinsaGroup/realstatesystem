<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipiosTable extends Migration
{
    public function up()
    {
        Schema::create('municipios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('my_id')->unique();
            //Llave foráneas
            $table->integer('department_id');
            $table->foreign('department_id')->references('my_id')->on('departments');
            //Terminan llaves foráneas
            $table->string('name');
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('municipios');
    }
}
