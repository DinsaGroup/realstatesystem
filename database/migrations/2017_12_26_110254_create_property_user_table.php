<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyUserTable extends Migration
{
    
    public function up()
    {
         Schema::create('property_users', function (Blueprint $table) {
            $table->increments('id');
            //Llave foráneas
            $table->integer('property_id');
            $table->foreign('property_id')->references('id')->on('properties');
            $table->integer('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('usertype_id');
            $table->foreign('usertype_id')->references('id')->on('landowner_types');
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('property_users');
    }
}
