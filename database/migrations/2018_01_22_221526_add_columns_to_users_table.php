<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration
{
    
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('identification')->nullable();
            $table->boolean('checked')->default(false);
        });
    }
    
    public function down()
    {
        $table->dropcolumn('identification');
        $table->dropcolumn('checked');
    }
}
