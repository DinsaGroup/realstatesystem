<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColummsToPropertiesTable extends Migration
{
   public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->float('comision')->nullable();
        });
    }
    
    public function down()
    {
        $table->dropcolumn('comision');
    }
}
