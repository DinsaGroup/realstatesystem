<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyFieldsComisionOnPropertiesTable extends Migration
{
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->string('comision')->nullable()->change();
        });
    }
    
    public function down()
    {
        $table->float('comision')->nullable()->change();
    }
}
