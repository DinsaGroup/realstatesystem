<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LandownerTypeTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(PropertyTypesTableSeeder::class);
        $this->call(MeasureUnitsTableSeeder::class);
        // $this->call(PropertiesTableSeeder::class);
        $this->call(FeaturesTableSeeder::class);
        // $this->call(PhotosTableSeeder::class);
        // $this->call(GoldPropertiesTableSeeder::class);
        // $this->call(FeaturedPropertiesTableSeeder::class);
        // $this->call(TestimonialsTableSeeder::class);
    }
}
