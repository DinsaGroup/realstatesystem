<?php

use Illuminate\Database\Seeder;

use App\FeaturedProperty;

class FeaturedPropertiesTableSeeder extends Seeder
{

    public function run()
    {
        $data = array(
        	[
        		'property_id' 	=> 1,
        		'start' 		=> '2017-01-10',
        		'end' 			=> '2017-11-11',
        		'note' 			=> 'test',
        		'visible' 		=> true,
        		'created_by' 	=> 'superadmin',
        		'updated_by' 	=> 'superadmin'
        	],
        	[
        		'property_id' 	=> 2,
        		'start' 		=> '2017-02-20',
        		'end' 			=> '2017-11-20',
        		'note' 			=> 'test',
        		'visible' 		=> true,
        		'created_by' 	=> 'superadmin',
        		'updated_by' 	=> 'superadmin'
        	],
        	[
        		'property_id' 	=> 3,
        		'start' 		=> '2017-01-11',
        		'end' 			=> '2017-04-11',
        		'note' 			=> 'test',
        		'visible' 		=> true,
        		'created_by' 	=> 'superadmin',
        		'updated_by' 	=> 'superadmin'
        	],
        	[
        		'property_id' 	=> 4,
        		'start' 		=> '2017-05-01',
        		'end' 			=> '2017-11-30',
        		'note' 			=> 'test',
        		'visible' 		=> true,
        		'created_by' 	=> 'superadmin',
        		'updated_by' 	=> 'superadmin'
        	],
        	[
        		'property_id' 	=> 5,
        		'start' 		=> '2017-02-11',
        		'end' 			=> '2017-11-30',
        		'note' 			=> 'test',
        		'visible' 		=> true,
        		'created_by' 	=> 'superadmin',
        		'updated_by' 	=> 'superadmin'
        	],
        	);
        FeaturedProperty::insert($data);
    }
}
