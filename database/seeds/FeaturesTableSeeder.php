<?php

use Illuminate\Database\Seeder;

use App\Feature;
use App\FeatureProperty;

class FeaturesTableSeeder extends Seeder
{
    public function run()
    {
        $data = array(
        	[
        		'name' => 'Sala',
        		'description' => '',
                'icon'  => 'fa-television',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Comedor',
        		'description' => '',
                'icon'  => 'fa-cutlery',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Sala Comedor',
        		'description' => '',
                'icon'  => 'fa-cutlery',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Cocina',
        		'description' => '',
                'icon'  => 'fa-fire',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Alacena',
        		'description' => '',
                'icon'  => 'fa-th',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Cuartos',
        		'description' => '',
                'icon'  => 'fa-bed',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Baños',
        		'description' => '',
                'icon'  => 'fa-shower',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Sala Familiar',
        		'description' => '',
                'icon'  => 'fa-television',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Terraza',
        		'description' => '',
                'icon'  => 'fa-external-link',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Patio',
        		'description' => '',
                'icon'  => 'fa-external-link',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Jardín Externo',
        		'description' => '',
                'icon'  => 'fa-leaf',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Jardín Interno',
        		'description' => '',
                'icon'  => 'fa-leaf',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Área de Servicio',
        		'description' => '',
                'icon'  => 'fa-flag',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Estacionamiento',
        		'description' => '',
                'icon'  => 'fa-road',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Bodega',
        		'description' => '',
                'icon'  => 'fa-archive',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Tanque de Agua',
        		'description' => '',
                'icon'  => 'fa-tint',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Piscina',
        		'description' => '',
                'icon'  => 'fa-superpowers',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

            [
                'name' => 'Aire Acondicionado',
                'description' => '',
                'icon'  => 'fa-snowflake-o',
                'visible' => 'true',
                'created_by' => 'super admin',
                'updated_by' => 'super admin',
            ],

            [
                'name' => 'Calentador de Agua',
                'description' => '',
                'icon'  => 'fa-fire',
                'visible' => 'true',
                'created_by' => 'super admin',
                'updated_by' => 'super admin',
            ],

        	[
        		'name' => 'Jacuzzi',
        		'description' => '',
                'icon'  => 'fa-bath',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Teléfono',
        		'description' => '',
                'icon'  => 'fa-phone',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Líneas Telefónicas',
        		'description' => '',
                'icon'  => 'fa-phone',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Planta Eléctrica',
        		'description' => '',
                'icon'  => 'fa-bolt',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Voltaje',
        		'description' => '',
                'icon'  => 'fa-bolt',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Línea Blanca',
        		'description' => '',
                'icon'  => 'fa-check',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Amueblado',
        		'description' => '',
                'icon'  => 'fa-check',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Habitada',
        		'description' => '',
                'icon'  => 'fa-blind',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Posee Escrituras',
        		'description' => '',
                'icon'  => 'fa-check',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Posee Avalúos',
        		'description' => '',
                'icon'  => 'fa-check',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Posee Planos',
        		'description' => '',
                'icon'  => 'fa-check',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Posee Libertad de Gravamen',
        		'description' => '',
                'icon'  => 'fa-check',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Posee Historial Registral',
        		'description' => '',
                'icon'  => 'fa-check',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Posee Certificado Catastral',
        		'description' => '',
                'icon'  => 'fa-check',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],

        	[
        		'name' => 'Posee Firma',
        		'description' => '',
                'icon'  => 'fa-check',
        		'visible' => 'true',
        		'created_by' => 'super admin',
        		'updated_by' => 'super admin',
        	],
    	);
		Feature::insert($data);

        // $data2 = array(
        //     [
        //         'property_id' => 1,
        //         'feature_id' => 1,
        //     ],
        //     [
        //         'property_id' => 1,
        //         'feature_id' => 2,
        //     ],
        //     [
        //         'property_id' => 1,
        //         'feature_id' => 8,
        //     ],
        //     [
        //         'property_id' => 1,
        //         'feature_id' => 3,
        //     ],
        //     [
        //         'property_id' => 1,
        //         'feature_id' => 6,
        //     ],
        //     [
        //         'property_id' => 1,
        //         'feature_id' => 7,
        //     ],
        //     [
        //         'property_id' => 1,
        //         'feature_id' => 9,
        //     ],
        //     [
        //         'property_id' => 1,
        //         'feature_id' => 11,
        //     ],
        //     [
        //         'property_id' => 2,
        //         'feature_id' => 1,
        //     ],
        //     [
        //         'property_id' => 2,
        //         'feature_id' => 3,
        //     ],
        //     [
        //         'property_id' => 2,
        //         'feature_id' => 4,
        //     ],
        //     [
        //         'property_id' => 2,
        //         'feature_id' => 8,
        //     ],
        //     [
        //         'property_id' => 2,
        //         'feature_id' => 6,
        //     ],
        //     [
        //         'property_id' => 2,
        //         'feature_id' => 7,
        //     ],
        //     [
        //         'property_id' => 2,
        //         'feature_id' => 9,
        //     ],
        //     [
        //         'property_id' => 2,
        //         'feature_id' => 11,
        //     ],

        //     [
        //         'property_id' => 3,
        //         'feature_id' => 1,
        //     ],
        //     [
        //         'property_id' => 3,
        //         'feature_id' => 3,
        //     ],
        //     [
        //         'property_id' => 3,
        //         'feature_id' => 4,
        //     ],
        //     [
        //         'property_id' => 3,
        //         'feature_id' => 8,
        //     ],
        //     [
        //         'property_id' => 3,
        //         'feature_id' => 6,
        //     ],
        //     [
        //         'property_id' => 3,
        //         'feature_id' => 7,
        //     ],
        //     [
        //         'property_id' => 3,
        //         'feature_id' => 9,
        //     ],
        //     [
        //         'property_id' => 3,
        //         'feature_id' => 11,
        //     ],

        //     [
        //         'property_id' => 4,
        //         'feature_id' => 1,
        //     ],
        //     [
        //         'property_id' => 4,
        //         'feature_id' => 3,
        //     ],
        //     [
        //         'property_id' => 4,
        //         'feature_id' => 4,
        //     ],
        //     [
        //         'property_id' => 4,
        //         'feature_id' => 5,
        //     ],
        //     [
        //         'property_id' => 4,
        //         'feature_id' => 6,
        //     ],
        //     [
        //         'property_id' => 4,
        //         'feature_id' => 8,
        //     ],
        //     [
        //         'property_id' => 4,
        //         'feature_id' => 9,
        //     ],
        //     [
        //         'property_id' => 4,
        //         'feature_id' => 14,
        //     ],

        //     [
        //         'property_id' => 5,
        //         'feature_id' => 1,
        //     ],
        //     [
        //         'property_id' => 5,
        //         'feature_id' => 3,
        //     ],
        //     [
        //         'property_id' => 5,
        //         'feature_id' => 4,
        //     ],
        //     [
        //         'property_id' => 5,
        //         'feature_id' => 14,
        //     ],
        //     [
        //         'property_id' => 5,
        //         'feature_id' => 6,
        //     ],
        //     [
        //         'property_id' => 5,
        //         'feature_id' => 7,
        //     ],
        //     [
        //         'property_id' => 5,
        //         'feature_id' => 9,
        //     ],
        //     [
        //         'property_id' => 5,
        //         'feature_id' => 11,
        //     ],

        //     [
        //         'property_id' => 6,
        //         'feature_id' => 2,
        //     ],
        //     [
        //         'property_id' => 6,
        //         'feature_id' => 13,
        //     ],
        //     [
        //         'property_id' => 6,
        //         'feature_id' => 4,
        //     ],
        //     [
        //         'property_id' => 6,
        //         'feature_id' => 3,
        //     ],
        //     [
        //         'property_id' => 6,
        //         'feature_id' => 6,
        //     ],
        //     [
        //         'property_id' => 6,
        //         'feature_id' => 7,
        //     ],
        //     [
        //         'property_id' => 6,
        //         'feature_id' => 10,
        //     ],
        //     [
        //         'property_id' => 6,
        //         'feature_id' => 11,
        //     ],

        //     [
        //         'property_id' => 7,
        //         'feature_id' => 1,
        //     ],
        //     [
        //         'property_id' => 7,
        //         'feature_id' => 13,
        //     ],
        //     [
        //         'property_id' => 7,
        //         'feature_id' => 4,
        //     ],
        //     [
        //         'property_id' => 7,
        //         'feature_id' => 3,
        //     ],
        //     [
        //         'property_id' => 7,
        //         'feature_id' => 6,
        //     ],
        //     [
        //         'property_id' => 7,
        //         'feature_id' => 7,
        //     ],
        //     [
        //         'property_id' => 7,
        //         'feature_id' => 9,
        //     ],
        //     [
        //         'property_id' => 7,
        //         'feature_id' => 11,
        //     ],

        //     [
        //         'property_id' => 8,
        //         'feature_id' => 1,
        //     ],
        //     [
        //         'property_id' => 8,
        //         'feature_id' => 13,
        //     ],
        //     [
        //         'property_id' => 8,
        //         'feature_id' => 4,
        //     ],
        //     [
        //         'property_id' => 8,
        //         'feature_id' => 3,
        //     ],
        //     [
        //         'property_id' => 8,
        //         'feature_id' => 6,
        //     ],
        //     [
        //         'property_id' => 8,
        //         'feature_id' => 7,
        //     ],
        //     [
        //         'property_id' => 8,
        //         'feature_id' => 9,
        //     ],
        //     [
        //         'property_id' => 8,
        //         'feature_id' => 11,
        //     ],

        //     [
        //         'property_id' => 9,
        //         'feature_id' => 1,
        //     ],
        //     [
        //         'property_id' => 9,
        //         'feature_id' => 13,
        //     ],
        //     [
        //         'property_id' => 9,
        //         'feature_id' => 4,
        //     ],
        //     [
        //         'property_id' => 9,
        //         'feature_id' => 3,
        //     ],
        //     [
        //         'property_id' => 9,
        //         'feature_id' => 6,
        //     ],
        //     [
        //         'property_id' => 9,
        //         'feature_id' => 7,
        //     ],
        //     [
        //         'property_id' => 9,
        //         'feature_id' => 9,
        //     ],
        //     [
        //         'property_id' => 9,
        //         'feature_id' => 11,
        //     ],

        // );

        // DB::table('feature_property')->insert($data2);
    }
}
