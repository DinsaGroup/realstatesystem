<?php

use Illuminate\Database\Seeder;

use App\GoldProperty;

class GoldPropertiesTableSeeder extends Seeder
{

    public function run()
    {
        $data = array(
        		[
        			'property_id' 	=> 6,
                    'type'          =>'venta/renta',
        			'start' 		=> '2017-10-10',
        			'end'			=> '2018-10-09',
                    'rentPrice'     => 2324.00,
        			'soldPrice'     => 124578.25,
        			'created_by'	=> 'superadmin',
        			'updated_by'	=> 'superadmin',
        		],
                [
                    'property_id'   => 3,
                    'type'          =>'venta/renta',
                    'start'         => '2017-10-10',
                    'end'           => '2018-10-09',
                    'rentPrice'     => 234.12,
                    'soldPrice'     => 4215.25,
                    'created_by'    => 'superadmin',
                    'updated_by'    => 'superadmin',
                ],
                [
                    'property_id'   => 1,
                    'type'          =>'venta/renta',
                    'start'             => '2017-10-10',
                    'end'           => '2018-10-09',
                    'rentPrice'     => 1245.10,
                    'soldPrice'     => 754862.26,
                    'created_by'    => 'superadmin',
                    'updated_by'    => 'superadmin',
                ],
        );

        GoldProperty::insert($data);
    }
}
