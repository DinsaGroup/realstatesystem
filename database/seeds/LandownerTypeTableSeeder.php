<?php

use Illuminate\Database\Seeder;

use App\LandownerType;

class LandownerTypeTableSeeder extends Seeder
{
    
    public function run()
    {
        $data = array(
        	[
        		'code' 			=> 'nd',
        		'description' 	=> 'no definido'
        	],
        	[
        		'code' 			=> 'prop',
        		'description' 	=> 'propietario'
        	],
        	[
        		'code' 			=> 'fam',
        		'description' 	=> 'familiar'
        	],
        	[
        		'code' 			=> 'cuid',
        		'description' 	=> 'cuidador'
        	],
        	[
        		'code' 			=> 'otro',
        		'description' 	=> 'otros'
        	],
        	[
        		'code' 			=> 'esp',
        		'description' 	=> 'esposo(a)'
        	],
        	[
        		'code' 			=> 'hij',
        		'description' 	=> 'hijo(a)'
        	],
        	[
        		'code' 			=> 'hrm',
        		'description' 	=> 'hermano(a)'
        	],
        	[
        		'code' 			=> 'padr',
        		'description' 	=> 'padre'
        	],
        	[
        		'code' 			=> 'madr',
        		'description' 	=> 'madre'
        	],
        	[
        		'code' 			=> 'apod',
        		'description' 	=> 'apoderado(a)'
        	],
        	[
        		'code' 			=> 'encarg',
        		'description' 	=> 'encargado(a)'
        	],
        	[
        		'code' 			=> 'asist',
        		'description' 	=> 'asistente'
        	],
        	[
        		'code' 			=> 'emp',
        		'description' 	=> 'empleada'
        	],
        	[
        		'code' 			=> 'repl',
        		'description' 	=> 'representante legal'
        	],
        );
        LandownerType::insert($data);
    }
}
