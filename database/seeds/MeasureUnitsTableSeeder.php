<?php

use Illuminate\Database\Seeder;
use App\MeasureUnit;

class MeasureUnitsTableSeeder extends Seeder
{
    public function run()
    {
        $data = array(

        	//Metro
        		[
        			'name' => 'Metro',
        			'short_name' => 'm',
        			'slug' => 'metro',
        			'visible' => 'true',
        			'created_by' => 'super administrador',
        			'updated_by' => 'super administrador',
        		],

        	//Metro cuadrado
        		[
        			'name' => 'Metro Cuadrado',
        			'short_name' => 'm2',
        			'slug' => 'metro-cuadrado',
        			'visible' => 'true',
        			'created_by' => 'super administrador',
        			'updated_by' => 'super administrador',
        		],

        	//Metro Cúbico
        		[
        			'name' => 'Metro Cúbico',
        			'short_name' => 'm3',
        			'slug' => 'metro-cubico',
        			'visible' => 'true',
        			'created_by' => 'super administrador',
        			'updated_by' => 'super administrador',
        		],
        );
        
        MeasureUnit::insert($data);
    }
}
