<?php

use Illuminate\Database\Seeder;

use App\Photo;

class PhotosTableSeeder extends Seeder
{

    public function run()
    {
        $data = array(
        	[
        		'property_id' => '1',
        		'filename' => 'default.jpg',
        	],
        	[
        		'property_id' => '1',
        		'filename' => 'default2.jpg',
        	],
        	[
        		'property_id' => '1',
        		'filename' => 'default3.jpg',
        	],
        	[
        		'property_id' => '2',
        		'filename' => 'default.jpg',
        	],
        	[
        		'property_id' => '2',
        		'filename' => 'default2.jpg',
        	],
        	[
        		'property_id' => '2',
        		'filename' => 'default3.jpg',
        	],
        	[
        		'property_id' => '3',
        		'filename' => 'default.jpg',
        	],
        	[
        		'property_id' => '3',
        		'filename' => 'default2.jpg',
        	],
        	[
        		'property_id' => '3',
        		'filename' => 'default3.jpg',
        	],
        	[
        		'property_id' => '4',
        		'filename' => 'default.jpg',
        	],
        	[
        		'property_id' => '4',
        		'filename' => 'default2.jpg',
        	],
        	[
        		'property_id' => '4',
        		'filename' => 'default5.jpg',
        	],
        	[
        		'property_id' => '5',
        		'filename' => 'default5.jpg',
        	],
        	[
        		'property_id' => '5',
        		'filename' => 'default6.jpg',
        	],
        	[
        		'property_id' => '5',
        		'filename' => 'default2.jpg',
        	],
        	[
        		'property_id' => '6',
        		'filename' => 'default4.jpg',
        	],
        	[
        		'property_id' => '6',
        		'filename' => 'default3.jpg',
        	],
        	[
        		'property_id' => '6',
        		'filename' => 'default2.jpg',
        	],
        	);
    	Photo::insert($data);
    }
}
