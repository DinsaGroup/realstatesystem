<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use App\PropertyType;

class PropertyTypesTableSeeder extends Seeder
{
    public function run()
    {
    	$data = array(

        //Apartamento

        	[
                'status_id' => '1',
        		'name' => 'Apartamento',
                'code' => 'A',
        		'description' => 'Apartamento.',
        		'slug' => 'apartamento',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
        	],

        //Bodegas

            [
                'status_id' => '1',
                'name' => 'Bodega',
                'code' => 'B',
                'description' => 'Bodega.',
                'slug' => 'bodega',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Casas

            [
                'status_id' => '1',
                'name' => 'Casa',
                'code' => 'C',
                'description' => 'Casa.',
                'slug' => 'casa',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Casas de Playa 

            [
                'status_id' => '1',
                'name' => 'Casa de Playa',
                'code' => 'CP',
                'description' => 'Casa de Playa.',
                'slug' => 'casa-de-playa',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Condominios

            [
                'status_id' => '1',
                'name' => 'Condominio',
                'code' => 'CO',
                'description' => 'Condominio.',
                'slug' => 'condominio',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Condominios de Playa

            [
                'status_id' => '1',
                'name' => 'Condominio de playa',
                'code' => 'COP',
                'description' => 'Condominio de playa.',
                'slug' => 'condominio-de-playa',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Edificios

            [
                'status_id' => '1',
                'name' => 'Edificio',
                'code' => 'ED',
                'description' => 'Edificio.',
                'slug' => 'edificio',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Fincas

            [
                'status_id' => '1',
                'name' => 'Finca',
                'code' => 'F',
                'description' => 'Finca.',
                'slug' => 'finca',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Hoteles

            [
                'status_id' => '1',
                'name' => 'Hotel',
                'code' => 'H',
                'description' => 'Hotel.',
                'slug' => 'hotel',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Islas

            [
                'status_id' => '1',
                'name' => 'Isla',
                'code' => 'I',
                'description' => 'Isla.',
                'slug' => 'isla',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Locales

            [
                'status_id' => '1',
                'name' => 'Local',
                'code' => 'LO',
                'description' => 'Local.',
                'slug' => 'local',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Lotificación

            [
                'status_id' => '1',
                'name' => 'Lotificación',
                'code' => 'LOT',
                'description' => 'Lotificación.',
                'slug' => 'lotificacion',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Mansión

            [
                'status_id' => '1',
                'name' => 'Mansión',
                'code' => 'MAN',
                'description' => 'Mansión.',
                'slug' => 'mansion',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Módulos

            [
                'status_id' => '1',
                'name' => 'Módulo',
                'code' => 'M',
                'description' => 'Módulo.',
                'slug' => 'modulo',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Oficinas

            [
                'status_id' => '1',
                'name' => 'Oficina',
                'code' => 'O',
                'description' => 'Oficina.',
                'slug' => 'oficina',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Ofi Bodegas

            [
                'status_id' => '1',
                'name' => 'Ofi Bodega',
                'code' => 'OB',
                'description' => 'Ofi Bodega.',
                'slug' => 'ofi-bodega',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Plaza Comercial

            [
                'status_id' => '1',
                'name' => 'Plaza Comercial',
                'code' => 'PL',
                'description' => 'Plaza Comercial.',
                'slug' => 'plaza-comercial',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Proyectos Playa

            [
                'status_id' => '1',
                'name' => 'Proyecto Playa',
                'code' => 'PPL',
                'description' => 'Proyecto playa.',
                'slug' => 'proyecto-playa',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Quintas

            [
                'status_id' => '1',
                'name' => 'Quinta',
                'code' => 'Q',
                'description' => 'Quinta.',
                'slug' => 'quinta',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Terrenos

            [
                'status_id' => '1',
                'name' => 'Terreno',
                'code' => 'T',
                'description' => 'Terreno.',
                'slug' => 'terreno',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Terreno Playas

            [
                'status_id' => '1',
                'name' => 'Terreno Playa',
                'code' => 'TP',
                'description' => 'Terreno Playa.',
                'slug' => 'terreno-playa',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Urbanizaciones

            [
                'status_id' => '1',
                'name' => 'Urbanización',
                'code' => 'URB',
                'description' => 'Urbanización.',
                'slug' => 'urbanizacion',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        //Sin Categoría

            [
                'status_id' => '1',
                'name' => 'Sin Categoría',
                'code' => 'SC',
                'description' => 'Sin Categoría.',
                'slug' => 'sin-categoria',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

        );
    	PropertyType::insert($data);
    }
}