<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use App\Status;

class StatusTableSeeder extends Seeder
{
    public function run()
    {
    	$data = array(
            [
                'name' => 'Sin estado',
                'description' => 'Sin estado',
                'slug' => 'sin-estado',
                'type' => 'ninguno',
                'visible' => 'true',
                'created_by' => 'super administrador',
                'updated_by' => 'super administrador',
            ],

            //Apartamento

            	[
            		'name' => 'Activo',
            		'description' => 'El apartamento está activo.',
            		'slug' => 'activo',
            		'type' => 'apartamentos',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
            	],

                [
                    'name' => 'Inactivo',
                    'description' => 'El apartamento está inactivo.',
                    'slug' => 'inactivo',
                    'type' => 'apartamentos',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Casas

                [
                    'name' => 'Activa',
                    'description' => 'La casa está activa.',
                    'slug' => 'activa',
                    'type' => 'casas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactiva',
                    'description' => 'La casa está inactiva.',
                    'slug' => 'inactiva',
                    'type' => 'casas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Condominios

                [
                    'name' => 'Activo',
                    'description' => 'El condominio está activo.',
                    'slug' => 'activo',
                    'type' => 'condominios',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactivo',
                    'description' => 'El condominio está inactivo.',
                    'slug' => 'inactivo',
                    'type' => 'condominios',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Bodegas

                [
                    'name' => 'Activa',
                    'description' => 'La bodega está activa.',
                    'slug' => 'activa',
                    'type' => 'bodegas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactiva',
                    'description' => 'La bodega está inactiva.',
                    'slug' => 'inactiva',
                    'type' => 'bodegas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Oficinas

                [
                    'name' => 'Activa',
                    'description' => 'La oficina está activa.',
                    'slug' => 'activa',
                    'type' => 'oficinas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactiva',
                    'description' => 'La oficina está inactiva.',
                    'slug' => 'inactiva',
                    'type' => 'oficinas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Terrenos

                [
                    'name' => 'Activo',
                    'description' => 'El terreno está activo.',
                    'slug' => 'activo',
                    'type' => 'terrenos',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactivo',
                    'description' => 'El terreno está inactivo.',
                    'slug' => 'inactivo',
                    'type' => 'terrenos',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Playas

                [
                    'name' => 'Activa',
                    'description' => 'La playa está activa.',
                    'slug' => 'activa',
                    'type' => 'playas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactiva',
                    'description' => 'La playa está inactiva.',
                    'slug' => 'inactiva',
                    'type' => 'playas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Edificios

                [
                    'name' => 'Activo',
                    'description' => 'El edificio está activo.',
                    'slug' => 'activo',
                    'type' => 'edificios',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactivo',
                    'description' => 'El edificio está inactivo.',
                    'slug' => 'inactivo',
                    'type' => 'edificios',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Módulos

                [
                    'name' => 'Activo',
                    'description' => 'El módulo está activo.',
                    'slug' => 'activo',
                    'type' => 'modulos',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactivo',
                    'description' => 'El módulo está inactivo.',
                    'slug' => 'inactivo',
                    'type' => 'modulos',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Fincas

                [
                    'name' => 'Activa',
                    'description' => 'La finca está activa.',
                    'slug' => 'activa',
                    'type' => 'fincas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactiva',
                    'description' => 'La finca está inactiva.',
                    'slug' => 'inactiva',
                    'type' => 'fincas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Quintas

                [
                    'name' => 'Activa',
                    'description' => 'La quinta está activa.',
                    'slug' => 'activa',
                    'type' => 'quintas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactiva',
                    'description' => 'La quinta está inactiva.',
                    'slug' => 'inactiva',
                    'type' => 'quintas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Hoteles

                [
                    'name' => 'Activo',
                    'description' => 'El hotel está activo.',
                    'slug' => 'activo',
                    'type' => 'hoteles',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactivo',
                    'description' => 'El hotel está inactivo.',
                    'slug' => 'inactivo',
                    'type' => 'hoteles',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Ofi Bodegas

                [
                    'name' => 'Activa',
                    'description' => 'La ofi bodega está activa.',
                    'slug' => 'activa',
                    'type' => 'ofi-bodega',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactiva',
                    'description' => 'La ofi bodega está inactiva.',
                    'slug' => 'inactiva',
                    'type' => 'ofi-bodega',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Activa',
                    'description' => 'La isla está activa.',
                    'slug' => 'activa',
                    'type' => 'islas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Inactiva',
                    'description' => 'La isla está inactiva.',
                    'slug' => 'inactiva',
                    'type' => 'islas',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Piscina
                
                [
                    'name' => 'Sin estado',
                    'description' => 'Sin estado',
                    'slug' => 'sin-estado',
                    'type' => 'piscina',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Buen estado',
                    'description' => 'Piscina en buen estado',
                    'slug' => 'buen-estado',
                    'type' => 'piscina',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Mal estado',
                    'description' => 'Piscina en mal estado',
                    'slug' => 'mal-estado',
                    'type' => 'piscina',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Calentador de Agua
                
                [
                    'name' => 'Sin estado',
                    'description' => 'Sin estado',
                    'slug' => 'sin-estado',
                    'type' => 'calentador',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Buen estado',
                    'description' => 'Calentador en buen estado',
                    'slug' => 'buen-estado',
                    'type' => 'calentador',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Mal estado',
                    'description' => 'Calentador en mal estado',
                    'slug' => 'mal-estado',
                    'type' => 'calentador',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Aire Acondicionado
                
                [
                    'name' => 'Sin estado',
                    'description' => 'Sin estado',
                    'slug' => 'sin-estado',
                    'type' => 'ac',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Buen estado',
                    'description' => 'Aire acondicionado en buen estado',
                    'slug' => 'buen-estado',
                    'type' => 'ac',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Mal estado',
                    'description' => 'Aire acondicionado en mal estado',
                    'slug' => 'mal-estado',
                    'type' => 'ac',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

            //Propiedad

                [
                    'name' => 'Sin estado',
                    'description' => 'Sin estado',
                    'slug' => 'sin-estado',
                    'type' => 'propiedad',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Buen estado',
                    'description' => 'Propiedad en buen estado',
                    'slug' => 'buen-estado',
                    'type' => 'propiedad',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Regular estado',
                    'description' => 'Propiedad en regular estado',
                    'slug' => 'regular-estado',
                    'type' => 'propiedad',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],

                [
                    'name' => 'Mal estado',
                    'description' => 'Propiedad en mal estado',
                    'slug' => 'mal-estado',
                    'type' => 'propiedad',
                    'visible' => 'true',
                    'created_by' => 'super administrador',
                    'updated_by' => 'super administrador',
                ],
        );
    	Status::insert($data);
    }
}