<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Insert a global admin user
        DB::table('users')->delete();
        DB::table('roles')->delete();
        DB::table('role_user')->delete();
        $users = [
            [
                'name' => 'Carlos Toruño', 'email' => 'catoruno@dinsagroup.com', 'password' => bcrypt('tempo*01'), 'photo' => 'default.jpg','visible' => 'true',
                'created_by' => 'superadmin',
                'updated_by' => 'superadmin',
            ],

            
        ];
        DB::table('users')->insert($users);

        // Create Global User Roles
        $superadmin = new Role();
        $superadmin->name         = 'superadmin';
        $superadmin->display_name = 'Super Administrador'; // optional
        $superadmin->description  = 'Administrador General del sitio.'; // optional
        $superadmin->save();

        $administrator = new Role();
        $administrator->name         = 'admin';
        $administrator->display_name = 'Administrador'; // optional
        $administrator->description  = 'Administradores generales del sitio.'; // optional
        $administrator->save();

        $supervisor = new Role();
        $supervisor->name         = 'supervisor';
        $supervisor->display_name = 'Supervisor';
        $supervisor->description  = 'Supervisor de los agentes.';
        $supervisor->save();

        $agent = new Role();
        $agent->name         = 'agent';
        $agent->display_name = 'Agente de ventas'; // optional
        $agent->description  = 'Agente vendedor de la empresa.'; // optional
        $agent->save();

        $owner = new Role();
        $owner->name         = 'user';
        $owner->display_name = 'Usuario'; // optional
        $owner->description  = 'Es el usuario dueño de las propiedades.'; // optional
        $owner->save();

        // Add role to Admin User
        $user = User::where('email', '=', 'catoruno@dinsagroup.com')->first();
        $user->attachRole($superadmin);

    }
}
