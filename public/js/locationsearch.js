var marker = new google.maps.Marker({
        position:{
            lat: 12.135,
            lng: -86.270
        },
    });

    var defaultBounds = new google.maps.LatLngBounds(
	  new google.maps.LatLng(10.826900763796283, -87.61033203124998),
	  new google.maps.LatLng(14.0450044105228, -83.21580078124998)
	);

    var options = {
	  bounds: defaultBounds,
	  types: ['establishment', 'geocode'],
	  componentRestrictions: {country: 'ni'}
	};

	// Get the HTML input element for the searchbox
	var input = document.getElementById('auto');

	// Create the autocomplete object
    var searchBox = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(searchBox,'place_changed',function(){

        var place = searchBox.getPlace();
        var bounds = new google.maps.LatLngBounds();
        var i, place;
        bounds.extend(place.geometry.location);
        marker.setPosition(place.geometry.location); // set marker position new...

    });

    google.maps.event.addListener(marker,'position_changed',function(){

        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();
        $('#lat').val(lat);
        $('#lng').val(lng);

    });

    $(document).ready(function(){

    	$('#auto').keypress(function(e){
    		if(e.which == 13 ){
    			if($('#lat').val() == ""){
    				return false;
    			}else{
    				return true;
    			}
    		}
    	});

    });