@extends('layouts.frontend')

@section('head')
@endsection

@section('content')

	<section id="banner" class="banner-mobil" style="background-image:url({{ url('img/slider/b20.jpg') }}); background-size: cover; background-position: 50% 50%; width: 100%; margin-top: 50px;">
        <div class="banner result">
            @include('frontend.search')
        </div>
    </section>

    <section>
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12 about-txt">
						<h1>Somos Momotombo Real Estate</h1>
						<h2>Bienes Raíces con Transparencia, Compromiso y Responsabilidad</h2>

						<p>Momotombo Real Estate inició en Nicaragua en el año 2005 con el compromiso de contribuir al desarrollo de nuestro país ofreciendo al mercado de bienes raíces una forma diferente de ofertar y adquirir propiedades.</p>

						<p>Desde nuestros inicios, nos hemos enfocado siempre en brindar la mejor atención a todos y cada uno de nuestros clientes poniendo la honestidad como principio básico de nuestro desempeño.</p>

						<p>Hemos laborado en instituciones nacionales e internacionales en donde la confiabilidad y el servicio al cliente son la norma, y en donde la ética profesional se vuelve un elemento crucial.</p>

						<p>Momotombo Real Estate es una empresa dinámica y ansiosa de retos que ofrece a nuestros clientes la mejor opción en el mercado de bienes inmuebles, ganándose así su confianza.</p>

						<h2>Nuestros Valores</h2>

						<ul>
							<li>Responsabilidad</li>
							<li>Compromiso</li>
							<li>Ética Profesional</li>
							<li>Honradez</li>
							<li>Honestidad</li>
							<li>Transparencia</li>
							<li>Calidez</li>
						</ul>
						<h2>Conoce lo que Nicaragua Tiene Para Ofrecer</h2>
						<a class="cta-button" href="//www.momotomborealestate.com/">Explorar Propiedades</a>
    			</div>
    		</div>
    	</div>
    </section>

@endsection
@section('script')
@endsection
