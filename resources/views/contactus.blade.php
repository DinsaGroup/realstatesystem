@extends('layouts.frontend')

@section('head')
@endsection

@section('content')

	<section id="banner" class="banner-mobil" style="background-image:url({{ url('img/slider/b20.jpg') }}); background-size: cover; background-position: 50% 50%; width: 100%; margin-top: 50px;">
        <div class="banner result">
            @include('frontend.search')
        </div>
    </section>

    <section>
    	<div class="container">
    		<div class="row" style="padding: 0 0 10px;">
                <div class="col-sm-12">
                	<h1>Contactar</h1>
					<h2>Bienes Raíces con Transparencia, Compromiso y Responsabilidad</h2>

                    <p>Si tiene una propiedad que desee vender o rentar, contactenos llenando el presente formulario, un agente en pocos momentos se contactará con usted.<br><br>Promocione su propiedad con la mejor agencia de bienes raices de Nicaragua</p>
                    {!! Form::open(['url' => 'request-information', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
                    <input type="hidden" name="message_type" value="Desea publicar un inmueble">
                        <div class="col-sm-6 pull-left">
                            <label class="control-label">Nombre completo *</label>
                            <div class="">
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'¿Cuál es su nombre?', 'required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 pull-left">
                            <label class="control-label">Correo electrónico *</label>
                            <div class="">
                                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder'=>'¿Cuál es su correo electrónico?', 'required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 pull-left">
                            <label class="control-label">Celular *</label>
                            <div class="">
                                {!! Form::text('phone', null, ['class' => 'form-control phone_number', 'placeholder'=>'¿Cuál es su celular?', 'required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 pull-left">
                            <label class="control-label">Tipo de propiedad</label>
                            <div class="">
                                {!! Form::select('property_id',$property_types, null, ['class' => 'form-control', 'placeholder'=>'Seleccionar', 'required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-sm-12 pull-left">
                            {!! Form::label('description', 'Escribanos una breve descripción * ', ['class' => ' control-label']) !!}
                            <div class="">
                                {!! Form::textArea('description', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6 pull-right">
                            {!! Form::submit('Enviar información', ['class' => 'btn btn-primary form-control ']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
    	</div>
    </section>

@endsection
@section('script')
@endsection
