@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">  

<style type="text/css">
    #changeForm .short{
        font-family: 'Rubik', sans-serif;
        font-weight:bold;
        color:#F44336;
        opacity: 0.9;
        font-size:1.2em;
    }
    #changeForm .weak{
        font-family: 'Rubik', sans-serif;
        font-weight:bold;
        color:#e67e22;
        opacity: 0.9;
        font-size:1.2em;
    }
    #changeForm .good{
        font-family: 'Rubik', sans-serif;
        font-weight:bold;
        color:#2D98F3;
        opacity: 0.9;
        font-size:1.2em;
    }
    #changeForm .strong{
        font-family: 'Rubik', sans-serif;
        font-weight:bold;
        color: limegreen;
        opacity: 0.9;
        font-size:1.2em;
    }
    #changeForm .notok{
        font-family: 'Rubik', sans-serif;
        font-weight:bold;
        color:#F44336;
        opacity: 0.9;
        font-size:1.2em;
    }
    #changeForm .ok{
        font-family: 'Rubik', sans-serif;
        font-weight:bold;
        color:limegreen;
        opacity: 0.9;
        font-size:1.2em;
    }
    #changeForm .sin-padding{
        padding-top: 10px;
        padding-bottom: 10px;
    }
    #changeForm .con-margin{
        margin-left: 55px;
    }
</style>

    <div class="content-wrapper">
        <section class="content-header">
            <h1> {{ $title }}</h1>
            <small>Actualice su contraseña.</small>
        </section>

        <section class="content">

            {!! Form::model($user, [
                    'method' => 'PATCH',
                    'url' => ['ControlPanel/cambiar-password', $user->id],
                    'class' => 'form-horizontal',
                    'id' => 'changeForm'
                ]) !!}
                {{ csrf_field() }}
            <div class="">
                <div class="col-md-6 col-sm-6" style="margin-top: 2%;">
                    <div class="form-group">
                        {!! Form::label('oldPass', 'Contraseña anterior : ', ['class' => 'control-label col-sm-4']) !!}
                        <div class="col-sm-4">
                            {!! Form::password('oldPass', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'********', 'id' => 'oldPass' ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('newPass', 'Contraseña nueva : ', ['class' => 'control-label col-sm-4']) !!}
                        <div class="col-sm-4">
                            {!! Form::password('newPass', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'********', 'id' => 'newPass' ]) !!}
                        </div>
                        <div class="col-sm-4 no-padding">
                            <span id="result"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('verPass', 'Verificar contraseña : ', ['class' => 'control-label col-sm-4']) !!}
                        <div class="col-sm-4">
                            {!! Form::password('verPass', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'********', 'id' => 'verPass' ]) !!}
                        </div>
                        <div class="col-sm-4 no-padding">
                            <span id="final"></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 con-margin">
                        <div class="row">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content sin-padding">
                                <h4>La contraseña debe tener:</h4>
                                    <ul>
                                        <li>
                                            <h5>Al menos 7 caracteres</h5>
                                        </li>
                                        <li>
                                            <h5>Al menos una minúscula</h5>
                                        </li>
                                        <li>
                                            <h5>Al menos una mayúscula</h5>
                                        </li>
                                        <li>
                                            <h5>Al menos un número</h5>
                                        </li>
                                        <li>
                                            <h5>Al menos un caracter especial</h5>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                </div>                
                    
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3" style="margin-left: 35%;margin-top: 2%;">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

                {!! Form::close() !!}

            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<script>
    $(document).ready(function() {

        $('#newPass').keyup(function() {
            $('#result').html(checkStrength($('#newPass').val()));
        });

        function checkStrength(password) {
            var strength = 0;
            var pass = 0;
            if (password.length < 6) {
                $('#result').removeClass();
                $('#result').addClass('short');
                pass = 0;
                return 'Contraseña muy corta';
            }
            if (password.length > 7) strength += 1
            // If password contains both lower and uppercase characters, increase strength value.
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
            // If it has numbers and characters, increase strength value.
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
            // If it has one special character, increase strength value.
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
            // If it has two special characters, increase strength value.
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
            // Calculated strength value, we can return messages
            // If value is less than 2

            if (strength < 2) {
                $('#result').removeClass();
                $('#result').addClass('weak');
                pass = 0;
                return 'Contraseña débil';
            } else if (strength == 2) {
                $('#result').removeClass();
                $('#result').addClass('good');
                pass = 1;
                $('#verPass').keyup(function() {
                    var newPass = $('#newPass').val();
                    console.log(newPass);
                    var verPass = $('#verPass').val();
                    console.log(verPass);
                    if (newPass === verPass) {
                        $('#final').removeClass();
                        $('#final').addClass('ok');
                        $('#final').text('Las contraseñas son iguales');
                        $('input[type=submit]').click(function(e) {
                            // e.preventDefault();

                            var $oldPass = $('#oldPass').val();
                            
                            var $url = '{{ url('ControlPanel/request/check-pass') }}';

                            $.getJSON( $url, {'oldPass': $oldPass}, function(result) {
                                if (pass == 1) {
                                    if(result==1) {
                                        if($('#newPass').val() === $('#verPass').val()) {
                                            $('#changeForm').submit();
                                        } else {
                                            alert('Las contraseñas no coinciden. Ingrese nuevamente su contraseña nueva.');
                                        }
                                    } else {
                                        alert('La contraseña que ingresó como "Contraseña anterior" no coincide con la de la base de datos.');
                                        $('#oldPass').focus();
                                    }
                                } else {
                                    alert('Su contraseña no es segura. Ingrese una contraseña más segura.');
                                }
                            });
                        });
                    } else {
                        $('#final').removeClass();
                        $('#final').addClass('notok');
                        $('#final').text('Las contraseñas no coinciden');
                    }
                });
                return 'Contraseña segura';
            } else {
                $('#result').removeClass();
                $('#result').addClass('strong');
                pass = 1;
                $('#verPass').keyup(function() {
                    var newPass = $('#newPass').val();
                    console.log(newPass);
                    var verPass = $('#verPass').val();
                    console.log(verPass);
                    if (newPass === verPass) {
                        $('#final').removeClass();
                        $('#final').addClass('ok');
                        $('#final').text('Las contraseñas son iguales');
                        $('input[type=submit]').click(function(e) {
                            // e.preventDefault();

                            var $oldPass = $('#oldPass').val();
                            
                            var $url = '{{ url('ControlPanel/request/check-pass') }}';

                            $.getJSON( $url, {'oldPass': $oldPass}, function(result) {
                                if (pass == 1) {
                                    if(result==1) {
                                        if($('#newPass').val() === $('#verPass').val()) {
                                            $('#changeForm').submit();
                                        } else {
                                            alert('Las contraseñas no coinciden. Ingrese nuevamente su contraseña nueva.');
                                        }
                                    } else {
                                        alert('La contraseña que ingresó como "Contraseña anterior" no coincide con la de la base de datos.');
                                        $('#oldPass').focus();
                                    }
                                } else {
                                    alert('Su contraseña no es segura. Ingrese una contraseña más segura.');
                                }
                            });
                        });
                    } else {
                        $('#final').removeClass();
                        $('#final').addClass('notok');
                        $('#final').text('Las contraseñas no coinciden');
                    }
                });
                return 'Contraseña muy segura';
            }
        }

        /*$('input[type=submit]').click(function(e) {
            e.preventDefault();

            var $oldPass = $('#oldPass').val();
            
            var $url = '{{ url('ControlPanel/request/check-pass') }}';

            $.getJSON( $url, {'oldPass': $oldPass}, function(result) {
                console.log(result);
                if(result==1) {
                    //alert('Ok');
                    if($('#newPass').val() === $('#verPass').val()) {
                        $('#changeForm').submit();
                    } else {
                        alert('Las contraseñas no coinciden. Ingrese nuevamente su contraseña nueva.');
                    }
                } else {
                    alert('La contraseña que ingresó como "Contraseña anterior" no coincide con la de la base de datos.');
                    $('#oldPass').focus();
                }
            });
        });*/
    });
</script>
@endsection