@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

	<!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Resumen comportamiento</small>
            {{-- <a href="{{ url('ControlPanel/update-data/ledger') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-refresh"></i> Actualizar datos</a> --}}
          </h1>
          
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-building"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Propiedades registradas</span>
                  <span class="info-box-number">{{ $total }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Propietarios</span>
                  <span class="info-box-number">{{ $clientes }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            {{-- <div class="clearfix visible-sm-block"></div> --}}

            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-star"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Propiedades Destacadas</span>
                  <span class="info-box-number">{{ count($featured) }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-bullhorn"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Propiedades en Ganga</span>
                  <span class="info-box-number">{{ count($gold) }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

          </div><!-- /.row -->

          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-building"></i> Últimas propiedades agregadas</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <div class="btn-group">
                      <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      
                    @foreach($last_properties as $item)
                          <div class="col-sm-3">
                              <div class="property"
                                @if($item->estadoTmp == 'vendida') id='sold' @endif 
                                @if($item->estadoTmp == 'alquilado') id='rented' @endif 
                              >

                                  <div class="image">
                                      @if(count($item->photos))
                                          <img src="{{ url('img/properties/'. array_get($item->photos,'0.filename') ) }}" class="photo" />
                                      @else
                                          <img src="{{ url('img/properties/no-img.png') }}" class="photo ancho" />
                                      @endif
                                  </div>

                                  <span class="title2">{{ ucwords($item->property_type->name) }} en {{ ucwords($item->ventaRenta) }}<br>
                                    {{ ucwords($item->sector) }}, {{ ucwords($item->city) }}<br>
                                    {{ ucwords($item->estadoTmp) }}
                                  </span>
                                  <span class="price2">
                                      @if($item->ventaRenta == 'venta')
                                         PV:  U$ {{ number_format($item->precioVenta,2,'.',',') }}
                                      @endif
                                      @if($item->ventaRenta == 'renta')
                                         PR: U$ {{ number_format($item->precioAlquiler,2,'.',',') }}
                                      @endif
                                      @if($item->ventaRenta == 'venta/renta')
                                         <span class="pull-left">PV: U$ {{ number_format($item->precioVenta,2,'.',',') }}</span>
                                         <span class="pull-right">PR: U$ {{ number_format($item->precioAlquiler,2,'.',',') }}</span>
                                      @endif

                                  </span>
                                  <a href="#property" class="show_now details" data-id="{{$item->id}}" data-toggle="modal">Detalle</a>
                                  <a href="{{ url('propiedad/'.$item->code ) }}" class="show_now" target="_blank">Web</a>
                              </div>
                          </div>
                    @endforeach

                    </div><!-- /.col -->
                    
                  </div><!-- /.row -->
                </div><!-- ./box-body -->
                
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->


          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-star"></i> Propiedades destacadas</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <div class="btn-group">
                      <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      
                    @foreach($featured as $item)
                          <div class="col-sm-3">
                              <div class="property"
                                 @if($item->estadoTmp == 'vendida') id='sold' @endif 
                                @if($item->estadoTmp == 'alquilado') id='rented' @endif 
                              >

                                  <div class="image">
                                      @if(count($item->property->photos))
                                          <img src="{{ url('img/properties/'. array_get($item->property->photos,'0.filename') ) }}" class="photo" />
                                      @else
                                          <img src="{{ url('img/properties/no-img.png') }}" class="photo ancho" />
                                      @endif
                                  </div>

                                  <span class="title2">{{ ucWords($item->property->property_type->name) }} en {{ ucWords($item->property->ventaRenta) }}<br>
                                   {{ ucWords($item->property->sector) }}, {{ ucWords($item->property->city) }}<br>
                                   {{ ucWords($item->property->estadoTmp) }}
                                 </span>
                                  <span class="price2">
                                      @if($item->property->ventaRenta == 'venta')
                                         PV:  U$ {{ number_format($item->property->precioVenta,2,'.',',') }}
                                      @endif
                                      @if($item->property->ventaRenta == 'renta')
                                         PR: U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }}
                                      @endif
                                      @if($item->property->ventaRenta == 'venta/renta')
                                         <span class="pull-left">PV: U$ {{ number_format($item->property->precioVenta,2,'.',',') }}</span>
                                         <span class="pull-right">PR: U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }}</span>
                                      @endif

                                  </span>
                                  <a href="#property" class="show_now details" data-id="{{$item->property->id}}" data-toggle="modal">Detalle</a>
                                  <a href="{{ url('propiedad/'.$item->property->code ) }}" class="show_now" target="_blank">Web</a>
                              </div>
                          </div>
                    @endforeach

                    </div><!-- /.col -->
                    
                  </div><!-- /.row -->
                </div><!-- ./box-body -->
                
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-bullhorn"></i> Propiedades en Ganga</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <div class="btn-group">
                      <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      
                    @foreach($gold as $item)
                          <div class="col-sm-3">
                              <div class="property"
                                @if($item->estadoTmp == 'vendida') id='sold' @endif 
                                @if($item->estadoTmp == 'alquilado') id='rented' @endif 
                              >

                                  <div class="image">
                                      @if(count($item->property->photos))
                                          <img src="{{ url('img/properties/'. array_get($item->property->photos,'0.filename') ) }}" class="photo" />
                                      @else
                                          <img src="{{ url('img/properties/no-img.png') }}" class="photo ancho" />
                                      @endif
                                  </div>

                                  <span class="title2">{{ ucWords($item->property->property_type->name) }} en {{ ucWords($item->property->ventaRenta) }}<br>
                                   {{ ucWords($item->property->sector) }}, {{ ucWords($item->property->city) }}<br>
                                   {{ ucWords($item->property->estadoTmp) }}
                                 </span>
                                  <span class="price2">
                                      @if($item->property->ventaRenta == 'venta')
                                         PV:  U$ {{ number_format($item->property->precioVenta,2,'.',',') }}
                                      @endif
                                      @if($item->property->ventaRenta == 'renta')
                                         PR: U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }}
                                      @endif
                                      @if($item->property->ventaRenta == 'venta/renta')
                                         <span class="pull-left">PV: U$ {{ number_format($item->property->precioVenta,2,'.',',') }}</span>
                                         <span class="pull-right">PR: U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }}</span>
                                      @endif

                                  </span>
                                  <a href="#property" class="show_now details" data-id="{{$item->property->id}}" data-toggle="modal">Detalle</a>
                                  <a href="{{ url('propiedad/'.$item->property->code ) }}" class="show_now" target="_blank">Web</a>
                              </div>
                          </div>
                    @endforeach

                    </div><!-- /.col -->
                    
                  </div><!-- /.row -->
                </div><!-- ./box-body -->
                
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


@include('controlpanel.properties.property_modal_formato_2')
@endsection

@section('javascript')
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
      function showphoto(){
            $('.photoProperty img').click(function(){
                $('#fullphoto img').attr('src',$(this).attr('src'));
            });
        }

        function delphoto(){

            $('.delphoto').click(function(e){
                e.preventDefault();
                swal({
                      title: "¿Esta seguro?",
                      text: "¡No podrá recuperar la imagen!",
                      icon: "warning",
                      buttons: true,
                      closeModal: false,
                      dangerMode: true,
                      // showCancelButton: true,
                      // closeOnCancel: false
                      // closeOnConfirm: false,
                    })
                    .then((willDelete) => {
                      if (willDelete) {
                        
                        
                        var object = $(this);
                        var $id = $(this).data('id');
                        var $url =  '{{ url('ControlPanel/request/delphoto') }}';

                        $.getJSON($url,{'id': $id },function(result){
                            
                            if(result ==='ok'){
                                object.parent().remove();
                                
                            };
                        });

                        swal("¡Listo, La imagen ha sido borrada!", {
                          icon: "success",
                          
                        });

                      } else {
                        swal("¡La imagen no ha sido borrada!");
                      }
                    });
                
            })
        }
        
        function action(){
            $('.details').click(function(){
                var $id = $(this).data('id');
                var $url = '{{ url('ControlPanel/request/property_detail') }}';
                var $edit = '{{ url('ControlPanel/propiedades/') }}' +'/'+ $id + '/edit';
                var $addphotos = '{{ url('ControlPanel/propiedad/') }}' +'/'+ $id + '/fotos/create';
                var $disponible = '{{ url('ControlPanel/request/property/disponible') }}' +'/'+ $id ;
                var $noDisponible = '{{ url('ControlPanel/request/property/nodisponible') }}' +'/'+ $id ;
                var $alquilada = '{{ url('ControlPanel/request/property/alquilada') }}' +'/'+ $id ;
                var $vendida = '{{ url('ControlPanel/request/property/vendida') }}' +'/'+ $id ;
                var $checked = '{{ url('ControlPanel/request/property/revisado-publicar') }}' +'/'+ $id ;

                var $delephoto = '{{ url('ControlPanel/deletephoto') }}' ;

                //Limpiar valores
                $('.valor').empty();
                $('#files').empty();
                $('#contacts').empty();

                $.getJSON( $url, {'id': $id }, function(result) {
                    console.log(result.contacts);
                    $.each(result.contacts, function(key,value){
                        $('tbody#contacts').append('<tr><td class="text-center">'+value.customer_id+'</td><td>'+value.name+'</td><td>'+value.email+'</td><td>'+value.house+' '+value.office+' '+ value.phone+'</td><td>'+value.comments+'</td></tr>');
                    });

                    // $('#customer_id').text(result.customer_id);
                    // $('#name').text(result.name);
                    // $('#house').text(result.house);
                    // $('#office').text(result.office);
                    // $('#phone').text(result.phone);
                    // $('#email').text(result.email);
                    // $('#private_note').text(result.private_note);
                    $('#code').text(result.code);
                    $('#sign').text(result.sign);
                    $('#sismobilia').text(result.sismobilia);
                    $('#property_type').text(result.property_type);
                    $('#venta-renta').text(result.venta_renta);
                    $('#exclusividad').text(result.exclusividad);
                    $('#disponible').text(result.disponible);
                    $('#precioVenta').text(result.precioVenta);
                    $('#precioRenta').text(result.precioRenta);
                    $('#precioxV2').text(result.precioxV2);
                    $('#areaTotal').text(result.areaTotal);
                    $('#areaConstruida').text(result.areaConstruida);
                    $('#antiguedad').text(result.antiguedad);
                    $('#acceso').text(result.acceso);
                    $('#topografia').text(result.topografia);
                    $('#description').text(result.description);
                    $('#address').text(result.address);
                    $('#sector').text(result.sector);
                    $('#city').text(result.city);
                    $('#state').text(result.state);
                    $('#country').text(result.country);
                    $('#latitude').text(result.latitude);
                    $('#longitude').text(result.longitude);
                    $('#agente').text(result.agente);
                    $('#status').text(result.status);
                    $('#notes').text(result.notes);
                    $('#created').text(result.created);
                    $('#updated').text(result.updated);
                    $('#comision').text(result.comision);
                    $('#estadoTmp').text(result.estadoTmp);

                    var $google = '{{ url('ControlPanel/googlemaps/') }}' +'/'+ result.latitude + '/' + result.longitude ;

                    $('#edit').attr('href',$edit);
                    $('#add_photos').attr('href',$addphotos);
                    $('#ubicacion').attr('href',$google);
                    $('#disp').attr('href',$disponible);
                    $('#noDisponible').attr('href',$noDisponible);
                    $('#alquilada').attr('href',$alquilada);
                    $('#vendida').attr('href',$vendida);
                    $('#checked').attr('href',$checked);

                    if((result.photos).length > 0){
                        $first = true;
                        $.each(result.photos, function(key,value){
                            if(value.visible == 'true'){
                                var $link = '{{ url('img/properties/') }}' +'/'+ value.filename ;
                                if($first){
                                    $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><a href="" data-id="'+value.id+'" class="pull-right delphoto"><i class="fa fa-times"></i></a><img src="'+ $link + '" width="100%" /></div>');
                                     $('#fullphoto img').attr('src',$link);
                                     $first = false;
                                }else{
                                    $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><a href="" data-id="'+value.id+'" class="pull-right delphoto"><i class="fa fa-times"></i></a><img src="'+ $link + '" width="100%" /></div>');
                                    
                                }
                            }
                        });

                        delphoto();
                        showphoto();

                    }else{
                        var $link = '{{  url('img/properties/no-img.png')}}';
                        $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><img src="'+ $link + '" width="100%" /></div>');
                    }

                    // Presentar caracteristicas
                    if((result.features).length > 0){
                        $.each(result.features, function(key,value){
                            if(value.valor !=null){
                                $('#features').append('('+value.valor+')' + value.feature + ', ');
                            }else{
                                $('#features').append(value.feature + ', '); 
                            }
                        });
                    }

                });
                
            });
        }

        $(document).ready(function(){


            action();

            $('#search_button').click(function(){

                if( $('#search_box').val()!==''){

                      $('.gradeA').remove();
                      $('.loading').toggleClass('hidden');

                      var value = $('#search_box').val();
                      var url = '{{ url('ControlPanel/request/properties') }}';
                      // var link = '{{ url('ControlPanel/clientes/') }}';

                      $.getJSON(url, {'value': value }, function(result) {
                        $('.loading').toggleClass('hidden');

                        if( $('#result').hasClass('hidden')){
                          $('#result').toggleClass('hidden');
                        } 

                        $('.result').remove();

                        if(result.length>0){
                            
                          $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
                          $.each(result,function(key,value){

                            $('tbody').append('<tr class="gradeA '+ value.class + '"><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.code+'</a></td><td class=""><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.inmueble+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.exclusivo+'</a></td><td class="text-right"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">U$ '+value.pv+'</a></td><td class="text-right"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">U$ '+value.pr+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.rotulo+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.checked+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.status+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.estadoTmp+'</a></td></tr>');
                          });
                          action();
                        }else{
                           
                          $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
                          $('tbody').append('<tr class="gradeA" ><td colspan="8" class="text-center" style="height:150px">No se encontraron resultados para la busqueda...</td></tr>');
                        }

                      }); //getJSON

                }else{
                    alert('No hay palabras claves en la busqueda...');
                }

            });

        });

        // $(function () {
        //     $('#transaction_history').dataTable({
        //       "bSortClasses": 'sorting_3',
        //       "bPaginate": true,
        //       "bLengthChange": true,
        //       "bFilter": true,
        //       "bSort": true,
        //       "bInfo": true,
        //       "bAutoWidth": false,
        //       "iDisplayLength": 250,
        //       "aLengthMenu": [[250, 500, 1000, 2000, -1], [250, 500, 1000, 2000, "All"]]
        //     });
        // });
    </script>
@endsection