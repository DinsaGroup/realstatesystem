@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

<style>
    select {
        font-family: 'FontAwesome', 'Helvetica';
    } 

    option {
        font-family: 'FontAwesome', 'Helvetica';  
    }
</style>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::open(['url' => 'ControlPanel/caracteristicas', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
            {{ csrf_field() }}
            <div class="col-xs-12">

                    <div class="form-group">
                        {!! Form::label('name', 'NOMBRE : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Niveles, Escaleras']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'DESCRIPCIÓN : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Terraza, Sala Familiar']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('icon', 'ÍCONO : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-3">
                            <select name="icon" class="form-control">
                                <option value="" disabled selected>Seleccione uno</option>
                                <option value="fa-television">&#xf26c; Televisión</i></option>
                                <option value="fa-cutlery">&#xf0f5; Comida</i></option>
                                <option value="fa-fire">&#xf06d; Cocina</i></option>
                                <option value="fa-th">&#xf00a; Almacenamiento</i></option>
                                <option value="fa-bed">&#xf236; Cuartos</i></option>
                                <option value="fa-shower">&#xf2cc; Baños</i></option>
                                <option value="fa-external-link">&#xf08e; Exterior</i></option>
                                <option value="fa-leaf">&#xf06c; Área verde</i></option>
                                <option value="fa-flag">&#xf024; Servicio</i></option>
                                <option value="fa-road">&#xf018; Estacionamiento</i></option>
                                <option value="fa-archive">&#xf187; Bodega</i></option>
                                <option value="fa-tint">&#xf043; Recipientes de agua</i></option>
                                <option value="fa-superpowers">&#xf2dd; Piscina, Pileta</i></option>
                                <option value="fa-snowflake-o">&#xf2dc; Aire Acondicionado</i></option>
                                <option value="fa-cloud">&#xf06d; Calentador de Agua</i></option>
                                <option value="fa-bath">&#xf2cd; Jacuzzi</i></option>
                                <option value="fa-phone">&#xf095; Telefonía</i></option>
                                <option value="fa-bolt">&#xf0e7; Electricidad</i></option>
                                <option value="fa-check">&#xf00c; Existencia</i></option>
                                <option value="fa-blind">&#xf29d; Personas en casa</i></option>
                            </select>
                            {{-- {!! Form::select('icon', ['fa-television' => '"&#xf26c"' ], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione uno']) !!} --}}
                        </div>
                    </div>

                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

            {!! Form::close() !!}

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection