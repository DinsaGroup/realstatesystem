@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

<style>
    select {
        font-family: 'FontAwesome', 'Helvetica';
    } 

    option {
        font-family: 'FontAwesome', 'Helvetica';  
    }
</style>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
        </section>

        <section class="content">

            {!! Form::model($feature, [
                    'method' => 'PATCH',
                    'url' => ['ControlPanel/caracteristicas', $feature->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                {{ csrf_field() }}
            <div class="">
            
                <div class="form-group">
                        {!! Form::label('name', 'TÍTULO : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('name', $feature->name, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Activo / Inactivo']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'DESCRIPCIÓN : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('description', $feature->description, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: El terreno se encuentra activo.']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('icon', 'ÍCONO : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-3">
                            <select name="icon" class="form-control">
                                {{-- <option value="" disabled selected>Seleccione uno</option> --}}
                                <option value="fa-television" @if($feature->icon == 'fa-television') selected @endif>&#xf26c; Televisión</i></option>
                                <option value="fa-cutlery" @if($feature->icon == 'fa-cutlery') selected @endif>&#xf0f5; Comida</i></option>
                                <option value="fa-fire" @if($feature->icon == 'fa-fire') selected @endif>&#xf06d; Cocina</i></option>
                                <option value="fa-th" @if($feature->icon == 'fa-th') selected @endif>&#xf00a; Almacenamiento</i></option>
                                <option value="fa-bed" @if($feature->icon == 'fa-bed') selected @endif>&#xf236; Cuartos</i></option>
                                <option value="fa-shower" @if($feature->icon == 'fa-shower') selected @endif>&#xf2cc; Baños</i></option>
                                <option value="fa-external-link" @if($feature->icon == 'fa-external-link') selected @endif>&#xf08e; Exterior</i></option>
                                <option value="fa-leaf" @if($feature->icon == 'fa-leaf') selected @endif>&#xf06c; Área verde</i></option>
                                <option value="fa-flag" @if($feature->icon == 'fa-flag') selected @endif>&#xf024; Servicio</i></option>
                                <option value="fa-road" @if($feature->icon == 'fa-road') selected @endif>&#xf018; Estacionamiento</i></option>
                                <option value="fa-archive" @if($feature->icon == 'fa-archive') selected @endif>&#xf187; Bodega</i></option>
                                <option value="fa-tint" @if($feature->icon == 'fa-tint') selected @endif>&#xf043; Recipientes de agua</i></option>
                                <option value="fa-superpowers" @if($feature->icon == 'fa-superpowers') selected @endif>&#xf2dd; Piscina, Pileta</i></option>
                                <option value="fa-snowflake-o"@if($feature->icon == 'fa-snowflake-o') selected @endif>&#xf2dc; Aire Acondicionado</i></option>
                                <option value="fa-fire" @if($feature->icon == 'fa-fire') selected @endif>&#xf06d; Calentador de Agua</i></option>
                                <option value="fa-bath" @if($feature->icon == 'fa-bath') selected @endif>&#xf2cd; Jacuzzi</i></option>
                                <option value="fa-phone" @if($feature->icon == 'fa-phone') selected @endif>&#xf095; Telefonía</i></option>
                                <option value="fa-bolt" @if($feature->icon == 'fa-bolt') selected @endif>&#xf0e7; Electricidad</i></option>
                                <option value="fa-check" @if($feature->icon == 'fa-check') selected @endif>&#xf00c; Existencia</i></option>
                                <option value="fa-blind" @if($feature->icon == 'fa-blind') selected @endif>&#xf29d; Personas en casa</i></option>
                            </select>
                            {{-- {!! Form::select('icon', ['fa-television' => '"&#xf26c"' ], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione uno']) !!} --}}
                        </div>
                    </div>
                    
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection