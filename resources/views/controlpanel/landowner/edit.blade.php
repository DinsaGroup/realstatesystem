@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

{!! Form::model($user, [
                    'method' => 'PATCH',
                    'url' => ['ControlPanel/clientes', $user->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                {{ csrf_field() }}

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{ $title }} <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger pull-right"><i class="fa fa-times"></i> Cancelar</a>
                <div class="col-sm-2 pull-right">
                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary form-control create']) !!}
                </div>
            </h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            
            <div class="">
                <div class="form-group">
                    {!! Form::label('identification', 'Identificación : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {{-- @if(Auth::user()->hasRole('agent'))
                            {!! Form::text('identification', null, ['class' => 'form-control idcard', 'readonly' => 'readonly', 'placeholder'=>'Ej: 001-140845-0036J', 'id'=>'identification']) !!}
                        @else --}}

                            {!! Form::text('identification', null, ['class' => 'form-control idcard', 'placeholder'=>'Ej: 001-140845-0036J', 'id'=>'identification']) !!}
                        {{-- @endif --}}
                    </div>
                </div>
            
                <div class="form-group">
                    {!! Form::label('name', 'Nombre : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Ana Carolina Baez']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Correo electrónico : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-4">
                        {!! Form::email('email', null, ['class' => 'form-control',  'placeholder'=>'micorreo@midominio.com']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('avatar', 'Seleccionar avatar : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-6">
                        <img src="/img/user/{{ $user->photo}}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                        <input type='file' name="avatar" class="from-control" style="padding-top: 10px"> 
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('house', 'Teléfono casa : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {!! Form::text('house', null, ['class' => 'form-control phone_number', 'placeholder'=>'8888 8888']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('office', 'Teléfono oficina : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {!! Form::text('office', null, ['class' => 'form-control phone_number', 'placeholder'=>'8888 8888']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('phone', 'Celular : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {!! Form::text('phone', null, ['class' => 'form-control phone_number', 'placeholder'=>'8888 8888']) !!}
                    </div>
                </div>
                <div class="form-group">
                        {!! Form::label('checked', 'Verificado : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            @if($user->checked)
                            {!! Form::select('checked', ['true'=>'Datos verificados', 'false'=>'Pendiente por verificar'],'true', ['class' => 'form-control phone_number', 'placeholder'=>'Seleccionar','required'=>'required']) !!}
                            @else
                            {!! Form::select('checked', ['true'=>'Datos verificados', 'false'=>'Pendiente por verificar'],'false', ['class' => 'form-control phone_number', 'placeholder'=>'Seleccionar','required'=>'required']) !!}
                            @endif
                        </div>
                    </div>

                <div class="form-group">
                    {!! Form::label('comments', 'Notas internas : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-8">
                        {!! Form::textarea('comments', null, ['class' => 'form-control', 'rows'=>3]) !!}
                    </div>
                </div>
                

            </div>

        </section>

    </div>

{!! Form::close() !!}
@endsection

@section('javascript')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function(){

        $('#identification').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-idclient') }}'
            });
       
        $('#name').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-client') }}'
            });

        $('#email').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-email') }}'
            });

        // $('#email').focusout(function(){
        //     var $email = $(this).val();
        //     var $url = '{{ url('ControlPanel/email/') }}';
        //     $url = $url + '/' + $email;

        //     $.getJSON($url,'',function(resp) {

        //         if(resp=='true'){
        //             if(!$('#ok').hasClass('hidden')){
        //                $('#ok').toggleClass('hidden'); 
        //             }

        //             $('#bad').toggleClass('hidden');
        //             // $('#submit').attr('disabled','disabled');

        //         }else{
        //             if(!$('#bad').hasClass('hidden')){
        //                $('#bad').toggleClass('hidden'); 
        //             }

        //             $('#ok').toggleClass('hidden');  
        //             // $('#submit').removeAttr('disabled');  
        //         } 
        //     });
        // })

    })
</script>
@endsection