@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        {{-- <section class="content-header">
            <h1>
            {{ $title }}
             <a href="{{ url('ControlPanel/clientes/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a>
             <a href="#" class="btn btn-primary pull-right btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload"><i class="fa fa-upload"></i> Import data</a>
            </h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>          
        </section> --}}

         <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                      <div class="box-header">
                        <h3 class="box-title pull-left" style="width: 100%; margin-bottom: 10px;"><i class="fa fa-users"></i> Lista de propietarios</h3>
                        <a href="{{ url('ControlPanel/clientes/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a>
                        {{-- <span class="pull-right"> {{ $owners->links() }}</span> --}}
                        
                        <div class="col-md-3" style=" padding: 0">
                          {!! Form::label('search', 'Identificacion/Nombre/Email/Teléfonos', ['class' => 'control-label']) !!}
                          <div class="">
                            <input type="text" name="search" id="search_box" placeholder="Buscar propietario..." class="form-control">
                          </div>
                        </div>
                        <div class="pull-left" style="padding: 0 0 0 10px; margin-top: 10px">
                            <button id="search_button" style="width:100px; padding: 5px; margin-top: 10px">Buscar <i class="fa fa-search"></i></button>
                        </div>
                        <div class="pull-left" style="padding: 0 0 0 10px; margin-top: 10px">
                            <button id="reset" style="width:100px; padding: 5px; margin-top: 10px"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                        <div id="result" class="col-md-12 hidden">
                          <p class="result"></p>
                        </div>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                        <table id="owners" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                                <th>Identificación</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Teléfonos</th>
                                <th>Verificado</th>
                                {{-- <th>Propiedades</th> --}}
                                <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($owners as $item)
                                {{-- */$x++;/* --}}
                                <tr class="gradeA @if(!$item->checked) {{ 'despublished'}} @endif""> {{-- update --}}
                                    <td class="text-center"><a href="{{ url('ControlPanel/clientes/' . $item->id ) }}" class="details">{{ $item->identification }}</a></td>
                                    <td style="text-transform: capitalize;" ><a href="{{ url('ControlPanel/clientes/' . $item->id ) }}" class="details"><img src="{{ url('img/user/'. $item->photo) }}" class="user-image" /> {{ $item->name }}</a></td>
                                    <td><a href="{{ url('ControlPanel/clientes/' . $item->id ) }}" class="details">{{ $item->email }}</a></td>
                                    <td><a href="{{ url('ControlPanel/clientes/' . $item->id ) }}" class="details">{{ $item->house }} {{ $item->office }} {{ $item->phone }}</a></td>
                                    <td class="text-center"><a href="{{ url('ControlPanel/clientes/' . $item->id ) }}" class="details">@if($item->checked) {{ 'Revisado' }} @else {{'Pendiente'}} @endif</a></td>
                                    {{-- <td>{{ count($item->properties) }}</td> --}}
                                    <td><a href="{{ url('ControlPanel/clientes/'.$item->id.'/edit') }}" class="" >Editar</a></td>
                                </tr>
                            @endforeach
                                <tr class="gradeB"><td colspan="5" class="text-center hidden loading"><img src="{{ url('img/cargando.gif') }}" /></td></tr>
                          </tbody>
                        </table>
                      </div><!-- /.box-body -->
                    </div><!-- /.box -->

                    

                </div> {{-- col-md-12 --}}
            </div>

        </section>

    </div>

@endsection

@section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>

      $(document).ready(function(){

        $('#search_button').click(function(){

          $('.gradeA').remove();
          $('.loading').toggleClass('hidden');

          var value = $('#search_box').val();
          var url = '{{ url('ControlPanel/request/users') }}';
          var link = '{{ url('ControlPanel/clientes/') }}';

          $.getJSON(url, {'value': value }, function(result) {
            $('.loading').toggleClass('hidden');

            if( $('#result').hasClass('hidden')){
              $('#result').toggleClass('hidden');
            } 

            $('.result').remove();

            if(result.length>0){
              $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
              $.each(result,function(key,value){
                $('tbody').append('<tr class="gradeA '+ value.class + '" ><td><a href="'+link+'/'+value.id+'" class="details">'+value.identification+'</a></td><td style="text-transform: capitalize;"><a href="'+link+'/'+value.id+'" class="details">'+value.name+'</a></td><td><a href="'+link+'/'+value.id+'" class="details">'+value.email+'</a></td><td><a href="'+link+'/'+value.id+'" class="details">'+value.house+' '+ value.office +' '+value.phone+'</a></td><td><a href="'+link+'/'+value.id+'" class="details">'+value.checked+'</a></td><td><a href="'+link+'/'+value.id+'/edit" class="details">Editar</a></td></tr>');
              });
            }else{
              $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
              $('tbody').append('<tr class="gradeA" ><td colspan="5" class="text-center" style="height:150px">No se encontraron resultados para la busqueda...</td></tr>');
            }

          }); //getJSON
          
        });

        $('#reset').click(function(){
          $('#search_box').val('');

          $('.gradeA').remove();
          $('.loading').toggleClass('hidden');

          var value = '';
          var url = '{{ url('ControlPanel/request/reset-client') }}';
          var link = '{{ url('ControlPanel/clientes/') }}';

          $.getJSON(url, {'value': value }, function(result) {

            $('.loading').toggleClass('hidden');

            if( $('#result').hasClass('hidden')){
              $('#result').toggleClass('hidden');
            } 

            $('.result').remove();

            if(result.length>0){
              $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
              $.each(result,function(key,value){
                $('tbody').append('<tr class="gradeA '+ value.class + '" ><td><a href="'+link+'/'+value.id+'" class="details">'+value.identification+'</a></td><td style="text-transform: capitalize;"><a href="'+link+'/'+value.id+'" class="details">'+value.name+'</a></td><td><a href="'+link+'/'+value.id+'" class="details">'+value.email+'</a></td><td><a href="'+link+'/'+value.id+'" class="details">'+value.house+' '+ value.office +' '+value.phone+'</a></td><td><a href="'+link+'/'+value.id+'" class="details">'+value.checked+'</a></td><td><a href="'+link+'/'+value.id+'/edit" class="details">Editar</a></td></tr>');
              });

            }else{
              $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
              $('tbody').append('<tr class="gradeA" ><td colspan="5" class="text-center" style="height:150px">No se encontraron resultados para la busqueda...</td></tr>');
            }

          });

        });

        $('#search_box').autocomplete({
                minLenght: 4,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-client') }}',
        });

      })
      
    </script>
@endsection