@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection
<style type="text/css">
    .ancho{
        width: 104%;
    }
</style>
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>Perfil <strong>{{ ucwords($title) }}</strong> <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-undo"></i> Regresar</a></h1>
            <small>Detalle del perfil</small>
          
        </section>

        <section class="content">
            <div class="row">
                {{-- Perfil --}}
                <div class="col-md-6">
                  <!-- TABLE: LATEST ORDERS -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-user"></i> Perfil</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="direct-chat-messages">
                            <div class="col-xs-4">
                                <img src="{{ url('img/user/'.$owner->photo ) }}" width="100%" />
                            </div>
                            <div class="col-xs-8">
                                <div class="row"><div class="col-xs-3 small-title">Identificación</div><div class="col-xs-8"><strong>{{ ucwords($owner->identification) }}</strong></div></div>
                                <div class="row"><div class="col-xs-3 small-title">Nombre</div><div class="col-xs-8"><strong>{{ ucwords($owner->name) }}</strong></div></div>
                                <div class="row"><div class="col-xs-3 small-title">Email</div><div class="col-xs-8">{{ $owner->email }}</div></div>
                                <div class="row"><div class="col-xs-3 small-title">Casa</div><div class="col-xs-8">{{ $owner->house}}</div></div>
                                <div class="row"><div class="col-xs-3 small-title">Oficina</div><div class="col-xs-8">{{ $owner->office }}</div></div>
                                <div class="row"><div class="col-xs-3 small-title">Celular</div><div class="col-xs-8">{{ $owner->phone }}</div></div>
                                <div class="row"><div class="col-xs-3 small-title">Verificado</div><div class="col-xs-8">@if($owner->checked)<span style="color:green">Verificado</span>@else <span style="color:red">Pendiente</span>@endif </div></div>
                                <div class="row"><div class="col-xs-3 small-title">Actualizado</div><div class="col-xs-8">{{ $owner->updated_at }}</div></div>
                                <br>
                                <div class="row"><div class="col-xs-12 small-title">Nota privada</div><div class="col-xs-12">{{ ucfirst($owner->comments) }}</div></div>

                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                      <a href="{{ url('ControlPanel/clientes/'.$owner->id.'/edit') }}" class="btn btn-sm btn-info btn-flat pull-left">Actualizar información</a>
                    </div><!-- /.box-footer -->
                    
                  </div><!-- /.box -->
                </div>
                
                {{-- Tarjetas asociadas --}}
                <div class="col-md-6">
                  <!-- TABLE: LATEST ORDERS -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-comment"></i> Testimonio</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="direct-chat-messages">
                            <div class="col-xs-12">
                                <p id="text">{{ ucfirst( array_get($owner->testimonials, '0.notes') ) }}</p>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <input type="hidden" name="test" id="test" value="{{ $testimonial }}">
                        <input type="hidden" name="user_test" id="user_test" value="{{ $owner->id }}">
                        @if(!$testimonial)
                            <a id="create" href="#" class="btn btn-sm btn-info btn-flat pull-left edit" data-toggle="modal" data-target="#testimonial" >Crear testimonio</a>
                        @else
                            <a id="link" href="#" class="btn btn-sm btn-info btn-flat pull-left edit" data-toggle="modal" data-target="#testimonial" >Editar testimonio</a>
                        @endif                    </div><!-- /.box-footer -->
                    
                  </div><!-- /.box -->
                </div>
 
            </div><!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                  <!-- TABLE: LATEST ORDERS -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-barcode"></i> Propiedades del usuario {{ count($properties) }}</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="direct-chat-messages" style="height: auto; width: 99%">
                            <div class="col-sm-12" style="margin-bottom: 20px">
                                 <a href="{{ url('ControlPanel/propiedades/create/'.$owner->id) }}" class="btn btn-sm btn-info btn-flat"><i class="fa fa-plus"></i> Agregar una propiedad</a>
                            </div>
                            @foreach($properties as $item)
                                <div class="col-sm-4" >
                                    <div class="property"
                                        @if($item->property->estadoTmp == 'vendida') id='sold' @endif 
                                        @if($item->property->estadoTmp == 'alquilado') id='rented' @endif 
                                    >

                                        <div class="image">
                                            @if(count($item->property->photos))
                                                <img src="{{ url('img/properties/'. array_get($item->property->photos,'0.filename') ) }}" class="photo" />
                                            @else
                                                <img src="{{ url('img/properties/no-img.png') }}" class="photo ancho" />
                                            @endif
                                        </div>

                                        <span class="title">Código : {{ $item->property->code }} <br> {{ ucwords($item->property->property_type->name .' '. $item->property->ventaRenta .' '. $item->property->sector .', '. $item->property->city) }}<br>{{ ucfirst($item->property->estadoTmp) }}</span>
                                        <span class="price" style="float: left;width: 100%">
                                            @if($item->property->ventaRenta == 'venta')
                                                Pv.: U$ {{ number_format($item->property->precioVenta,2,'.',',') }}
                                            @endif
                                            @if($item->property->ventaRenta == 'renta')
                                                Pr.: U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }}
                                            @endif
                                            @if($item->property->ventaRenta == 'venta/renta')
                                                <span class="pull-left">Pv.: U$ {{ number_format($item->property->precioVenta,2,'.',',') }}</span>
                                                <span class="pull-right">Pr.: U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }}</span>
                                            @endif

                                        </span>
                                        <span style="float: left; padding: 10px; width: 100%;">
                                            <a href="#property" class="show_now details" data-id="{{$item->property->id}}" data-toggle="modal">Detalle</a>
                                            <a href="{{ url('propiedad/'.$item->property->code ) }}" class="show_now" target="_blank">Web</a>
                                        </span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div><!-- /.box-body -->
                    
                    
                  </div><!-- /.box -->
                </div>
                
            </div>

        </section>
    </div>

    <div id="testimonial" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">TESTIMONIO</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="">
                                        {!! Form::textArea('notes', array_get($owner->testimonials, '0.notes'), ['class' => 'form-control', 'required'=>'required','id'=>'notes']) !!}
                                    </div>  
                                </div>                        
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-2">
                            {!! Form::submit('Guardar', ['class' => 'btn btn-primary create','id'=>'save', 'data-testimonial' =>  array_get($owner->testimonials, '0.id')]) !!}
                        </div>
                    </div>
                </div>
            </div>  
    </div>

    @include('controlpanel.properties.property_modal_formato_2')

@endsection

@section('javascript')
    <!-- page script -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        
        function showphoto(){
            $('.photoProperty img').click(function(){
                $('#fullphoto img').attr('src',$(this).attr('src'));
            });
        }

        function delphoto(){

            $('.delphoto').click(function(e){
                e.preventDefault();
                swal({
                      title: "¿Esta seguro?",
                      text: "¡No podrá recuperar la imagen!",
                      icon: "warning",
                      buttons: true,
                      closeModal: false,
                      dangerMode: true,
                      // showCancelButton: true,
                      // closeOnCancel: false
                      // closeOnConfirm: false,
                    })
                    .then((willDelete) => {
                      if (willDelete) {
                        
                        
                        var object = $(this);
                        var $id = $(this).data('id');
                        var $url =  '{{ url('ControlPanel/request/delphoto') }}';

                        $.getJSON($url,{'id': $id },function(result){
                            
                            if(result ==='ok'){
                                object.parent().remove();
                                
                            };
                        });

                        swal("¡Listo, La imagen ha sido borrada!", {
                          icon: "success",
                          
                        });

                      } else {
                        swal("¡La imagen no ha sido borrada!");
                      }
                    });
                
            })
        }

        function action(){
            $('.details').click(function(){
                var $id = $(this).data('id');
                var $url = '{{ url('ControlPanel/request/property_detail') }}';
                var $edit = '{{ url('ControlPanel/propiedades/') }}' +'/'+ $id + '/edit';
                var $addphotos = '{{ url('ControlPanel/propiedad/') }}' +'/'+ $id + '/fotos/create';
                var $disponible = '{{ url('ControlPanel/request/property/disponible') }}' +'/'+ $id ;
                var $noDisponible = '{{ url('ControlPanel/request/property/nodisponible') }}' +'/'+ $id ;
                var $alquilada = '{{ url('ControlPanel/request/property/alquilada') }}' +'/'+ $id ;
                var $vendida = '{{ url('ControlPanel/request/property/vendida') }}' +'/'+ $id ;
                var $checked = '{{ url('ControlPanel/request/property/revisado-publicar') }}' +'/'+ $id ;

                var $delephoto = '{{ url('ControlPanel/deletephoto') }}' ;

                //Limpiar valores
                $('.valor').empty();
                $('#files').empty();
                $('#contacts').empty();

                $.getJSON( $url, {'id': $id }, function(result) {
                    // console.log(result.contacts);
                    $.each(result.contacts, function(key,value){
                        $('tbody#contacts').append('<tr><td class="text-center">'+value.customer_id+'</td><td>'+value.name+'</td><td>'+value.email+'</td><td>'+value.house+' '+value.office+' '+ value.phone+'</td><td>'+value.comments+'</td></tr>');
                    });

                    // $('#customer_id').text(result.customer_id);
                    // $('#name').text(result.name);
                    // $('#house').text(result.house);
                    // $('#office').text(result.office);
                    // $('#phone').text(result.phone);
                    // $('#email').text(result.email);
                    // $('#private_note').text(result.private_note);
                    $('#code').text(result.code);
                    $('#property_type').text(result.property_type);
                    $('#venta-renta').text(result.venta_renta);
                    $('#exclusividad').text(result.exclusividad);
                    $('#disponible').text(result.disponible);
                    $('#precioVenta').text(result.precioVenta);
                    $('#precioRenta').text(result.precioRenta);
                    $('#precioxV2').text(result.precioxV2);
                    $('#areaTotal').text(result.areaTotal);
                    $('#areaConstruida').text(result.areaConstruida);
                    $('#antiguedad').text(result.antiguedad);
                    $('#acceso').text(result.acceso);
                    $('#topografia').text(result.topografia);
                    $('#description').text(result.description);
                    $('#address').text(result.address);
                    $('#sector').text(result.sector);
                    $('#city').text(result.city);
                    $('#state').text(result.state);
                    $('#country').text(result.country);
                    $('#latitude').text(result.latitude);
                    $('#longitude').text(result.longitude);
                    $('#agente').text(result.agente);
                    $('#notasinternas').text(result.notes);
                    $('#status').text(result.status);
                    $('#created').text(result.created);
                    $('#updated').text(result.updated);
                    $('#comision').text(result.comision);
                    $('#estadoTmp').text(result.estadoTmp);

                    var $google = '{{ url('ControlPanel/googlemaps/') }}' +'/'+ result.latitude + '/' + result.longitude ;

                    $('#edit').attr('href',$edit);
                    $('#add_photos').attr('href',$addphotos);
                    $('#ubicacion').attr('href',$google);
                    $('#disp').attr('href',$disponible);
                    $('#noDisponible').attr('href',$noDisponible);
                    $('#alquilada').attr('href',$alquilada);
                    $('#vendida').attr('href',$vendida);
                    $('#checked').attr('href',$checked);

                    if((result.photos).length > 0){
                        $first = true;
                        $.each(result.photos, function(key,value){
                            if(value.visible == 'true'){
                                var $link = '{{ url('img/properties/') }}' +'/'+ value.filename ;
                                if($first){
                                    $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><a href="" data-id="'+value.id+'" class="pull-right delphoto"><i class="fa fa-times"></i></a><img src="'+ $link + '" width="100%" /></div>');
                                     $('#fullphoto img').attr('src',$link);
                                     $first = false;
                                }else{
                                    $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><a href="" data-id="'+value.id+'" class="pull-right delphoto"><i class="fa fa-times"></i></a><img src="'+ $link + '" width="100%" /></div>');
                                    
                                }
                            }
                        });

                        delphoto();
                        showphoto();

                    }else{
                        var $link = '{{  url('img/properties/no-img.png')}}';
                        $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><img src="'+ $link + '" width="100%" /></div>');
                    }

                    // Presentar caracteristicas
                    if((result.features).length > 0){
                        $.each(result.features, function(key,value){
                            if(value.valor !=null){
                                $('#features').append('('+value.valor+')' + value.feature + ', ');
                            }else{
                                $('#features').append(value.feature + ', '); 
                            }
                        });
                    }
                });
                
            });
        }

      $(document).ready(function() {

        $('#save').click(function(){
            //event.preventDefault();
            var id = $(this).data('testimonial');
            var testimonial = $('#test').val();
            var user_id = $('#user_test').val();
            // console.log(user);

            var notes = $('#notes').val();
            
            var $url2 = '{{ url('ControlPanel/request/save-testimonial') }}';
            var $url = '{{ url('ControlPanel/request/check-testimonial') }}';

            if(testimonial == '') {
                $.getJSON( $url2, {'user_id': user_id, 'notes': notes,}, function(resultado) {
                    $('#testimonial').modal('hide');
                    $('#notes').val('');
                    $('#text').empty();
                    $('#text').text(notes);
                    alert(resultado);
                });
            }

            if(notes != testimonial){
                    $.getJSON( $url, {'id': id, 'notes' : notes, }, function(result) {
                        $('#testimonial').modal('hide');
                        $('#notes').val('');
                        $('#text').empty();
                        $('#text').text(notes);
                        alert(result);
                    });
            }else{
                alert('You do not write any solution, please check and try again!');
            }
        });

        action();

        $('#search_button').click(function(){

            if( $('#search_box').val()!==''){

                  $('.gradeA').remove();
                  $('.loading').toggleClass('hidden');

                  var value = $('#search_box').val();
                  var url = '{{ url('ControlPanel/request/properties') }}';
                  // var link = '{{ url('ControlPanel/clientes/') }}';

                  $.getJSON(url, {'value': value }, function(result) {
                    $('.loading').toggleClass('hidden');

                    if( $('#result').hasClass('hidden')){
                      $('#result').toggleClass('hidden');
                    } 

                    $('.result').remove();

                    if(result.length>0){
                        
                      $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
                      $.each(result,function(key,value){

                        $('tbody').append('<tr class="gradeA '+ value.class + '"><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.code+'</a></td><td class=""><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.inmueble+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.exclusivo+'</a></td><td class="text-right"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">U$ '+value.pv+'</a></td><td class="text-right"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">U$ '+value.pr+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.rotulo+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.checked+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.status+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.estadoTmp+'</a></td></tr>');
                      });
                      action();
                    }else{
                       
                      $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
                      $('tbody').append('<tr class="gradeA" ><td colspan="8" class="text-center" style="height:150px">No se encontraron resultados para la busqueda...</td></tr>');
                    }

                  }); //getJSON

            }else{
                alert('No hay palabras claves en la busqueda...');
            }

        });


      });
    </script>
@endsection