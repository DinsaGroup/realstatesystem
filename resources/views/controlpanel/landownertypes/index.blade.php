@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/tipo-contacto/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                          <h3 class="box-title"><i class="fa fa-building"></i> Listado de propiedades</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            
                            <table id="contacttype" class="table table-bordered table-striped"> {{-- update --}}
                                <thead>
                                    <tr>
                                        <th class="text-center" width="50">ID</th>
                                        <th class="text-center" width="100">CODIGO</th>
                                        <th class="text-center">DESCRIPCION</th>
                                        {{-- <th class="text-center"><i class="fa fa-sort-desc"></i></th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- */$x=0;/* --}}
                                    @foreach($landownertypes as $item)
                                        {{-- */$x++;/* --}}
                                        <tr class="gradeA"> {{-- update --}}
                                            <td class="text-center"><a href="{{ url('ControlPanel/tipo-contacto/' . $item->id . '/edit') }}" class="details">{{ $item->id }}</a></td>
                                            <td class="text-center"><a href="{{ url('ControlPanel/tipo-contacto/' . $item->id . '/edit') }}" class="details">{{ strtoupper($item->code) }}</a></td>
                                            <td ><a href="{{ url('ControlPanel/tipo-contacto/' . $item->id . '/edit') }}" class="details">{{ ucfirst($item->description) }}</a></td>
                                            
                                           {{--  <td  class="text-center">
                                                <a href="{{ url('ControlPanel/tipo-contacto/' . $item->id . '/edit') }}" class="details">
                                                    <i class="fa fa-pencil"></i>
                                                </a> 
                                                
                                            </td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                                        
                                   
                        </div>
                    </div>
                </div>
        </section>

    </div>

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#contacttype").dataTable();
        
      });
    </script>
@endsection