@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

<div class="content-wrapper">
    <section class="content-header">
            <h1>
            {{ $title }}
             
            </h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>          
        </section>

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-building"></i> Importar información</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-country"><i class="fa fa-upload"></i> Importar país</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-department"><i class="fa fa-upload"></i> Importar departamento</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-municipio"><i class="fa fa-upload"></i> Importar municipio</a>                

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-barrio"><i class="fa fa-upload"></i> Importar barrio</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-clientes"><i class="fa fa-upload"></i> Importar Clientes</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-inmuebles"><i class="fa fa-upload"></i> Importar Inmuebles</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-fotos"><i class="fa fa-upload"></i> Importar Fotos</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-propiedad-usuario"><i class="fa fa-upload"></i> Importar relación propiedad/usuario</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-rotulos"><i class="fa fa-upload"></i> Importar rotulos</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-notes"><i class="fa fa-upload"></i> Importar notas a propiedades</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-agents"><i class="fa fa-upload"></i> Importar agentes </a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-correcciones"><i class="fa fa-upload"></i> Importar correcciones </a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-cedulas"><i class="fa fa-upload"></i> Importar cedulas </a>

            </div><!-- /.box-body -->
          </div><!-- /.box -->
            </div> {{-- col-md-12 --}}
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-building"></i> Mantenimiento a tablas</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <a href="{{ url('ControlPanel/maintenace_data/properties') }}" class="btn btn-primary btn-sm" style="margin-right: 10px;" ><i class="fa fa-upload"></i> Tabla Propiedades</a> 
              <a href="{{ url('ControlPanel/maintenace_data/users') }}" class="btn btn-primary btn-sm" style="margin-right: 10px;" ><i class="fa fa-upload"></i> Tabla usuarios</a> 
              <a href="{{ url('ControlPanel/maintenace_data/optimizacion_users') }}" class="btn btn-primary btn-sm" style="margin-right: 10px;" ><i class="fa fa-upload"></i> Opitmizacion usuarios</a>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
            </div> {{-- col-md-12 --}}
        </div>

    </section>

</div>

<div id="upload-cedulas" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importcedulas', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Importar cedulas</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-correcciones" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importcorrecciones', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Importar correcciones</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-agents" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importagents', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir asignacion de agentes a propiedades del sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-notes" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importnotes', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir notas a propiedades del sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-rotulos" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importsigns', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir rotulos al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-country" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importcountry', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir paises al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-department" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importdepartment', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir departamentos al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-municipio" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importmunicipio', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir municipios al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-barrio" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importbarrio', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir barrios al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-clientes" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importcontact', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir clientes al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-inmuebles" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importproperties', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir inmuebles al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-fotos" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importphotos', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir fotos al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-propiedad-usuario" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/import-property-user', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir relación propiedades/usuarios</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

@endsection

