<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
          <div class="pull-left image">
            <img src="{{ url('img/user/'. Auth::user()->photo ) }}" class="img-circle" alt="User Image" />
          </div>
          <div class="pull-left info">
            <p>{{ ucWords(Auth::user()->name) }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU PRINCIPAL</li>

        <li class="@if (array_get($menu,'level_1') === 'dashboard') active  @endif treeview">
          <a href="{{ url('ControlPanel') }}">
            <i class="fa fa-dashboard"></i> <span>DASHBOARD</span></i>
          </a>
        </li>

        <li class="@if (array_get($menu,'level_1') === 'clientes') active  @endif treeview">

          <a href="#">
            <i class="fa fa-address-book""></i>
            <span>PROPIETARIOS</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="@if (array_get($menu,'level_2') === 'clientes') active  @endif treeview">
              <a href="{{ url('ControlPanel/clientes') }}"><i class="fa fa-users"></i> Propietarios</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'create') active  @endif treeview">
              <a href="{{ url('ControlPanel/clientes/create') }}"><i class="fa fa-plus-circle"></i> Ingresar nuevo</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'testimonios') active  @endif treeview">
              <a href="{{ url('ControlPanel/testimonios') }}"><i class="fa fa-book"></i> Testimonios</a>
            </li>
          </ul>
        </li>

        <li class="@if (array_get($menu,'level_1') === 'propiedades') active  @endif treeview">
          <a href="#">
            <i class="fa fa-barcode"></i>
            <span>PROPIEDADES</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="@if (array_get($menu,'level_2') === 'todas') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedades') }}"><i class="fa fa-building"></i> Propiedades Enlistadas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'myproperties') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedad/ingresadas') }}"><i class="fa fa-list"></i> Mis Propiedades Ingresadas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'vendidas') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedad/vendidas') }}"><i class="fa fa-list"></i> Propiedades Vendidas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'alquiladas') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedad/alquiladas') }}"><i class="fa fa-list"></i> Propiedades alquiladas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'despublicadas') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedad/despublicadas') }}"><i class="fa fa-low-vision"></i> Despublicadas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'revision') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedad/revision') }}"><i class="fa fa-search"></i> Propiedades pendientes</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'destacadas') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedades-destacadas') }}"><i class="fa fa-star"></i> Propiedades Destacadas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'oro') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedades-oro') }}"><i class="fa fa-bullhorn"></i> Propiedades en Ganga</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'rotulos') active  @endif treeview">
              <a href="{{ url('ControlPanel/rotulos') }}"><i class="fa fa-bullhorn"></i> Rotulos</a>
            </li>
            </li>
          </ul>
        </li>


        <li class="@if (array_get($menu,'level_1') === 'staff') active  @endif treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>STAFF</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            {{-- <li class="@if (array_get($menu,'level_2') === 'super-administradores') active  @endif treeview">
              <a href="{{ url('ControlPanel/super-administradores') }}"><i class="fa fa-star"></i> Super administradores</a>
            </li> --}}
            <li class="@if (array_get($menu,'level_2') === 'administradores') active  @endif treeview">
              <a href="{{ url('ControlPanel/administradores') }}"><i class="fa fa-star-half-o"></i> Administradores</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'supervisores') active  @endif treeview">
              <a href="{{ url('ControlPanel/supervisores') }}"><i class="fa fa-star-o"></i> Supervisores</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'agentes') active  @endif treeview">
              <a href="{{ url('ControlPanel/agentes') }}"><i class="fa fa-user"></i> Agentes</a>
            </li>
          </ul>
        </li>

        {{-- <li class="@if (array_get($menu,'level_1') === 'configuraciones') active  @endif treeview">
          <a href="#">
            <i class="fa fa-gears"></i>
            <span>CONFIGURACIONES</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="@if (array_get($menu,'level_2') === 'caracteristicas') active  @endif treeview">
              <a href="{{ url('ControlPanel/caracteristicas') }}"><i class="fa fa-bars"></i> Caracteristicas propiedades</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'roles-administrativos') active  @endif treeview">
              <a href="{{ url('ControlPanel/roles-administrativos') }}"><i class="fa fa-vcard"></i> Roles administrativos</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'status') active  @endif treeview">
              <a href="{{ url('ControlPanel/status') }}"><i class="fa fa-bolt"></i> Estatus sistema</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'tipo-de-propiedades') active  @endif treeview">
              <a href="{{ url('ControlPanel/tipo-de-propiedades') }}"><i class="fa fa-bars"></i> Tipo propiedades</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'unidades-de-medida') active  @endif treeview">
              <a href="{{ url('ControlPanel/unidades-de-medida') }}"><i class="fa fa-balance-scale"></i> Unidades de medida</a>
            </li>
          </ul>
        </li> --}}
      </ul>
    </section>
</aside>