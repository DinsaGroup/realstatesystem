<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
          <div class="pull-left image">
            <img src="{{ url('img/user/'. Auth::user()->photo ) }}" class="img-circle" alt="User Image" />
          </div>
          <div class="pull-left info">
            <p>{{ ucWords(Auth::user()->name) }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU PRINCIPAL</li>

        {{-- <li class="@if (array_get($menu,'level_1') === 'dashboard') active  @endif treeview">
          <a href="{{ url('ControlPanel') }}">
            <i class="fa fa-dashboard"></i> <span>DASHBOARD</span></i>
          </a>
        </li> --}}

        <li class="@if (array_get($menu,'level_1') === 'clientes') active  @endif treeview">

          <a href="#">
            <i class="fa fa-address-book""></i>
            <span>PROPIETARIOS</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="@if (array_get($menu,'level_2') === 'clientes') active  @endif treeview">
              <a href="{{ url('ControlPanel/clientes') }}"><i class="fa fa-users"></i> Propietarios</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'create') active  @endif treeview">
              <a href="{{ url('ControlPanel/clientes/create') }}"><i class="fa fa-plus-circle"></i> Ingresar nuevo</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'testimonios') active  @endif treeview">
              <a href="{{ url('ControlPanel/testimonios') }}"><i class="fa fa-book"></i> Testimonios</a>
            </li>
          </ul>
        </li>

        <li class="@if (array_get($menu,'level_1') === 'propiedades') active  @endif treeview">
          <a href="#">
            <i class="fa fa-barcode"></i>
            <span>PROPIEDADES</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="@if (array_get($menu,'level_2') === 'todas') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedades') }}"><i class="fa fa-building"></i> Propiedades Enlistadas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'myproperties') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedad/ingresadas') }}"><i class="fa fa-list"></i> Mis Propiedades Ingresadas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'vendidas') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedad/vendidas') }}"><i class="fa fa-list"></i> Propiedades Vendidas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'alquiladas') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedad/alquiladas') }}"><i class="fa fa-list"></i> Propiedades alquiladas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'destacadas') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedades-destacadas') }}"><i class="fa fa-star"></i> Propiedades Destacadas</a>
            </li>
            <li class="@if (array_get($menu,'level_2') === 'oro') active  @endif treeview">
              <a href="{{ url('ControlPanel/propiedades-oro') }}"><i class="fa fa-bullhorn"></i> Propiedades en Ganga</a>
            </li>
            
          </ul>
        </li>
      </ul>
    </section>
</aside>