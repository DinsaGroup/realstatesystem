@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI43-yj5Hf3fZo8eyIrWULm_VGwbXZecs&libraries=places"  type="text/javascript"></script>
{!! Form::model($property, [
                    'method' => 'PATCH',
                    'url' => ['ControlPanel/propiedades', $property->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                {{ csrf_field() }}

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{ $title }} ({{$property->code}})

                <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger pull-right "><i class="fa fa-times"></i> Cancelar</a>
                <div class="col-sm-2 pull-right">
                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary form-control ']) !!}
                </div>

            </h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
        </section>

        <section class="content">

            <section class="content">
                <div class="container-fluid">
                    <div class="row" style="margin-bottom: 15px;">
                        <div class="form-group">
                                <input type="hidden" name="user_id" value="{{ $property->user->get(0)->user->id }}">
                                <div class="col-sm-4">
                                    {!! Form::label('user_name', 'Buscar propietario * ', ['class' => 'control-label']) !!}
                                    <div class="">
                                        {!! Form::text('user_name', ucwords($property->user->get(0)->user->name), ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Ej: Carlos Torrez', 'readonly']) !!}
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    {!! Form::label('landowner_type_id', 'Relación *', ['class' => 'control-label']) !!}
                                    <div class="">
                                        {!! Form::select('landowner_type_id', $landownerTypes, $property->user->get(0)->user_type->id, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar opción']) !!}
                                    </div>
                                </div>

                                @if(Auth::user()->hasRole('agent'))
                                    <div class="col-sm-2 pull-right">
                                        {!! Form::label('published', 'Estatus * ', ['class' => 'control-label']) !!}
                                        <div class="">
                                            @if($property->published)
                                            {!! Form::select('published', [ 'true'=>'Si, publicar ahora', 'false'=>'No, despublicar'], 'true', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar']) !!}
                                            @else
                                            {!! Form::select('published', [ 'true'=>'Si, publicar ahora', 'false'=>'No, despublicar'], 'false', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar']) !!}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-2 pull-right">
                                        {!! Form::label('checked', '¿Faltan datos? *', ['class' => 'control-label']) !!}
                                        <div class="">
                                            @if($property->checked)
                                            {!! Form::select('checked', [ 'true'=>'Si, están completos.', 'false'=>'No, falta información'], 'true', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar']) !!}
                                            @else
                                            {!! Form::select('checked', [ 'true'=>'Si, están completos.', 'false'=>'No, falta información'], 'false', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar']) !!}
                                            @endif
                                        </div>
                                    </div>
                                @else
                                    
                                    <div class="col-sm-2 pull-right">
                                        {!! Form::label('published', '¿Desea publicar? *', ['class' => 'control-label']) !!}
                                        <div class="">
                                            @if($property->published)
                                            {!! Form::select('published', [ 'true'=>'Si, publicar ahora', 'false'=>'No, despublicar'], 'true', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar']) !!}
                                            @else
                                            {!! Form::select('published', [ 'true'=>'Si, publicar ahora', 'false'=>'No, despublicar'], 'false', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar']) !!}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-2 pull-right">
                                        {!! Form::label('checked', '¿Faltan datos? *', ['class' => 'control-label']) !!}
                                        <div class="">
                                            @if($property->checked)
                                            {!! Form::select('checked', [ 'true'=>'Si, están completos.', 'false'=>'No, falta información'], 'true', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar']) !!}
                                            @else
                                            {!! Form::select('checked', [ 'true'=>'Si, están completos.', 'false'=>'No, falta información'], 'false', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar']) !!}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-2 pull-right">
                                        {!! Form::label('agent_id', 'Agente *', ['class' => 'control-label']) !!}
                                        <div class="">
                                            {!! Form::select('agent_id', $agents, null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar agente']) !!}
                                        </div>
                                    </div>
                                @endif       
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 15px;">
                        <div class="form-group">
                            <div class="col-sm-3">
                                {!! Form::label('property_type_id', 'Tipo de propiedad *', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::select('property_type_id', $propertyTypes, $property->property_type->id, ['class' => 'form-control', 'required' => 'required',]) !!}
                                </div>
                            </div>

                             <div class="col-sm-3">
                                {!! Form::label('ventaRenta', '¿Venta ó Renta? *', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::select('ventaRenta', ['venta' => 'Solo venta', 'renta' => 'Solo renta', 'venta/renta'=> 'Venta & Renta'], $property->ventaRenta, ['class' => 'form-control', 'required' => 'required',]) !!}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                {!! Form::label('exclusividad', '¿Es exclusiva ? *', ['class' => 'control-label ']) !!}
                                <div class="">
                                    @if($property->exclusividad)
                                    {!! Form::select('exclusividad', ['true' => 'Tenemos exclusividad', 'false' => 'No es exclusiva'], 'true', ['class' => 'form-control', 'required' => 'required',]) !!}
                                    @else
                                    {!! Form::select('exclusividad', ['true' => 'Tenemos exclusividad', 'false' => 'No es exclusiva'], 'false', ['class' => 'form-control', 'required' => 'required',]) !!}
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-2">
                                {!! Form::label('disponible', '¿Esta disponible? *', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::select('disponible', ['true' => 'Sí', 'false' => 'No'], $property->disponible, ['class' => 'form-control', 'required' => 'required',]) !!}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                {!! Form::label('estadoTmp', 'Estado *', ['class' => 'control-label']) !!}
                                <div class="">
                                    {{-- {!! Form::select('estadoTmp', ['disponible' => 'Disponible', 'alquilado' => 'Alquilado','vendida'=>'Vendida'], $property->estadoTmp, ['class' => 'form-control', 'required' => 'required',]) !!} --}}
                                    {!! Form::select('estadoTmp', [ 'na'=>'NA','disponible' => 'Disponible', 'alquilado' => 'Alquilado','vendida'=>'Vendida','no disponible'=>'No Disponible','pendiente confirmación'=>'Pendiente Confirmación','es incontactable'=>'Es Incontactable','viven los dueños'=>'Viven los dueños','no trabaja con bienes raices'=>'No trabaja con bienes raices','en proceso de venta'=>'En proceso de venta','en proceso de alquiler'=>'En proceso de alquiler'], $property->estadoTmp, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar opción']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 15px;">
                        <div class="form-group">  
                            <div class="col-sm-2">
                                {!! Form::label('precioVenta', 'Precio de Venta (U$)', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::text('precioVenta', number_format($property->precioVenta,2,'.',','), ['class' => 'form-control text-right only_numbers',  'placeholder' => '1,000,000.00']) !!}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                {!! Form::label('precioAlquiler', 'Precio Renta (U$)', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::text('precioAlquiler', number_format($property->precioAlquiler,2,'.',','), ['class' => 'form-control text-right only_numbers',  'placeholder' => '600.00']) !!}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                {!! Form::label('precioPorV2', 'Precio Vara (U$)', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::text('precioPorV2', number_format($property->precioPorV2,2,'.',','), ['class' => 'form-control text-right only_numbers',  'placeholder' => '150.00']) !!}
                                </div>
                            </div>
                                            
                            

                            <div class="col-sm-2">
                                {!! Form::label('areaConstruccion', 'Área construida (mts2)', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::text('areaConstruccion', number_format($property->areaConstruccion,2,'.',','), ['class' => 'form-control text-right only_numbers',  'placeholder' => '450']) !!}
                                </div>
                            </div>
                            <div class="col-sm-2">
                                {!! Form::label('antiguedad', 'Año de construcción ', ['class' => ' control-label']) !!}
                                <div class="">
                                    {!! Form::select('antiguedad', $years, $property->antiguedad, ['class' => 'form-control',  'placeholder' => 'Seleccionar']) !!}
                                </div>
                            </div>
                            <div class="col-sm-2">
                                    {!! Form::label('rotulo', 'Rotulo ', ['class' => 'control-label']) !!}
                                    <div class="">
                                        {!! Form::text('rotulo', array_get($property->sign,'0.code'), ['class' => 'form-control',  'placeholder'=>'COD-215']) !!}
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 15px;">
                        <div class="form-group">
                                <div class="col-sm-2">
                                    {!! Form::label('acceso', 'Fácil acceso?', ['class' => 'control-label']) !!}
                                    <div class="">
                                        {!! Form::select('acceso', ['true' => 'Sí', 'false' => 'No'], $property->acceso, ['class' => 'form-control', ]) !!}
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    {!! Form::label('topografia', 'Topografía ', ['class' => ' control-label']) !!}
                                    <div class="">
                                        {{-- {!! Form::text('topografia', $property->topografia, ['class' => 'form-control',  'placeholder'=>'']) !!} --}}
                                        {!! Form::select('topografia', ['ascendente' => 'Ascendente','descendente'=>'Descendente','ondulada'=>'Ondulada','plana' => 'Plana','semi-plana' => 'Semi-plana'], $property->topografia, ['class' => 'form-control',  'placeholder' => 'Seleccionar']) !!}
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    {!! Form::label('status_id', 'Estatus propiedad *', ['class' => 'control-label']) !!}
                                    <div class="">
                                        {!! Form::select('status_id', $status, $property->status->name, ['class' => 'form-control', 'required' => 'required',]) !!}
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    {!! Form::label('measure_unit_id', 'Unidad de Medida', ['class' => 'control-label']) !!}
                                    <div class="">
                                        {!! Form::select('measure_unit_id', $measureUnits, $property->measure_unit->id, ['class' => 'form-control' ]) !!}
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    {!! Form::label('areaTotal', 'Área total', ['class' => 'control-label']) !!}
                                    <div class="">
                                        {!! Form::text('areaTotal', number_format($property->areaTotal,2,'.',','), ['class' => 'form-control text-right only_numbers',  'placeholder' => '200']) !!}
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    {!! Form::label('comision', '% de Comisión', ['class' => 'control-label']) !!}
                                    <div class="">
                                        {!! Form::text('comision', null, ['class' => 'form-control text-right',  'placeholder' => 'Ej: 5%, 7%, 10% ...']) !!}
                                    </div>
                                </div>
                                
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 15px;">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::label('description', 'Descripción del inmueble visible al usuario : ', ['class' => ' control-label']) !!}
                                <div class="">
                                    {!! Form::textArea('description', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'']) !!}
                                </div>
                            </div>                        
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 15px;">
                        <div class="col-sm-12 form-group">
                           
                                {!! Form::label('address', 'Dirección : ', ['class' => ' control-label']) !!}
                                <div class="">
                                    {!! Form::text('address', $property->address, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Av. Vigo, La Lomita', 'id' => 'address']) !!}
                                </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                {!! Form::label('sector', 'Sector : ', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::text('sector', $property->sector, ['class' => 'form-control', 'placeholder' => 'Los Robles', 'id' => 'sector', 'required' => 'required']) !!}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                {!! Form::label('city', 'Ciudad : ', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::text('city', $property->city, ['class' => 'form-control', 'placeholder' => 'Managua', 'id' => 'city', 'required' => 'required']) !!}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                {!! Form::label('state', 'Departamento : ', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::text('state', $property->state, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Managua', 'id' => 'state']) !!}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                {!! Form::label('country', 'País : ', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::text('country', $property->country, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nicaragua', 'id' => 'country']) !!}
                                </div>
                            </div>
                            <div class="col-sm-2 pull-right">
                                {!! Form::label('longitude', 'Longitud : ', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::text('longitude', $property->longitude, ['class' => 'form-control', 'required' => 'required', 'placeholder' => '-86.256', 'id' => 'longitude', 'readonly']) !!}
                                </div>
                            </div>
                            
                            <div class="col-sm-2 pull-right">
                                {!! Form::label('latitude', 'Latitude : ', ['class' => 'control-label']) !!}
                                <div class="">
                                    {!! Form::text('latitude', $property->latitude, ['class' => 'form-control', 'required' => 'required', 'placeholder' => '12.1542', 'id' => 'latitude', 'readonly']) !!}
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 15px;">
                        <div class="form-group">
                            <a href="#googlemaps" class="btn btn-primary col-sm-4" data-toggle="modal">Buscar la propiedad con ayuda de google maps</a>
                        </div>
                    </div>

                </div>
            </section>

            <section class="content-header">
                <h1>{{ $title2 }}</h1>
                <small>Seleccione las características de la propiedad, puede escoger más de una.</small>
            </section>

            <section class="content">
                <div class="row w-margin">               
                    <?php $pos = 0; $checked=false;?>

                        <div class="col-md-3">
                            @foreach($features as $item)

                                @foreach($result as $value)
                                    @if($value->feature_id == $item->id)
                                        <?php 
                                            $checked = true; 

                                        ?>
                                    @endif
                                @endforeach

                                

                                @if($checked)
                                    {{ Form::checkbox('feature['.$item->id.']', $item->name, true, ['class' => 'col-md-1 class['.$item->id.']', 'id' => 'feat_'.$item->id]) }}
                                    <?php $checked=false; ?>
                                @else
                                    {{ Form::checkbox('feature['.$item->id.']', $item->name, null, ['class' => 'col-md-1 class['.$item->id.']', 'id' => 'feat_'.$item->id]) }}
                                @endif

                                {!! Form::label($item->id, $item->name, ['class' => 'col-md-11', 'id' => 'label_'.$item->id]) !!}

                                @if($pos>$count)
                                    </div>
                                    <div class="col-md-3">
                                    <?php $pos=0; ?>
                                @else
                                    
                                    <?php $pos++; ?>
                                @endif
                                 
                                
                            @endforeach
                            <input type="hidden" id="rooms_cant" value="{{ array_get($room, '0.quantity') }}"></input>
                            <input type="hidden" id="restrooms_cant" value="{{ array_get($restroom, '0.quantity') }}"></input>
                            <input type="hidden" id="parking" value="{{ $estacionamiento }}"></input>
                            <input type="hidden" id="pool_largo" value="{{ array_get($pool, '0.largo') }}"></input>
                            <input type="hidden" id="pool_ancho" value="{{ array_get($pool, '0.ancho') }}"></input>
                            <input type="hidden" id="pool_alto" value="{{ array_get($pool, '0.alto') }}"></input>
                            <input type="hidden" id="ac_tipo" value="{{ array_get($ac, '0.type') }}"></input>
                            <input type="hidden" id="wh_tipo" value="{{ array_get($wh, '0.type') }}"></input>
                        </div>
                </div>
            </section>

            <section class="content-header">
                <h1>{{ $title4 }}</h1>
                <small>Agregue las notas necesarias para brindar mayor información del inmueble.</small>
            </section>

            <section class="content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="">
                            {!! Form::textArea('notes', $property->notes, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </section>

            <section class="content-header">
                <h1>{{ $title3 }}</h1>
                <small>Agregue la información necesaria para los motores de búsqueda.</small>
            </section>

            <section class="content">
                <div class="row">
                    
                
                    <div class="col-sm-6">
                        {!! Form::label('metaTitleEs', 'Meta títulos en Español : ', ['class' => ' control-label ']) !!}
                        <div class="">
                            {!! Form::textArea('metaTitleEs', $property->metaTitleEs, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-6">
                    
                        {!! Form::label('metaTitleEn', 'Meta títulos en Inglés : ', ['class' => ' control-label ']) !!}
                        <div class="">
                            {!! Form::textArea('metaTitleEn', $property->metaTitleEn, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    {{-- <div class="col-sm-6">
                        {!! Form::label('metaKeywordEs', 'Meta keywords en Español : ', ['class' => ' control-label ']) !!}
                        <div class="">
                            {!! Form::textArea('metaKeywordEs', $property->metaKeywordEs, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="col-sm-6">
                        {!! Form::label('metaKeywordEn', 'Meta keywords en Inglés : ', ['class' => ' control-label ']) !!}
                        <div class="">
                            {!! Form::textArea('metaKeywordEn', $property->metaKeywordEn, ['class' => 'form-control']) !!}
                        </div>
                    </div> --}}
                </div>
            </section>
        </section>

    </div>

{!! Form::close() !!}


<div id="googlemaps" class="modal fade" role="dialog">     
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Localizar propiedad </h4>
            </div>
            <div class="modal-body"> 
                
                <div class="row" style="margin: 0 0 25px 0;">
                    <h4>Buscador</h4>
                    <p>Escriba el sector donde se ubica la propiedad, seleccione el puntero y ubiquelo donde se encuentra el inmueble.</p>
                    <div class="col-sm-5 ">
                        {!! Form::label('zone_name', 'Buscar zona en el mapa : ', ['class' => 'control-label' ]) !!}
                        <div class="">
                            {!! Form::text('zone_name', null, ['class' => 'form-control', 'id' => 'searchmap' ]) !!}
                        </div>
                    </div>
                    <div class="col-sm-2 ">
                        {!! Form::label('latitude', 'Latitud : ', ['class' => 'control-label']) !!}
                        <div class="">
                            {!! Form::text('latitude', null, ['class' => 'form-control', 'readonly', 'id' =>'lat'  ]) !!}
                        </div>
                    </div>

                    <div class="col-sm-2 ">
                        {!! Form::label('longitude', 'Longitud : ', ['class' => 'control-label']) !!}
                        <div class="">
                            {!! Form::text('longitude', null, ['class' => 'form-control', 'readonly', 'id' =>'lng' ]) !!}
                        </div>
                    </div>
                
                    <div id="mapCanvas" class="col-sm-12" style="height: 450px">
                    </div>
                </div>
                

            </div>
            
        </div>
    </div>   
</div>

@endsection
@section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">

        function initializeMap() {
            
            // Google maps
            var map = new google.maps.Map(document.getElementById('mapCanvas'),{
                center:{
                    lat: 12.135,
                    lng: -86.270
                },
                zoom:12
            });

            var marker = new google.maps.Marker({
                position:{
                    lat: 12.135,
                    lng: -86.270
                },
                map:map,
                draggable:true
            });

            var defaultBounds = new google.maps.LatLngBounds(
              new google.maps.LatLng(10.826900763796283, -87.61033203124998),
              new google.maps.LatLng(14.0450044105228, -83.21580078124998)
            );

            var options = {
              bounds: defaultBounds,
              types: ['establishment', 'geocode'],
              componentRestrictions: {country: 'ni'}
            };

            var input = document.getElementById('searchmap');

            // Create the autocomplete object
            var searchBox = new google.maps.places.Autocomplete(input, options);
            google.maps.event.addListener(searchBox,'place_changed',function(){

                var place = searchBox.getPlace();
                var bounds = new google.maps.LatLngBounds();
                var i, place;
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location); 
                map.fitBounds(bounds);
                map.setZoom(16);
                console.log('Respuesta :' + place.adr_address);
                console.log(place.adr_address.substring('<span class="locality">','</span>'));
            });

            google.maps.event.addListener(marker,'position_changed',function(){

                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();
                $('#lat').val(lat);
                $('#lng').val(lng);
                $('#latitude').val(lat);
                $('#longitude').val(lng);
            });
        }

        $("#googlemaps").on('shown.bs.modal', function () {
                initializeMap();
        });
    </script>
    <script>
        $(document).ready(function() {

            $('#user_name').autocomplete({
                minLenght:2,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-client') }}'
            });

            var rooms_cant = $('#rooms_cant').val();
            var restrooms_cant = $('#restrooms_cant').val();
            var pool_largo = $('#pool_largo').val();
            var pool_ancho = $('#pool_ancho').val();
            var pool_alto = $('#pool_alto').val();
            var ac_tipo = $('#ac_tipo').val();
            var wh_tipo = $('#wh_tipo').val();
            var option1, option2, option3 = '';
            var opcion1, opcion2 = '';

            if (ac_tipo == 'ventana') {
                option1 = 'selected';
                option2 = '';
                option3 = '';
            } else if (ac_tipo == 'central') {
                option2 = 'selected';
                option1 = '';
                option3 = '';
            } else {
                option3 = 'selected';
                option2 = '';
                option1 = '';
            }

            if (wh_tipo == 'electrico') {
                opcion1 = 'selected';
                opcion2 = '';
            } else {
                opcion2 = 'selected';
                opcion1 = '';
            }

            if ($('#feat_6').is(':checked')) {
                $('#label_6').append('<div id="rooms" class="form-group"><label class="control-label col-md-6" for="rooms">Cantidad :</label><div class="col-md-6"><input name="rooms" value="'+rooms_cant+'" type="text" class="form-control"></div></div>')
            }

            if ($('#feat_7').is(':checked')) {
                $('#label_7').append('<div id="restrooms" class="form-group"><label class="control-label col-md-6" for="restrooms">Cantidad :</label><div class="col-md-6"><input value="'+restrooms_cant+'" name="restrooms" type="text" class="form-control"></div></div>')
            }

            if ($('#feat_14').is(':checked')) {
                $('#label_14').append('<div id="estacionamiento" class="form-group"><label class="control-label col-md-6" for="estacionamiento">Cantidad :</label><div class="col-md-6"><input value="'+$('#parking').val()+'" name="estacionamiento" type="text" class="form-control"></div></div>')
            }

            

            if ($('#feat_17').is(':checked')) {
                $('#label_17').append('<div id="largo" class="form-group"><label class="control-label col-md-6" for="largo">Largo (m):</label><div class="col-md-6"><input value="'+pool_largo+'" name="largo" type="text" class="form-control"></div></div><div id="ancho" class="form-group"><label class="control-label col-md-6" for="ancho">Ancho (m) :</label><div class="col-md-6"><input value="'+pool_ancho+'" name="ancho" type="text" class="form-control"></div></div><div id="alto" class="form-group"><label class="control-label col-md-6" for="alto">Alto (m) :</label><div class="col-md-6"><input value="'+pool_alto+'" name="alto" type="text" class="form-control"></div></div>')
            }

            if ($('#feat_18').is(':checked')) {
                $('#label_18').append('<div id="ac" class="form-group"><label class="control-label col-md-4" for="ac">Tipo :</label><div class="col-md-8"><select id="ac" class="form-control" required="required" name="ac"><option '+option2+' value="central">Central</option><option '+option3+' value="split">Split</option><option '+option1+' value="ventana">Ventana</option></select></div></div>')
            }

            if ($('#feat_19').is(':checked')) {
                $('#label_19').append('<div id="water_heater" class="form-group"><label class="control-label col-md-4" for="water_heater">Tipo :</label><div class="col-md-8"><select id="water_heater" class="form-control" required="required" name="water_heater"><option '+opcion1+' value="electrico">Eléctrico</option><option '+opcion2+' value="gas">Gas</option></select></div></div>')
            }

            $('#feat_6').click(function() {
                if ($(this).is(':checked')) {
                    $('#label_6').append('<div id="rooms" class="form-group"><label class="control-label col-md-6" for="rooms">Cantidad :</label><div class="col-md-6"><input name="rooms" type="text" class="form-control"></div></div>')
                } else {
                    $('#rooms').remove();
                }
            });

            $('#feat_14').click(function() {
                if ($(this).is(':checked')) {
                    $('#label_14').append('<div id="estacionamiento" class="form-group"><label class="control-label col-md-6" for="estacionamiento">Cantidad :</label><div class="col-md-6"><input name="estacionamiento" type="text" class="form-control"></div></div>')
                } else {
                    $('#estacionamiento').remove();
                }
            });

            $('#feat_7').click(function() {
                if ($(this).is(':checked')) {
                    $('#label_7').append('<div id="restrooms" class="form-group"><label class="control-label col-md-6" for="restrooms">Cantidad :</label><div class="col-md-6"><input name="restrooms" type="text" class="form-control"></div></div>')
                } else {
                    $('#restrooms').remove();
                }
            });

            $('#feat_17').click(function() {
                if ($(this).is(':checked')) {
                    $('#label_17').append('<div id="largo" class="form-group"><label class="control-label col-md-6" for="largo">Largo (m):</label><div class="col-md-6"><input name="largo" type="text" class="form-control"></div></div><div id="ancho" class="form-group"><label class="control-label col-md-6" for="ancho">Ancho (m) :</label><div class="col-md-6"><input name="ancho" type="text" class="form-control"></div></div><div id="alto" class="form-group"><label class="control-label col-md-6" for="alto">Alto (m) :</label><div class="col-md-6"><input name="alto" type="text" class="form-control"></div></div>')
                } else {
                    $('#largo').remove();
                    $('#ancho').remove();
                    $('#alto').remove();
                }
            });

            $('#feat_18').click(function() {
                if ($(this).is(':checked')) {
                    $('#label_18').append('<div id="ac" class="form-group"><label class="control-label col-md-4" for="ac">Tipo :</label><div class="col-md-8"><select id="ac" class="form-control" required="required" name="ac"><option selected="selected" value="">Seleccione uno</option><option value="central">Central</option><option value="split">Split</option><option value="ventana">Ventana</option></select></div></div>')
                } else {
                    $('#ac').remove();
                }
            });

            $('#feat_19').click(function() {
                if ($(this).is(':checked')) {
                    $('#label_19').append('<div id="water_heater" class="form-group"><label class="control-label col-md-4" for="water_heater">Tipo :</label><div class="col-md-8"><select id="water_heater" class="form-control" required="required" name="water_heater"><option selected="selected" value="">Seleccione uno</option><option value="electrico">Eléctrico</option><option value="gas">Gas</option></select></div></div>')
                } else {
                    $('#water_heater').remove();
                }
            });


        });  
    </script>
@endsection