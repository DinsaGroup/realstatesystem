@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

<div class="content-wrapper">
    <section class="content-header">
    
        <h1>{{ $title }} <a href="{{ url('ControlPanel/propiedades-destacadas/create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
        <small>{!! $subtitle !!}</small>
      
    </section>

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-building"></i> Listado de inmuebles</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="transaction_history" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th width="75px">CODE</th>
                        <th>INMUEBLE</th>
                        <th>P. VENTA</th>
                        <th>P. RENTA</th>
                        <th>DESDE</th>
                        <th>HASTA</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($featured as $item)
                    <tr class="gradeA @if(!$item->property->published) {{ 'despublished'}} @endif" > 
                        <td class="text-center">
                            <a href="#property" class="details" data-id="{{ $item->property->id }}" data-toggle="modal">{{ $item->id }}</a>
                        </td>
                        <td class="text-center">
                            <a href="#property" class="details" data-id="{{ $item->property->id }}" data-toggle="modal">{{ $item->property->code }}</a>
                        </td>
                        <td>
                            <a href="#property" class="details" data-id="{{ $item->property->id }}" data-toggle="modal">{{ $item->property->property_type->name }} en {{ $item->property->ventaRenta }} en el sector de {{ $item->property->sector }}<br> {{ $item->property->city }}, {{ $item->property->state }}</a>
                        </td>
                        <td class="text-right">
                            <a href="#property" class="details" data-id="{{ $item->property->id }}" data-toggle="modal">U$ {{ number_format($item->property->precioVenta,2,'.',',') }}</a>
                        </td>
                        <td class="text-right">
                            <a href="#property" class="details" data-id="{{ $item->property->id }}" data-toggle="modal">U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }}</a>
                        </td>
                        <td class="text-center">
                            <a href="#property" class="details" data-id="{{ $item->property->id }}" data-toggle="modal">{{ $item->start }}</a>
                        </td>
                        <td class="text-center">
                            <a href="#property" class="details" data-id="{{ $item->property->id }}" data-toggle="modal">{{ $item->end }}</a>
                        </td>
                        <td class="text-center">
                            <a href="{{ url('ControlPanel/propiedades-destacadas/'.$item->id.'/edit') }}" class="details"  >Editar periodo</a>
                        </td>
                    </tr>
                  @endforeach
                </tbody>
                
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
            </div> {{-- col-md-12 --}}
        </div>

    </section>

</div>

<div id="property" class="modal fade" role="dialog">     
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">DETALLE DE LA PROPIEDAD</h4>
            </div>
            <div class="modal-body"> 

                <div class="row" style="margin: 0 0 25px 0;">
                    <h4>PROPIEDAD</h4>
                    <div class="col-xs-12">
                        <div class="col-sm-1 box_detail">
                            <span class="note">Codigo</span>
                            <span id="code" class="valor"> CVR-452</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Tipo de propiedad</span>
                            <span id="property_type" class="valor"> Casa de habitación</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Venta / Renta</span>
                            <span id="venta-renta" class="valor"> 12456235</span>
                        </div>
                        <div class="col-sm-1 box_detail">
                            <span class="note">Exclusiva</span>
                            <span id="exclusividad" class="valor"> Si</span>
                        </div>
                        <div class="col-sm-1 box_detail">
                            <span class="note">Disponible</span>
                            <span id="disponible" class="valor"> No</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Precio venta</span>
                            <span id="precioVenta" class="valor"> U$ 2,152.25</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Precio renta</span>
                            <span id="precioRenta" class="valor"> U$ 152.25</span>
                        </div>
                        <div class="col-sm-1 box_detail">
                            <span class="note">Precio x vara</span>
                            <span id="precioxV2" class="valor"> U$ 12.50</span>
                        </div>
                        
                    </div>

                    <div class="col-xs-12">
                        <div class="col-sm-1 box_detail">
                            <span class="note">Area Total</span>
                            <span id="areaTotal" class="valor"> 1254 mts</span>
                        </div>
                        <div class="col-sm-1 box_detail">
                            <span class="note">Area Construida</span>
                            <span id="areaConstruida" class="valor"> 125 mts</span>
                        </div>
                        <div class="col-sm-1 box_detail">
                            <span class="note">Construida en</span>
                            <span id="antiguedad" class="valor"> 1925</span>
                        </div>
                        <div class="col-sm-1 box_detail">
                            <span class="note">Fácil acceso</span>
                            <span id="acceso" class="valor"> Si</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Topografia</span>
                            <span id="topografia" class="valor"> con problemas de acceso al inicio</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Estado de la propiedad</span>
                            <span id="status" class="valor" >Buen estado</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Ingresada</span>
                            <span id="created" class="valor"> con problemas de acceso al inicio</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Actualizada</span>
                            <span id="updated" class="valor"> con problemas de acceso al inicio</span>
                        </div>
                        
                    </div>

                    <div class="col-xs-12">
                        <div class="col-sm-12 box_detail">
                            <span class="note">Descripción</span>
                            <span id="description" class="valor"> Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</span>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="col-sm-4 box_detail">
                            <span class="note">Dirección</span>
                            <span id="address" class="valor" >De donde orino el perro 2c al sur, 3c este, mano derecha casa #q123</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Sector</span>
                            <span id="sector" class="valor" >Barrio no tengo idea</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Municipio</span>
                            <span id="city" class="valor" >Ciudad Sandino</span>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Ciudad</span>
                            <span id="state" class="valor" >Managua</span>
                        </div>
                        <div class="col-sm-1 box_detail">
                            <span class="note">País</span>
                            <span id="country" class="valor" >Nicaragua</span>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="col-sm-6 box_detail">
                            <span class="note">Ubicación</span>
                            <a href="#" id="ubicacion" target="_blank"><i class="fa fa-map-marker"></i> Ver ubicación en google maps</a>
                        </div>
                        <div class="col-sm-2 box_detail">
                            <span class="note">Agente</span>
                            <span id="agente" class="valor" >Cualquier nombre</span>
                        </div>
                        <div class="col-sm-1 box_detail">
                            <span class="note">Comisión x venta</span>
                            <span id="comision" class="valor"> 20%</span>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="col-sm-12 box_detail">
                        <span class="note">Notas</span>
                        <span id="notasinternas" class="valor"> Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</span>
                    </div>
                </div>
            
                <div class="row" style="margin: 0 0 25px 0;">
                    <h4>FOTOS</h4>
                    <div class="col-xs-12" id="files">
                        <div class="col-sm-2 box_detail photoProperty">
                            <img src="{{  url('img/properties/no-img.png')}}" width="100%" />
                        </div>
                    </div>
                </div>

                <div class="row" style="margin: 0 0 25px 0;">
                    <h4>CONTACTOS DE LA PROPIEDAD</h4>
                    <div class="col-xs-12">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="75">Codigo</th>
                                    <th width="15%">Nombre Completo</th>
                                    {{-- <th width="">Relación</th> --}}
                                    <th width="10%">Email</th>
                                    <th width="15%">Teléfonos</th>
                                    <th width="">Notas</th>
                                </tr>
                            </thead>

                            <tbody id="contacts">
                                <tr></tr>
                            </tbody>
                        </table>
                        
                    </div>

                    
                </div>
 
            </div>
            <div class="modal-footer">
                
                    <a id="add_photos" href="#" class="btn btn-primary col-md-2" ><i class="fa fa-picture-o"></i> Subir fotos</a>
                
                    <a id="edit" href="#" class="btn btn-primary col-md-1" ><i class="fa fa-pencil"></i> Editar</a>

                    @if(!Auth::user()->hasRole('agent'))

                    <a id="checked" href="#" class="btn btn-success " ><i class="fa fa-check"></i> Revisado / Publicar</a>

                    <a id="disp" href="#" class="btn btn-success " ><i class="fa fa-check"></i> Disponible</a>

                    <a id="noDisponible" href="#" class="btn btn-danger " ><i class="fa fa-ban"></i> No Disponible</a>

                    <a id="alquilada" href="#" class="btn btn-warning " ><i class="fa fa-handshake-o"></i> Alquilada</a>

                    <a id="vendida" href="#" class="btn btn-danger " ><i class="fa fa-sign-out"></i> Vendida</a>

                    @endif
                
            </div>
        </div>
    </div>   
</div>

@endsection

@section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- page script -->
    <script>

        function delphoto(){

            $('.delphoto').click(function(e){
                e.preventDefault();
                var object = $(this);
                var $id = $(this).data('id');
                var $url =  '{{ url('ControlPanel/request/delphoto') }}';

                $.getJSON($url,{'id': $id },function(result){
                    
                    if(result ==='ok'){
                        object.parent().remove();
                        alert('La imagen fue borrado con éxito!');
                    }else{
                        alert('Hubo un problema, intente nuevamente!');
                    }
                });
            })
        }
        
        function action(){
            $('.details').click(function(){
                var $id = $(this).data('id');
                var $url = '{{ url('ControlPanel/request/property_detail') }}';
                var $edit = '{{ url('ControlPanel/propiedades/') }}' +'/'+ $id + '/edit';
                var $addphotos = '{{ url('ControlPanel/propiedad/') }}' +'/'+ $id + '/fotos/create';
                var $disponible = '{{ url('ControlPanel/request/property/disponible') }}' +'/'+ $id ;
                var $noDisponible = '{{ url('ControlPanel/request/property/nodisponible') }}' +'/'+ $id ;
                var $alquilada = '{{ url('ControlPanel/request/property/alquilada') }}' +'/'+ $id ;
                var $vendida = '{{ url('ControlPanel/request/property/vendida') }}' +'/'+ $id ;
                var $checked = '{{ url('ControlPanel/request/property/revisado-publicar') }}' +'/'+ $id ;

                var $delephoto = '{{ url('ControlPanel/deletephoto') }}' ;

                //Limpiar valores
                $('.valor').empty();
                $('#files').empty();
                $('#contacts').empty();

                $.getJSON( $url, {'id': $id }, function(result) {
                    console.log(result.contacts);
                    $.each(result.contacts, function(key,value){
                        $('tbody#contacts').append('<tr><td class="text-center">'+value.customer_id+'</td><td>'+value.name+'</td><td>'+value.email+'</td><td>'+value.house+' '+value.office+' '+ value.phone+'</td><td>'+value.comments+'</td></tr>');
                    });

                    // $('#customer_id').text(result.customer_id);
                    // $('#name').text(result.name);
                    // $('#house').text(result.house);
                    // $('#office').text(result.office);
                    // $('#phone').text(result.phone);
                    // $('#email').text(result.email);
                    // $('#private_note').text(result.private_note);
                    $('#code').text(result.code);
                    $('#property_type').text(result.property_type);
                    $('#venta-renta').text(result.venta_renta);
                    $('#exclusividad').text(result.exclusividad);
                    $('#disponible').text(result.disponible);
                    $('#precioVenta').text(result.precioVenta);
                    $('#precioRenta').text(result.precioRenta);
                    $('#precioxV2').text(result.precioxV2);
                    $('#areaTotal').text(result.areaTotal);
                    $('#areaConstruida').text(result.areaConstruida);
                    $('#antiguedad').text(result.antiguedad);
                    $('#acceso').text(result.acceso);
                    $('#topografia').text(result.topografia);
                    $('#description').text(result.description);
                    $('#address').text(result.address);
                    $('#sector').text(result.sector);
                    $('#city').text(result.city);
                    $('#state').text(result.state);
                    $('#country').text(result.country);
                    $('#latitude').text(result.latitude);
                    $('#longitude').text(result.longitude);
                    $('#agente').text(result.agente);
                    $('#status').text(result.status);
                    $('#notasinternas').text(result.notes);
                    $('#created').text(result.created);
                    $('#updated').text(result.updated);
                    $('#comision').text(result.comision);

                    var $google = '{{ url('ControlPanel/googlemaps/') }}' +'/'+ result.latitude + '/' + result.longitude ;

                    $('#edit').attr('href',$edit);
                    $('#add_photos').attr('href',$addphotos);
                    $('#ubicacion').attr('href',$google);
                    $('#disp').attr('href',$disponible);
                    $('#noDisponible').attr('href',$noDisponible);
                    $('#alquilada').attr('href',$alquilada);
                    $('#vendida').attr('href',$vendida);
                    $('#checked').attr('href',$checked);

                    if((result.photos).length > 0){
                        $.each(result.photos, function(key,value){
                            if(value.visible == 'true'){
                                var $link = '{{ url('img/properties/') }}' +'/'+ value.filename ;
                                // $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><a href="'+$delephoto+'/'+value.id+'" class="pull-right ">Eliminar</a><img src="'+ $link + '" width="100%" /><br></div>');
                                $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><a href="" data-id="'+value.id+'" class="pull-right delphoto">Eliminar</a><img src="'+ $link + '" width="100%" /></div>');
                            }
                        });

                        delphoto();

                    }else{
                        var $link = '{{  url('img/properties/no-img.png')}}';
                        $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><img src="'+ $link + '" width="100%" /></div>');
                    }
                });
                
            });
        }

        $(document).ready(function(){


            action();

            $('#search_button').click(function(){

                if( $('#search_box').val()!==''){

                      $('.gradeA').remove();
                      $('.loading').toggleClass('hidden');

                      var value = $('#search_box').val();
                      var price_from = $('#from').val();
                      var price_at = $('#at').val();
                      var propertyType = $('#propertyType').val();
                      // var rotulo = $('#rotulo').val();

                      var url = '{{ url('ControlPanel/request/properties') }}';
                      // var link = '{{ url('ControlPanel/clientes/') }}';

                      $.getJSON(url, {'value': value, 'price_from':price_from,'price_at':price_at,'propertyType':propertyType }, function(result) {
                        $('.loading').toggleClass('hidden');

                        if( $('#result').hasClass('hidden')){
                          $('#result').toggleClass('hidden');
                        } 

                        $('.result').remove();

                        if(result.length>0){
                            
                          $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
                          $.each(result,function(key,value){

                            $('tbody').append('<tr class="gradeA '+ value.class + '"><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.code+'</a></td><td class=""><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.inmueble+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.exclusivo+'</a></td><td class="text-right"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">U$ '+value.pv+'</a></td><td class="text-right"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">U$ '+value.pr+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.rotulo+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.checked+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.status+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.estadoTmp+'</a></td></tr>');
                          });
                          action();
                        }else{
                           
                          $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
                          $('tbody').append('<tr class="gradeA" ><td colspan="9" class="text-center" style="height:150px">No se encontraron resultados para la busqueda...</td></tr>');
                        }

                      }); //getJSON

                      $('#search_box').val('');
                      $('#from').val('');
                      $('#at').val('');
                      $('#propertyType').val('');
                      $('#rotulo').val();

                }else{
                    alert('No hay palabras claves en la busqueda...');
                }

            });


            $(function () {
                $('#transaction_history').dataTable({
                  
                  "bPaginate": true,
                  "bLengthChange": true,
                  "bFilter": true,
                  "bSort": true,
                  "bInfo": true,
                  "bAutoWidth": false,
                  "aaSorting": [[0,'desc']],
                  "iDisplayLength": 100,
                  "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
                });
            });
            

        });

        
    </script>
@endsection