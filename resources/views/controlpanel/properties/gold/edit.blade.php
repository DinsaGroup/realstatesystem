@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')
{!! Form::model($goldProperty, [
        'method' => 'PATCH',
        'url' => ['ControlPanel/propiedades-oro', $goldProperty->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}
    {{ csrf_field() }}

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{ $title }} <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger pull-right"><i class="fa fa-times"></i> Cancelar</a>
                <div class="col-sm-2 pull-right">
                        {!! Form::submit('Guardar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>

            </h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
        </section>

        <section class="content">

            <div class="row">

                <div class="col-xs-12">

                    <div class="form-group">
                        {!! Form::label('property_id', 'CÓDIGO : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::text('property_id', $goldProperty->property->code, ['class' => 'form-control', 'required' => 'required', 'id' => 'property_id', 'readonly' => 'readonly']) !!}
                            <small >Note que solo apareceran las propiedades que esten disponibles, publicadas y revisadas por el supervisor.</small>
                        </div>

                    </div>

                    <div class="form-group">
                        {!! Form::label('type', 'TIPO : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::select('type', ['venta' => 'Venta', 'renta' => 'Renta', 'venta/renta' => 'Venta / Renta'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccione uno', 'id' => 'tipo']) !!}
                        </div>
                    </div>

                    @php
                        $date = Carbon\Carbon::now()->toDateString();
                        $date2 = Carbon\Carbon::now()->addYear(1)->toDateString();
                    @endphp

                    <div class="form-group">
                        {!! Form::label('start', 'COMIENZA : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-2">
                            {!! Form::text('start', null, ['class' => 'form-control datepicker', 'required' => 'required', 'placeholder'=>$date]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('end', 'TERMINA : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-2">
                            {!! Form::text('end', null, ['class' => 'form-control datepicker', 'required' => 'required', 'placeholder'=>$date2]) !!}
                        </div>
                    </div>

                    <div id="renta" class="form-group">
                        {!! Form::label('rentPrice', 'PRECIO RENTA (U$) : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-2">
                            {!! Form::text('rentPrice', null, ['class' => 'form-control text-right', 'placeholder' => '35000',]) !!}
                        </div>
                    </div>

                    <div id="venta" class="form-group">
                        {!! Form::label('soldPrice', 'PRECIO VENTA (U$) : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-2">
                            {!! Form::text('soldPrice', null, ['class' => 'form-control text-right', 'placeholder' => '35000']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('status', 'ESTADO : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::select('status', ['activa' => 'Publicar', 'inactiva' => 'Despublicar'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccione uno']) !!}
                        </div>
                    </div>

                </div>

            </div>

        </section>

    </div>

{!! Form::close() !!}

@endsection

@section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function() {
             $('.datepicker').datepicker({
                autoclose:true,
                dateFormat: 'yy-mm-dd'
            });

            // $('#renta').hide();
            // $('#venta').hide();

            $('#tipo').change(function() {
                var tipo = $('#tipo').val();
                console.log(tipo);
                if (tipo == 'venta') {
                    $('#renta').hide();
                    $('#venta').show();                    
                } else if (tipo == 'renta') {
                    $('#renta').show();
                    $('#venta').hide();
                } else {
                    $('#renta').show();
                    $('#venta').show();
                }
            });

            $('#property_id').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-property') }}'
            });
        });
    </script>
@endsection