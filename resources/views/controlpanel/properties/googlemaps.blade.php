@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI43-yj5Hf3fZo8eyIrWULm_VGwbXZecs&libraries=places"  type="text/javascript"></script>

<div class="content-wrapper">
    <section class="content-header">
    
        <h1>{{ $title }} <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger pull-right "><i class="fa fa-times"></i> Cancelar</a></h1>
        <p>{{ $subtitle }}</p>
      
    </section>

    <section class="content">

        <div id="mapCanvas" class="form-group col-sm-12" style="height: 450px"></div>

    </section>

</div>

@endsection

@section('javascript')

<script type="text/javascript">
            
            // Google maps
            var map = new google.maps.Map(document.getElementById('mapCanvas'),{
                center:{
                    lat: {{ $latitude }},
                    lng: {{ $longitude }}
                },
                zoom:16
            });

            var marker = new google.maps.Marker({
                position:{
                    lat: {{ $latitude }},
                    lng: {{ $longitude }}
                },
                map:map,
                draggable:false
            });
</script>
    
@endsection