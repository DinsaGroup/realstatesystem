@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

<div class="content-wrapper">
    <section class="content-header">
            <h1>
            {{ $title }}
             
            </h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>          
        </section>

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-building"></i> Importar información</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-country"><i class="fa fa-upload"></i> Importar país</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-department"><i class="fa fa-upload"></i> Importar departamento</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-municipio"><i class="fa fa-upload"></i> Importar municipio</a>                

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-barrio"><i class="fa fa-upload"></i> Importar barrio</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-clientes"><i class="fa fa-upload"></i> Importar Clientes</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-inmuebles"><i class="fa fa-upload"></i> Importar Inmuebles</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-fotos"><i class="fa fa-upload"></i> Importar Fotos</a>

              <a href="#" class="btn btn-primary btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload-propiedad-usuario"><i class="fa fa-upload"></i> Importar relación propiedad/usuario</a>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
            </div> {{-- col-md-12 --}}
        </div>

    </section>

</div>

<div id="upload-country" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importcountry', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir paises al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-department" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importdepartment', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir departamentos al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-municipio" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importmunicipio', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir municipios al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-barrio" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importbarrio', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir barrios al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-clientes" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importcontact', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir clientes al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-inmuebles" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importproperties', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir inmuebles al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-fotos" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importphotos', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir fotos al sistema</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

<div id="upload-propiedad-usuario" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/import-property-user', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir relación propiedades/usuarios</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

@endsection

{{-- @section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- page script -->
    <script>
        

        $(document).ready(function(){

            $('.details').click(function(){
                var $id = $(this).data('id');
                var $url = '{{ url('ControlPanel/request/property_detail') }}';
                var $edit = '{{ url('ControlPanel/propiedades/') }}' +'/'+ $id + '/edit';
                var $addphotos = '{{ url('ControlPanel/propiedad/') }}' +'/'+ $id + '/fotos/create';


                //Limpiar valores
                $('.valor').empty();
                $('#files').empty();

                $.getJSON( $url, {'id': $id }, function(result) {
                    $('#customer_id').text(result.customer_id);
                    $('#name').text(result.name);
                    $('#house').text(result.house);
                    $('#office').text(result.office);
                    $('#phone').text(result.phone);
                    $('#email').text(result.email);
                    $('#private_note').text(result.private_note);
                    $('#code').text(result.code);
                    $('#property_type').text(result.property_type);
                    $('#venta-renta').text(result.venta_renta);
                    $('#exclusividad').text(result.exclusividad);
                    $('#disponible').text(result.disponible);
                    $('#precioVenta').text(result.precioVenta);
                    $('#precioRenta').text(result.precioRenta);
                    $('#precioxV2').text(result.precioxV2);
                    $('#areaTotal').text(result.areaTotal);
                    $('#areaConstruida').text(result.areaConstruida);
                    $('#antiguedad').text(result.antiguedad);
                    $('#acceso').text(result.acceso);
                    $('#topografia').text(result.topografia);
                    $('#description').text(result.description);
                    $('#address').text(result.address);
                    $('#sector').text(result.sector);
                    $('#city').text(result.city);
                    $('#state').text(result.state);
                    $('#country').text(result.country);
                    $('#latitude').text(result.latitude);
                    $('#longitude').text(result.longitude);
                    $('#agente').text(result.agente);
                    $('#status').text(result.status);
                    $('#notes').text(result.notes);

                    var $google = '{{ url('ControlPanel/googlemaps/') }}' +'/'+ result.latitude + '/' + result.longitude ;

                    $('#edit').attr('href',$edit);
                    $('#add_photos').attr('href',$addphotos);
                    $('#ubicacion').attr('href',$google);

                    if((result.photos).length > 0){
                        $.each(result.photos, function(key,value){
                            if(value.visible == 'true'){
                                var $link = '{{ url('img/properties/') }}' +'/'+ value.filename ;
                                $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><img src="'+ $link + '" width="100%" /></div>');
                                
                            }
                        });
                    }else{
                        var $link = '{{  url('img/properties/no-img.png')}}';
                        $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><img src="'+ $link + '" width="100%" /></div>');
                    }
                });

                
            });

        });

        $(function () {
            $('#transaction_history').dataTable({
              "bSortClasses": 'sorting_3',
              "bPaginate": true,
              "bLengthChange": true,
              "bFilter": true,
              "bSort": false,
              "bInfo": true,
              "bAutoWidth": false,
              // "aaSorting": [[0,'desc']],
              "iDisplayLength": 100,
              "aLengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
            });
        });
    </script>
@endsection --}}