@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

<div class="content-wrapper">
    {{-- <section class="content-header">
        <h1>
        {{ $title }}
         <a href="{{ url('ControlPanel/propiedades/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a>
         <a href="#" class="btn btn-primary pull-right btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload"><i class="fa fa-upload"></i> Import data</a>
        </h1>
        <small>{!! $subtitle !!}</small>          
    </section> --}}

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box" style="margin:0">
                    <div class="box-header">
                        <h3 class="box-title" style="width: 100%"><i class="fa fa-building"></i> Listado de propiedades</h3>
                        <div id="result" class="col-md-12 pull-right">
                            <p class="result">&nbsp;</p>
                        </div>
                    </div>
                    
                     <div class="box-body">

                        <table class="table table-bordered properties">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>CODIGO</th>
                                    <th>ROTULO</th>
                                    <th>CATEGORIA</th>
                                    <th style="min-width: 125px">FECHA INGRESO</th>
                                    <th style="min-width: 75px">ESTATUS</th>
                                    <th>VENTA/RENTA</th>
                                    <th>P. VENTA</th>
                                    <th>P. RENTA</th>
                                    <th>P. x V2</th>
                                    <th style="min-width: 75px">AREA TOTAL</th>
                                    <th style="min-width: 75px">AREA CONST.</th>
                                    <th>SECTOR / UBICACION</th>
                                    <th>CIUDAD</th>
                                    <th>DEPARTAMENTO</th>
                                    <th>CONTACTO</th>
                                    <th>CUARTOS</th>
                                    <th>BAÑOS</th>
                                    <th>AMUEBLADO</th>
                                    <th>PISCINA</th>
                                    <th>LINEAB.</th>
                                    <th style="min-width: 125px">ACTUALIZADA</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="filters">
                                    <td><input type="text" id="id" class="only_integer" style="width: 50px"></td>
                                    <td><input type="text" id="codigo" style="text-transform: uppercase;width: 65px"></td>
                                    <td><input type="text" id="rotulo" style="text-transform: uppercase;width: 65px"></td>
                                    <td><input type="text" id="categoria" style="text-transform: capitalize;"></td>
                                    <td><input type="hidden" id="createdQ"  readonly="readonly"></td>
                                    <td><input type="hidden" id="statusQ"  readonly="readonly"></td>
                                    <td><select id="ventaRenta" ><option value="">Seleccionar</option><option value="renta">En Renta</option><option value="venta">En Venta</option><option value="venta/renta">En Venta/Renta</option></select></td>
                                    <td><select id="pventa"><option value="">Seleccionar</option><option value="0-25000">U$ 0k - U$ 25k</option><option value="25000-50000">U$ 25k - U$ 50k</option><option value="50000-100000">U$ 50k - U$ 100k</option><option value="100000-250000">U$ 100k - U$ 250k</option><option value="250000-500000">U$ 250k - U$ 500k</option><option value="500000-100000000">Más de U$ 500k</option></select></td>
                                    <td><select id="prenta"><option value="">Seleccionar</option><option value="0-250">U$ 0 - U$ 250.00</option><option value="250-500">U$ 250 - U$ 500</option><option value="500-1000">U$ 500 - U$ 1000</option><option value="1000-2500">U$ 1000 - U$ 2500</option><option value="2500-100000">Más de U$ 2,500</option></select></td>
                                    <td><input type="text" id="pvara" class="only_integer"></td>
                                    <td><input type="hidden" id="atotal" class="only_integer" readonly="readonly"></td>
                                    <td><input type="hidden" id="aconstruida" class="only_integer" readonly="readonly"></td>
                                    <td><input type="text" id="sectorQ" style="text-transform: capitalize;width: 150px"></td>
                                    <td><input type="text" id="ciudad" style="text-transform: capitalize;width: 100px"></td>
                                    <td><input type="text" id="departamento" style="text-transform: capitalize;width: 100px"></td>
                                    <td><input type="text" id="contacto" style="text-transform: capitalize;width: 275px"></td>
                                    <td><input type="text" id="cuartos" class="only_integer"></td>
                                    <td><input type="text" id="banos" class="only_integer"></td>
                                    <td><input type="hidden" id="amueblado"></td>
                                    <td><input type="hidden" id="piscina"></td>
                                    <td><input type="hidden" id="lineablanca"></td>
                                    <td><input type="hidden" id="updatedQ"  readonly="readonly"></td>
                                </tr>

                                @foreach($properties as $item)
                                    <tr class="gradeA 
                                        @if( date("Y", strtotime($item->created_at) ) < 2018 && (!$item->published || !$item->checked )) {{ 'despublished'}} @endif 
                                        @if( date("Y", strtotime($item->created_at) ) >= 2018 && (!$item->published || !$item->checked )) {{ 'despublished-green'}} @endif 
                                        @if($item->estadoTmp =='vendida') vendida  @endif 
                                        @if($item->estadoTmp =='alquilado') alquilado  @endif"> 
                                        
                                        <td class="text-center">
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">{{ $item->id }}</a>
                                        </td>

                                        <td class="text-center">
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">{{ $item->code }}</a>
                                        </td>

                                        <td class="text-center">
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">{{ array_get($item->sign,'0.code') }}</a>
                                        </td>

                                        <td>
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">
                                                {{ $item->property_type->name }}
                                            </a>
                                        </td>

                                        <td class="text-center">
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">{{ ucfirst($item->created_at) }}</a>
                                        </td>

                                        <td class="text-center">
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">{{ ucfirst($item->estadoTmp) }}</a>
                                        </td>

                                        <td>
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">
                                                {{ ucfirst( $item->ventaRenta ) }}
                                            </a>
                                        </td>

                                        <td class="text-right">
                                            @if($item->precioVenta>0)
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">U$ {{ number_format($item->precioVenta,2,'.',',') }}</a>
                                            @endif
                                        </td>

                                        <td class="text-right">
                                            @if($item->precioAlquiler>0)
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">U$ {{ number_format($item->precioAlquiler,2,'.',',') }}</a>
                                            @endif
                                        </td>

                                        <td class="text-right">
                                            @if($item->precioPorV2>0)
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">U$ {{ number_format($item->precioPorV2,2,'.',',') }}</a>
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            @if($item->areaTotal>0)
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">
                                                {{ $item->areaTotal }} {{ $item->measure_unit->short_name }}
                                            </a>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if($item->areaConstruida>0)
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">
                                                {{ $item->areaConstruida }} {{ $item->measure_unit->short_name }}
                                            </a>
                                            @endif
                                        </td>

                                        <td>
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">
                                                {{ ucfirst( $item->sector) }}
                                            </a>
                                        </td>

                                        <td>
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">
                                                {{ ucfirst( $item->city) }}
                                            </a>
                                        </td>

                                        <td>
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">
                                                {{ ucfirst( $item->state) }}
                                            </a>
                                        </td>

                                        <td class="">
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">
                                                {{ trim( ucWords( array_get($item->user,'0.user.name') ) ) }}
                                            </a>
                                        </td>

                                        

                                        <td></td>

                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        
                                        
                                        
                                        
                                        <td class="text-center">
                                            <a href="#property" class="details" data-id="{{ $item->id }}" data-toggle="modal">{{ ucfirst($item->updated_at) }}</a>
                                        </td>
                                    </tr>
                                @endforeach

                                <tr class="gradeB">
                                    <td colspan="21" class="text-center hidden loading"><img src="{{ url('img/cargando.gif') }}" /></td>
                                </tr>

                            </tbody>
                        </table>

                        
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
</div>

@include('controlpanel.properties.property_modal_formato_2')

@endsection

@section('javascript')
    <!-- sweetalert -->
    {{-- <script src="{{{ asset('js/sweetalert.min.js') }}}" type="text/javascript"></script> --}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>

        function showphoto(){
            $('.photoProperty img').click(function(){
                $('#fullphoto img').attr('src',$(this).attr('src'));
            });
        }

        function delphoto(){

            $('.delphoto').click(function(e){
                e.preventDefault();
                swal({
                      title: "¿Esta seguro?",
                      text: "¡No podrá recuperar la imagen!",
                      icon: "warning",
                      buttons: true,
                      closeModal: false,
                      dangerMode: true,
                      // showCancelButton: true,
                      // closeOnCancel: false
                      // closeOnConfirm: false,
                    })
                    .then((willDelete) => {
                      if (willDelete) {
                        
                        
                        var object = $(this);
                        var $id = $(this).data('id');
                        var $url =  '{{ url('ControlPanel/request/delphoto') }}';

                        $.getJSON($url,{'id': $id },function(result){
                            
                            if(result ==='ok'){
                                object.parent().remove();
                                
                            };
                        });

                        swal("¡Listo, La imagen ha sido borrada!", {
                          icon: "success",
                          
                        });

                      } else {
                        swal("¡La imagen no ha sido borrada!");
                      }
                    });
                
            })
        }
        
        function action(){
            $('.details').click(function(){
                var $id = $(this).data('id');
                var $url = '{{ url('ControlPanel/request/property_detail') }}';
                var $edit = '{{ url('ControlPanel/propiedades/') }}' +'/'+ $id + '/edit';
                var $addphotos = '{{ url('ControlPanel/propiedad/') }}' +'/'+ $id + '/fotos/create';
                var $disponible = '{{ url('ControlPanel/request/property/disponible') }}' +'/'+ $id ;
                var $noDisponible = '{{ url('ControlPanel/request/property/nodisponible') }}' +'/'+ $id ;
                var $alquilada = '{{ url('ControlPanel/request/property/alquilada') }}' +'/'+ $id ;
                var $vendida = '{{ url('ControlPanel/request/property/vendida') }}' +'/'+ $id ;
                var $checked = '{{ url('ControlPanel/request/property/revisado-publicar') }}' +'/'+ $id ;

                var $delephoto = '{{ url('ControlPanel/deletephoto') }}' ;

                //Limpiar valores
                $('.valor').empty();
                $('#files').empty();
                $('#contacts').empty();

                $.getJSON( $url, {'id': $id }, function(result) {
                    console.log(result.contacts);
                    $.each(result.contacts, function(key,value){
                        $('tbody#contacts').append('<tr><td class="text-center">'+value.customer_id+'</td><td>'+value.name+'</td><td>'+value.email+'</td><td>'+value.house+' '+value.office+' '+ value.phone+'</td><td>'+value.comments+'</td></tr>');
                    });

                    // $('#customer_id').text(result.customer_id);
                    // $('#name').text(result.name);
                    // $('#house').text(result.house);
                    // $('#office').text(result.office);
                    // $('#phone').text(result.phone);
                    // $('#email').text(result.email);
                    // $('#private_note').text(result.private_note);
                    $('#code').text(result.code);
                    $('#sign').text(result.sign);
                    $('#sismobilia').text(result.sismobilia);
                    $('#property_type').text(result.property_type);
                    $('#venta-renta').text(result.venta_renta);
                    $('#exclusividad').text(result.exclusividad);
                    $('#disponible').text(result.disponible);
                    $('#precioVenta').text(result.precioVenta);
                    $('#precioRenta').text(result.precioRenta);
                    $('#precioxV2').text(result.precioxV2);
                    $('#areaTotal').text(result.areaTotal);
                    $('#areaConstruida').text(result.areaConstruida);
                    $('#antiguedad').text(result.antiguedad);
                    $('#acceso').text(result.acceso);
                    $('#topografia').text(result.topografia);
                    $('#description').text(result.description);
                    $('#address').text(result.address);
                    $('#sector').text(result.sector);
                    $('#city').text(result.city);
                    $('#state').text(result.state);
                    $('#country').text(result.country);
                    $('#latitude').text(result.latitude);
                    $('#longitude').text(result.longitude);
                    $('#agente').text(result.agente);
                    $('#status').text(result.status);
                    $('#notasinternas').text(result.notes);
                    $('#created').text(result.created);
                    $('#updated').text(result.updated);
                    $('#comision').text(result.comision);
                    $('#estadoTmp').text(result.estadoTmp);

                    var $google = '{{ url('ControlPanel/googlemaps/') }}' +'/'+ result.latitude + '/' + result.longitude ;

                    $('#edit').attr('href',$edit);
                    $('#add_photos').attr('href',$addphotos);
                    $('#ubicacion').attr('href',$google);
                    $('#disp').attr('href',$disponible);
                    $('#noDisponible').attr('href',$noDisponible);
                    $('#alquilada').attr('href',$alquilada);
                    $('#vendida').attr('href',$vendida);
                    $('#checked').attr('href',$checked);

                    if((result.photos).length > 0){
                        $first = true;
                        $.each(result.photos, function(key,value){
                            if(value.visible == 'true'){
                                var $link = '{{ url('img/properties/') }}' +'/'+ value.filename ;
                                if($first){
                                    $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><a href="" data-id="'+value.id+'" class="pull-right delphoto"><i class="fa fa-times"></i></a><img src="'+ $link + '" width="100%" /></div>');
                                     $('#fullphoto img').attr('src',$link);
                                     $first = false;
                                }else{
                                    $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><a href="" data-id="'+value.id+'" class="pull-right delphoto"><i class="fa fa-times"></i></a><img src="'+ $link + '" width="100%" /></div>');
                                    
                                }
                            }
                        });

                        delphoto();
                        showphoto();

                    }else{
                        var $link = '{{  url('img/properties/no-img.png')}}';
                        $('#fullphoto img').attr('src',$link);
                        $('#files').append('<div class="box_detail col-sm-2 photoProperty" ><img src="'+ $link + '" width="100%" /></div>');
                    }

                    // Presentar caracteristicas
                    if((result.features).length > 0){
                        $.each(result.features, function(key,value){
                            if(value.valor !=null){
                                $('#features').append('('+value.valor+')' + value.feature + ', ');
                            }else{
                                $('#features').append(value.feature + ', '); 
                            }
                        });
                    }

                });
                
            });
        }


        $(document).ready(function(){

            action();

            $('input').keypress(function(event){
                
                if ( event.which == 13 ) {

                    $('.gradeA').remove();
                    $('.loading').toggleClass('hidden'); 

                    var id = $('#id').val();
                    var codigo = $('#codigo').val();
                    var rotulo = $('#rotulo').val(); 
                    var categoria = $('#categoria').val();
                    var sector = $('#sectorQ').val(); 
                    var ciudad = $('#ciudad').val();
                    var departamento = $('#departamento').val();
                    var contacto = $('#contacto').val();
                    var ventaRenta = $('#ventaRenta').val();
                    var pventa = $('#pventa').val();
                    var prenta = $('#prenta').val();
                    var pvara = $('#pvara').val();
                    var cuartos = $('#cuartos').val();
                    var banos = $('#banos').val();
                    var atotal = $('#atotal').val();
                    var aconstruida = $('#aconstruida').val();
                    var status = $('#statusQ').val();
                    var created = $('#createdQ').val();
                    var updated = $('#updatedQ').val();

                    var url = '{{ url('ControlPanel/request/properties') }}';

                    $.getJSON(url, {'id': id, 'codigo': codigo, 'rotulo': rotulo, 'categoria': categoria, 'sector': sector, 'ciudad': ciudad, 'departamento': departamento, 'contacto': contacto, 'ventaRenta': ventaRenta, 'pventa': pventa, 'prenta': prenta, 'pvara': pvara,'cuartos': cuartos,'banos':banos,'atotal': atotal,'aconstruida': aconstruida,'status': status,'created': created,'updated': updated }, function(result) {

                        // console.log(result);

                        $('.loading').toggleClass('hidden');

                        if( $('#result').hasClass('hidden')){
                          $('#result').toggleClass('hidden');
                        } 

                        $('.result').remove();

                        if(result.length>0){
                          $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
                          $.each(result,function(key,value){

                            $('tbody').append('<tr class="gradeA '+ value.class + '"><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.id+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.code+'</a></td><td class=""><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.rotulo+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.categoria+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.created+'</a></td><td class="text-right"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.estadoTmp+'</a></td><td class="text-right"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.ventaRenta+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.pventa+'</a></td><td class="text-center"><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.prenta+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.pvara+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.atotal+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.aconstruida+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.sector+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.ciudad+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.departamento+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.contacto+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.cuartos+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.banos+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.amueblado+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.piscina+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.lineab+'</a></td><td><a href="#property" class="details" data-id="'+value.id+'" data-toggle="modal">'+value.updated+'</a></td></tr>');
                          });

                          action();
                        }else{
                           
                          $('#result').append('<p class="result">Se encontraron '+ result.length +' coincidencias.</p>');
                          $('tbody').append('<tr class="gradeA" ><td colspan="9" class="text-center" style="height:150px">No se encontraron resultados para la busqueda...</td></tr>');
                        }

                      }); //getJSON
                }

            });


            // ---------- Autocompletes -------------------

            $('#id').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-property-id') }}'
            });

            $('#codigo').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-codigo') }}'
            });

            $('#rotulo').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-rotulo') }}'
            });

            $('#categoria').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-categoria') }}'
            });

            $('#sectorQ').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-sector') }}'
            });

            $('#ciudad').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-ciudad') }}'
            });

            $('#departamento').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-departamento') }}'
            });

            $('#contacto').autocomplete({
                minLenght:3,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-contacto') }}'
            });

        });
    </script>

@endsection