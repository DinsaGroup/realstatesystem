<div id="property" class="modal fade" role="dialog">     
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">DETALLE DE LA PROPIEDAD</h4>
            </div>
            <div class="modal-body"> 

                <div class="row" style="margin: 0 0 25px 0;">
                    <h4>PROPIEDAD</h4>
                    <div class="col-xs-4">
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Codigo</span>
                            <span id="code" class="valor"> CVR-452</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Rotulo</span>
                            <span id="sign" class="valor"> VR-32</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Sismobilia ID</span>
                            <span id="sismobilia" class="valor"> CVR-452</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Tipo de propiedad</span>
                            <span id="property_type" class="valor"> Casa de habitación</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Venta / Renta</span>
                            <span id="venta-renta" class="valor"> 12456235</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Exclusiva</span>
                            <span id="exclusividad" class="valor"> Si</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Estatus</span>
                            <span id="estadoTmp" class="valor"> No</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Precio venta</span>
                            <span id="precioVenta" class="valor"> U$ 2,152.25</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Precio renta</span>
                            <span id="precioRenta" class="valor"> U$ 152.25</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Precio x vara</span>
                            <span id="precioxV2" class="valor"> U$ 12.50</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Area Total</span>
                            <span id="areaTotal" class="valor"> 1254 mts</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Area Construida</span>
                            <span id="areaConstruida" class="valor"> 125 mts</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Construida en</span>
                            <span id="antiguedad" class="valor"> 1925</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Fácil acceso</span>
                            <span id="acceso" class="valor"> Si</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Topografia</span>
                            <span id="topografia" class="valor"> con problemas de acceso al inicio</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Estado de la propiedad</span>
                            <span id="status" class="valor" >Buen estado</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Agente</span>
                            <span id="agente" class="valor" >Cualquier nombre</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Comisión x venta</span>
                            <span id="comision" class="valor"> 20%</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Ingresada</span>
                            <span id="created" class="valor"> con problemas de acceso al inicio</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Actualizada</span>
                            <span id="updated" class="valor"> con problemas de acceso al inicio</span>
                        </div>
                        
                    </div>

                    <div class="col-xs-4">
                        <div class="col-sm-12 box_detail2">
                            <span class="note">País</span>
                            <span id="country" class="valor" >Nicaragua</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Ciudad</span>
                            <span id="state" class="valor" >Managua</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Municipio</span>
                            <span id="city" class="valor" >Ciudad Sandino</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Sector</span>
                            <span id="sector" class="valor" >Barrio no tengo idea</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note">Ubicación</span>
                            <a href="#" id="ubicacion" target="_blank"><i class="fa fa-map-marker"></i> Ver ubicación en google maps</a>
                        </div>
                        <div class="col-sm-12 box_detail2" style="width: 100%">
                            <span class="note">Dirección</span>
                            <span id="address" class="valor" >De donde orino el perro 2c al sur, 3c este, mano derecha casa #q123</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note" style="width: 100%">Características de la propiedad</span>
                            <span id="features" class="valor"> Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</span>
                        </div>
                        
                          
                    </div>

                    <div class="col-xs-4">
                        <div class="col-sm-12 box_detail2">
                            <span class="note" style="width: 100%">Descripción visible al usuario</span>
                            <span id="description" class="valor"> Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</span>
                        </div>
                        <div class="col-sm-12 box_detail2">
                            <span class="note" style="width: 100%">Notas internas de la empresa</span>
                            <span id="notasinternas" class="valor"> Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</span>
                        </div>
                        
                    </div>

                </div>

                <div class="row" style="margin: 0 0 25px 0;">
                    <h4>CONTACTOS DE LA PROPIEDAD</h4>
                    <div class="col-xs-12">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="75">Codigo</th>
                                    <th width="15%">Nombre Completo</th>
                                    {{-- <th width="">Relación</th> --}}
                                    <th width="10%">Email</th>
                                    <th width="15%">Teléfonos</th>
                                    <th width="">Notas</th>
                                </tr>
                            </thead>

                            <tbody id="contacts">
                                <tr></tr>
                            </tbody>
                        </table>
                        
                    </div>    
                </div>
            
                <div class="row" style="margin: 0 0 25px 0;">
                    <h4>FOTOS</h4>
                    <div class="col-md-6 col-sm-12" id="fullphoto">
                        <img src="{{  url('img/properties/no-img.png')}}" width="100%" />
                    </div>
                    <div class="col-md-6 col-sm-12" id="files">
                        <div class="col-sm-2 box_detail2 photoProperty">
                            <img src="{{  url('img/properties/no-img.png')}}" width="100%" />
                        </div>
                    </div>
                </div>

                
 
            </div>
            <div class="modal-footer">
                
                    <a id="add_photos" href="#" class="btn btn-primary col-md-2" ><i class="fa fa-picture-o"></i> Subir fotos</a>
                
                    <a id="edit" href="#" class="btn btn-primary col-md-1" ><i class="fa fa-pencil"></i> Editar</a>

                    @if(!Auth::user()->hasRole('agent'))

                    <a id="checked" href="#" class="btn btn-success " ><i class="fa fa-check"></i> Revisado / Publicar</a>

                    <a id="disp" href="#" class="btn btn-success " ><i class="fa fa-check"></i> Disponible</a>

                    <a id="noDisponible" href="#" class="btn btn-danger " ><i class="fa fa-ban"></i> No Disponible</a>

                    <a id="alquilada" href="#" class="btn btn-warning " ><i class="fa fa-handshake-o"></i> Alquilada</a>

                    <a id="vendida" href="#" class="btn btn-danger " ><i class="fa fa-sign-out"></i> Vendida</a>

                    @endif
                
            </div>
        </div>
    </div>   
</div>
