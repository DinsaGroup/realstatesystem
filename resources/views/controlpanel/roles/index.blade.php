@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

<style type="text/css">
    .details{
        color:#000000;
        font-size: 0.9em;
    }
</style>

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
        
            <h1>{{ $title }}</h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-users"></i> Listado de Administradores</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="transaction_history" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                            <th>ID</th>
                            <th>Título</th>
                            <th>Descripción</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($roles as $item)
                        <tr class="gradeA">
                            <td class="text-center">{{ $item->id }}</td>
                            <td>{{ $item->display_name }}</td>
                            <td>{{ $item->description }}</td>
                        </tr>
                      @endforeach
                    </tbody>
                    {{-- <tfoot>
                      <tr>
                          <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>País</th>
                            <th>Casa</th>
                            <th>Oficina</th>
                            <th>Celular</th>
                            <th>Tipo</th>
                            <th></th>
                        </tr>
                    </tfoot> --}}
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                </div> {{-- col-md-12 --}}
            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#transaction_history").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>
@endsection