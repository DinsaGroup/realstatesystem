<!-- Menu -->
@if (Auth::user()->hasRole('agent'))
    @include('controlpanel.menu.agent')
@elseif (Auth::user()->hasRole('supervisor'))
    @include('controlpanel.menu.supervisor')
@elseif(Auth::user()->hasRole('admin'))
    @include('controlpanel.menu.admin')
@else
    @include('controlpanel.menu.superadmin')
@endif

{{-- <li class="nav-header">
    <div class="dropdown profile-element"> 
        <span class="block m-t-xs">
            
        </span>

        <span class="text-muted text-xs block">
            <div class="info_system">
                <strong class="font-bold">Momotombo Real Estate</strong><br>
                Logued as: {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}<br>
                <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <br>Versión: <b>1.0<b></p>
            </div>

        </span> 


    </div>

</li> --}} <!-- /.nav-header -->