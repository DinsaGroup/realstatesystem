@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')
{!! Form::open(['url' => 'ControlPanel/rotulos', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
{{ csrf_field() }}
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{ $title }} 
                <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger pull-right"><i class="fa fa-times"></i> Cancelar</a>
                <div class="col-sm-2 pull-right">
                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary form-control ']) !!}
                </div>
            </h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            
            <div class="form-group">

                <div class="form-group">
                    {!! Form::label('code', 'CÓDIGO DEL ROTULO : ', ['class' => ' control-label  col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::text('code', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: R-123', 'id' => 'user_id']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('code_id', 'CÓDIGO DEL INMUEBLE : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::text('code_id', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: AV-123', 'id' => 'code_id']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('status', 'ESTADO : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::select('status', ['disponible' => 'Disponible', 'deteriorado' => 'Deteriorado', 'instalado' => 'Instalado', 'robadio' => 'Robado'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Seleccione uno']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('notes', 'COMENTARIOS : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-8">
                        {!! Form::textArea('notes', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                

            

            </div>

        </section>

    </div>
{!! Form::close() !!}
@endsection

@section('javascript')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function() {
            $('#code_id').autocomplete({
                minLenght:4,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-property') }}'
            });
        });
    </script>
@endsection