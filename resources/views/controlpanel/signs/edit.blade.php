@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
        </section>

        <section class="content">

            {!! Form::model($sign, [
                    'method' => 'PATCH',
                    'url' => ['ControlPanel/rotulos', $sign->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                {{ csrf_field() }}
            <div class="">
            
                <div class="form-group">
                    {!! Form::label('code', 'CÓDIGO DEL ROTULO : ', ['class' => ' control-label  col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::text('code', $sign->code, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: R-123', 'id' => 'user_id', 'readonly']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('code_id', 'CÓDIGO DEL INMUEBLE : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::text('code_id', $sign->property->code, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: AV-123', 'id' => 'code_id']) !!}
                    </div>
                </div>

                <div class="form-group">
                        {!! Form::label('status', 'ESTADO : ', ['class' => ' control-label col-md-3']) !!}
                        <div class="col-md-3">
                            {!! Form::select('status', ['disponible' => 'Disponible', 'deteriorado' => 'Deteriorado', 'instalado' => 'Instalado', 'robadio' => 'Robado'], null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Seleccione uno']) !!}
                        </div>
                    </div>

                <div class="form-group">
                    {!! Form::label('notes', 'COMENTARIOS : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::textArea('notes', $sign->notes, ['class' => 'form-control']) !!}
                    </div>
                </div>

                
                    
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

            </div>

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection
@section('javascript')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function() {
            $('#code_id').autocomplete({
                minLenght:4,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-property') }}'
            });
        });
    </script>
@endsection