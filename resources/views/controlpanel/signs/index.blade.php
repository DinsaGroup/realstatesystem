@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/rotulos/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

                <div class="ibox float-e-margins">
                    <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

                        <div class="table-responsive"> {{-- update --}}
                            <table id="transaction_history" class="table table-bordered table-striped"> {{-- update --}}
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">CÓDIGO</th>
                                        <th class="text-center">PROPIEDAD</th>
                                        <th class="text-center">ESTADO</th>
                                        <th class="text-center">NOTAS</th>
                                        <th class="text-center"><i class="fa fa-sort-desc"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- */$x=0;/* --}}
                                    @foreach($signs as $item)
                                        {{-- */$x++;/* --}}
                                        <tr class="gradeA"> {{-- update --}}
                                            <td class="text-center">{{ $item->id }}</td>
                                            <td class="text-center">{{ $item->code }}</td>
                                            <td class="text-center">{{ $item->property->code }}</td>
                                            <td class="text-center">{{ $item->status }}</td>
                                            <td class="text-center">{{ $item->notes }}</td>
                                            
                                            <td  class="text-center">
                                                <a href="{{ url('ControlPanel/rotulos/' . $item->id . '/edit') }}" class="details">
                                                    <i class="fa fa-pencil"></i>
                                                </a> 
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination"></div>
                        </div>

                    </div> <!-- Termina el contenido de la seccion --> 
                </div> <!-- Termina el ibox --> 

        </section>

    </div>


@endsection

@section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function(){
            $(function () {
                $('#transaction_history').dataTable({
                  "bSortClasses": 'sorting_3',
                  "bPaginate": true,
                  "bLengthChange": true,
                  "bFilter": true,
                  "bSort": true,
                  "bInfo": true,
                  "bAutoWidth": false,
                  "iDisplayLength": 250,
                  "aLengthMenu": [[250, 500, 1000, 2000, -1], [250, 500, 1000, 2000, "All"]]
                });
            });
            })
    </script>
@endsection