@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
          
        </section>

        <section class="content">

            {!! Form::open(['url' => 'ControlPanel/testimonios', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
            {{ csrf_field() }}
            <div class="row">
                <div class="col-xs-12">

                    <div class="form-group col-sm-12">
                        {!! Form::label('user_id', 'Buscar usuario : ', ['class' => ' control-label']) !!}
                        <div class="">
                            {!! Form::text('user_id', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Ej: Marcos Alonso', 'id' => 'user_id']) !!}
                        </div>
                    </div>

                    <div class="form-group col-sm-12">
                        {!! Form::label('notes', 'TESTIMONIO : ', ['class' => ' control-label']) !!}
                        <div class="">
                            {!! Form::textArea('notes', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2">
                            {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection
@section('javascript')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function() {
            $('#user_id').autocomplete({
                minLenght:4,
                autoFocus: true,
                source: '{{ url('ControlPanel/request/get-client') }}'
            });
        });
    </script>
@endsection