@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/testimonios/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

                <div class="ibox float-e-margins">
                    <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

                        <div class="table-responsive"> {{-- update --}}
                            <table class="table table-striped table-bordered table-hover dataTables"> {{-- update --}}
                                <thead>
                                    <tr>
                                        <th class="text-center" width="50">ID</th>
                                        <th class="text-center" width="20%">USUARIO</th>
                                        <th class="text-center" width="">TESTIMONIO</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- */$x=0;/* --}}
                                    @foreach($testimonials as $item)
                                        {{-- */$x++;/* --}}
                                        <tr class="gradeA"> {{-- update --}}
                                            <td class="text-center"><a href="{{ url('ControlPanel/testimonios/' . $item->id . '/edit') }}" class="details">{{ $item->id }}</a></td>
                                            <td class="text-center"><a href="{{ url('ControlPanel/clientes/' . $item->user_id ) }}" class="details"><img src="{{ url('img/user/'. $item->user->photo) }}" class="user-image" /> {{ $item->user->name }}</a></td>
                                            <td >
                                                <div style="overflow: hidden; height: 15px; width: auto; float: left;">
                                                    <a href="{{ url('ControlPanel/testimonios/' . $item->id . '/edit') }}" class="details">{{ $item->notes }} </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination"></div>
                        </div>

                    </div> <!-- Termina el contenido de la seccion --> 
                </div> <!-- Termina el ibox --> 
            

        </section>

    </div>


      <!-- Header page / Titulo de la seccion -->


<div class="wrapper wrapper-content animated fadeInRight">
    
</div> <!-- Termina el wrapper --> 

@endsection