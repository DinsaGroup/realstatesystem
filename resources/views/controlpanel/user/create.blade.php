@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $title }} <a href="{{ url('ControlPanel/'.$path) }}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-times"></i> Cancelar</a></h1>
            <small>Ingrese los datos en el siguiente formulario, note que hay datos que son necesarios para crear el registro.</small>
        </section>

        <section class="content">

            {!! Form::open(['url' => 'ControlPanel/'.$path, 'class' => 'form-horizontal', 'method'=>'POST', 'files'=>true]) !!}
            {{ csrf_field() }}
            <div class="">
            
                <div class="form-group">
                    {!! Form::label('name', 'Nombre : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Pedro González']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Correo electrónico : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'micorreo@midominio.com']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('password', 'Contraseña : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => '**********']) !!}
                    </div>
                </div>

                {{-- <div class="form-group">
                    {!! Form::label('confirm_pass', 'Confirmar : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-3">
                        {!! Form::password('confirm_pass', ['class' => 'form-control', 'required' => 'required', 'placeholder' => '**********']) !!}
                    </div>
                </div> --}}

                <div class="form-group">
                    {!! Form::label('avatar', 'Seleccionar avatar : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-6">
                        <img src="/img/user/default.jpg" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                        <input type='file' name="logo" class="from-control" style="padding-top: 10px"> 
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('house', 'Teléfono casa : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {!! Form::text('house', null, ['class' => 'form-control', 'placeholder'=>'8888 8888']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('office', 'Teléfono oficina : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {!! Form::text('office', null, ['class' => 'form-control', 'placeholder'=>'8888 8888']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('phone', 'Celular : ', ['class' => ' control-label col-md-3']) !!}
                    <div class="col-md-2">
                        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder'=>'8888 8888']) !!}
                    </div>
                </div>
                    
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                    </div>
                </div>

                {!! Form::close() !!}

            </div>

        </section>

    </div>

@endsection