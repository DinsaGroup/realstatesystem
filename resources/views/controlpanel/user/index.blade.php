@extends('layouts.controlpanel')
@section('title') {{ $title }} @endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
        
            <h1>{{ $title }} <a href="{{ url('ControlPanel/'.$path.'/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a>
            {{-- <a href="#" class="btn btn-primary pull-right btn-sm" style="margin-right: 10px;" data-toggle="modal" data-target="#upload"><i class="fa fa-upload"></i> Import data</a> --}}
            </h1>
            <small>Listado de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
          
        </section>

        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-users"></i> Listado de Administradores</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="transaction_history" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Casa</th>
                            <th>Oficina</th>
                            <th>Celular</th>
                            <th>Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($administrators as $item)
                        @if(Auth::user()->hasRole('supervisor'))
                          <tr class="gradeA"> 
                            <td class="text-center"><a href="{{ url('ControlPanel/'.$path.'/'. $item->id . '/edit') }}" class="details">{{ $item->id }}</td>
                            <td><img src="{{ url('img/user/'. $item->photo) }}" class="user-image"/> {{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->house }}</td>
                            <td>{{ $item->office }}</td>
                            <td>{{ $item->phone }}</td>
                            <td>{{ array_get($item->role,'0.display_name') }}</td>
                        </tr>
                        @else
                          <tr class="gradeA"> 
                            <td class="text-center"><a href="{{ url('ControlPanel/'.$path.'/'. $item->id . '/edit') }}" class="details">{{ $item->id }}</td>
                            <td><img src="{{ url('img/user/'. $item->photo) }}" class="user-image"/> <a href="{{ url('ControlPanel/'.$path.'/'. $item->id . '/edit') }}" class="details"> {{ $item->name }}</td>
                            <td><a href="{{ url('ControlPanel/'.$path.'/'. $item->id . '/edit') }}" class="details">{{ $item->email }}</td>
                            <td><a href="{{ url('ControlPanel/'.$path.'/'. $item->id . '/edit') }}" class="details">{{ $item->house }}</td>
                            <td><a href="{{ url('ControlPanel/'.$path.'/'. $item->id . '/edit') }}" class="details">{{ $item->office }}</td>
                            <td><a href="{{ url('ControlPanel/'.$path.'/'. $item->id . '/edit') }}" class="details">{{ $item->phone }}</td>
                            <td><a href="{{ url('ControlPanel/'.$path.'/'. $item->id . '/edit') }}" class="details">{{ array_get($item->role,'0.display_name') }}</td>
                        </tr>
                        @endif
                      @endforeach
                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                </div> {{-- col-md-12 --}}
            </div>

        </section>

    </div>

<div id="upload" class="modal fade" role="dialog">
    {!! Form::open(['url' => 'ControlPanel/importusers', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','method'=>'POST', 'files' => true ]) !!}
        
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir archivos</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row" style="margin-bottom: 25px;">
                        <div class="form-group">
                            {!! Form::label('avatar', 'Import CSV file: ', ['class' => ' control-label col-md-3']) !!}
                            <div class="col-md-8">
                                <img src="{{ url('/img/csv.png') }}" class="img-circle" width="45" style="float: left; margin-right: 20px" />
                                <input type='file' name="imported-file" class="from-control" style="padding-top: 10px"> 
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary create ']) !!}
                    
                    </div>
                </div>
            </div>
        </div>   

    {!! Form::close() !!}
</div>

@endsection

@section('javascript')
<!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#transaction_history").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>
@endsection