<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI43-yj5Hf3fZo8eyIrWULm_VGwbXZecs&libraries=places"  type="text/javascript"></script>

<script>
	function getCoords(){
		if (navigator && navigator.geolocation) {
			var tiempo_de_espera = 10000;
			 navigator.geolocation.getCurrentPosition(mostrarCoordenadas, mostrarError, { enableHighAccuracy: true, timeout: tiempo_de_espera, maximumAge: 0 } );
		} else {
			alert("La Geolocalización no es soportada por este navegador"); 
		}
	} 

	function mostrarCoordenadas(position) {
		var url = window.location.href;
		var array = url.split("/");
		
		document.getElementById('lat').value = position.coords.latitude;
		document.getElementById('lng').value = position.coords.longitude;
		document.getElementById('search').submit();
	} 

	function mostrarError(error) { 
		var errores = {1: 'Tu GPS esta deshabilitado, activalo y volve a intentar! :)', 2: 'Posición no disponible', 3: 'Expiró el tiempo de respuesta'}; 
		var os = detectarOS();

		if(error.code == 1){


			 if(os.includes('Android')){
				$(document).ready(function(){
					$("#myModal").modal();
				});
			}

			else if(os.includes('Mac OS X')){
				$(document).ready(function(){
					$("#myModal2").modal();
				});
			}

			else{
				alert("Upps: " + errores[error.code]);
			}

		}

		else{
				alert("Upps: " + errores[error.code]);
			}

	}

	function detectarOS(){
		var x = navigator.appVersion;
		return x;

	}

	function validar(){

		var lat = document.getElementById('lat');

		if(document.getElementById('lat').value.length < 1){
			alert('¡Oopps Sorry! Google maps no encontró tu zona :( \n\n Volve a escribir la zona y selecciona entre las opciones que se te presentan.');
			return false;
		}
		else{
			return true;
		}

	}

</script>

<div class="container search-container">
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2">
			<div class="searchBox">
				{{-- <h2>{{ trans('messages.header_searchbox') }}</h2> --}}
				<div class="innerBox">
					<div class="row">
						<div class="col-lg-12">

							{{ Form::open(array('url'=>'buscar-propiedades','class' => 'inline', 'method' => 'POST', 'onsubmit' => 'return validar()', 'id'=>'search')) }}
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon"><i class="fa fa-map-marker ce-fa-map text-orange"></i></div>
										{{ Form::text('auto',$value = null, array('placeholder' => 'Buscar propiedades cerca de ...', 'id' => 'auto', 'class' => 'form-control item search', 'required' => 'required')) }}
										{!! Form::hidden('latitude', null, ['class' => 'form-control', 'id' =>'lat' , 'required' => 'required' ]) !!}
										{!! Form::hidden('longitude', null, ['class' => 'form-control', 'id' =>'lng', 'required' => 'required' ]) !!}
									</div>
								</div>
								<div class="item button-search">
									<button type="submit" class="btn btn-default lupa" style="padding: 12px 30px;"> Buscar</button>
								</div>
								
							{{ Form::close() }}

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="{{{ asset('/js/locationsearch.js') }}}"></script>
