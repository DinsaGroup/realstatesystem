<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI43-yj5Hf3fZo8eyIrWULm_VGwbXZecs&libraries=places"  type="text/javascript"></script>

<div class="container search-container">

	<div class="row">
		<div class="col-lg-12">
			<div class="searchBox">
				<div class="innerBox">
					<div class="row">
						<div class="col-lg-12">


							{{ Form::open(array('url'=>'buscar-propiedades','class' => 'inline', 'method' => 'POST', 'id'=>'search')) }}
								<input type="hidden" name="buscar" value='true'>

								<div class="input" style="width: 25%">
									@if(Session::has('property_id'))
	                                    {!! Form::select('property_type_id', $property_types, Session::get('property_id'), ['class' => 'form-control item search ', 'placeholder' => 'Tipo propiedad']) !!}
	                                @else
	                                	{!! Form::select('property_type_id', $property_types, Null, ['class' => 'form-control item search ', 'placeholder' => 'Tipo propiedad']) !!}
	                                @endif
                                </div>
                                <div class="input" style="width: 25%">
                                	@if(Session::has('ventaRenta'))
	                                    {!! Form::select('venta_renta', ['venta'=>'Solo en Venta','renta'=>'Solo en Renta','venta/renta'=>'Venta & Renta'], Session::get('ventaRenta'), ['class' => 'form-control item search ','placeholder'=>'Estatus']) !!}
	                                @else
	                                	{!! Form::select('venta_renta', ['venta'=>'Solo en Venta','renta'=>'Solo en Renta','venta/renta'=>'Venta & Renta'], null, ['class' => 'form-control item search ','placeholder'=>'Estatus']) !!}
	                                @endif
                                </div>
                                <div class="input-box" style="width:50%">
                                	@if(Session::has('value'))
										{{ Form::text('auto',Session::get('value'), array('placeholder' => 'Escribe el Sector, Residencial, Ciudad ó Municipio', 'id' => 'auto', 'class' => 'form-control item search')) }}
									@else
										{{ Form::text('auto',$value = null, array('placeholder' => 'Escribe el Sector, Residencial, Ciudad ó Municipio', 'id' => 'auto', 'class' => 'form-control item search')) }}
										{{-- {{ Form::text('sector',null, array('placeholder' => 'Escribe el Sector, Residencial, Ciudad ó Municipio', 'id' => 'sector', 'class' => 'form-control item search')) }} --}}
									@endif
								</div>
                                <div class="input" style="width: 25%">
                                	@if(Session::has('desde'))
	                                    {!! Form::text('desde', Session::get('desde'), ['class' => 'form-control item search only_numbers text-right', 'placeholder' => 'Desde U$']) !!}
	                                @else
	                                	{!! Form::text('desde', null, ['class' => 'form-control item search only_numbers text-right', 'placeholder' => 'Desde U$']) !!}
	                                @endif
                                </div>
                                <div class="input" style="width: 25%">
                                	@if(Session::has('hasta'))
	                                    {!! Form::text('hasta', Session::get('hasta'), ['class' => 'form-control item search only_numbers text-right', 'placeholder' => 'Hasta U$']) !!}
	                                @else
	                                	{!! Form::text('hasta', null, ['class' => 'form-control item search only_numbers text-right', 'placeholder' => 'Hasta U$']) !!}
	                                @endif
                                </div>
                                <div class="input" style="width: 25%">
                                	@if(Session::has('code'))
	                                    {!! Form::text('code', Session::get('code'), ['class' => 'form-control item search', 'placeholder' => 'CODIGO']) !!}
	                                @else
	                                	{!! Form::text('code', null, ['class' => 'form-control item search', 'placeholder' => 'CODIGO']) !!}
	                                @endif
                                </div>
								<div class="item button-search" style="width: 25%; float: left; margin-top: 5px;">
									<button type="submit" class="cta-button" style="padding: 12px 30px;"> Buscar</button>
								</div>

							{{ Form::close() }}

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="{{{ asset('/js/locationsearch.js') }}}"></script>
