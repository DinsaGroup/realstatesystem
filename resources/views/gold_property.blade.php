@extends('layouts.frontend')

@section('head')
@endsection

@section('content')


    <section id="banner" style="height: 300px; background-image:url({{ url('img/slider/b21.jpg') }});
                			background-size: cover;
        					background-position: 50% 50%;
        					width: 100%;
        					margin-top: 50px">
        <div class="banner result">
            @include('frontend.search')
        </div>
    </section>

    <section>
    	<div class="container" style="width: 96%">
    		
    		<div class="row">
    			<div class="col-sm-12 noPadding">
                    <div class="col-sm-12">
    				    <h3>{{ $title }}</h3>
                        <p>¡Felicidades! ¡Hemos encontrado ciertos inmuebles que pueden ser de tu interes!</p>
                    </div>

                    @foreach($featured as $item)
                    <a href="{{ url('propiedad/'.$item->property->code ) }}" class="show_now">
                    <div class="col-sm-2 box-property {{ $item->property->property_type->name }} {{ $item->property->ventaRenta }}" data-type="{{ $item->property->ventaRenta }}">
                        <div class="featured" style="height: 275px;">

                            <div class="image">
                                <img src="{{ url('img/properties/'. array_get($item->property->photos,'0.filename') ) }}" class="photo" />
                            </div>

                            <span class="title" style="margin-bottom: 0">{{ ucfirst($item->property->property_type->name) }} en {{ ucfirst($item->property->ventaRenta) }} <br>
                                <?php
                                    $sector = explode('/',$item->property->sector);
                                    $cadena = explode(' ',$sector[0]);
                                    $string = '';

                                    for($i=0;$i<count($cadena);$i++){

                                        if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                            $string .= $cadena[$i].' ';
                                        }else{
                                            $string .= ucfirst($cadena[$i]).' ';
                                        }

                                    }
                                    echo trim($string).', ';
                                ?>
                                {{-- {{ ucwords($item->property->sector) }} --}}
                                {{ ucwords($item->property->city) }}</span>
                            <span class="price" style="padding: 0 10px;">
                                @if($item->property->ventaRenta == 'venta')
                                    <strong style="font-size: 18px">PV. U$ {{ number_format($item->soldPrice, 2,'.',',') }}</strong><br>
                                    PV. U$ <span class="underline">{{ number_format($item->property->precioVenta,2,'.',',') }}</span>
                                @endif
                                @if($item->property->ventaRenta == 'renta')
                                    <strong style="font-size: 18px">PR. U$ {{ number_format($item->rentPrice, 2,'.',',') }}</strong><br>
                                    PR. U$ <span class="underline">{{ number_format($item->property->precioAlquiler,2,'.',',') }}</span>
                                @endif
                                @if($item->property->ventaRenta == 'venta/renta')
                                    <span class="pull-left"> PV. U$ {{ number_format($item->property->precioVenta,2,'.',',') }}</span>
                                    <span class="pull-right"> PR. U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }}</span>
                                @endif

                            </span>
                            
                        </div>
                    </div>
                    </a>
                    @endforeach


    			</div>
    			

    		</div>	
    	</div>
    </section>


@endsection

@section('script')

	
@endsection
