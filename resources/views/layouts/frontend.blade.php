<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title }}</title>
    <meta name="description" content="{{ $metadesc }}" />

    <!-- Open Graph + Twitter Cards Metadata -->
    <meta property="og:locale" content="es_NI" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $title }}" />
    <meta property="og:description" content="{{ $metadesc }}" />
    <meta property="og:site_name" content="Momotombo Real Estate" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="{{ $metadesc }}" />
    <meta name="twitter:title" content="{{ $title }}" />
    <!-- Open Graph + Twitter Cards Metadata -->

    <!-- Bootstrap -->
    {{-- <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet"> --}}
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.5/flatly/bootstrap.min.css" rel="stylesheet"> --}}

    <!-- Icons -->
    {{-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel='stylesheet' type='text/css'> --}}
    {{-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> --}}

    <!-- Styles -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/template.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">

    @yield('head')

    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NXPZX3J');</script>

    <!-- End Google Tag Manager -->



</head>
<body>

    <!-- ********************** Google Tag Manager ***************************** (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NXPZX3J"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <header>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="homepage" href="{{ url('/') }}">
                        <img src="{{ url('img/logo-white.png') }}" class="logo-white" />
                    </a>

                    {{-- <div id="google_translate_element"></div>
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                              new google.translate.TranslateElement({pageLanguage: 'es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
                            }
                        </script>
                        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                    </div> --}}

                <div class="collapse navbar-collapse" id="app-navbar-collapse">

                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    {{-- <ul class="nav navbar-nav navbar-left">
                            <li><a href="{{ url('publicar') }}">¡Publicar!</a></li>
                            <li><a href="{{ url('solicitar') }}">¡Solicitar propiedad!</a></li>

                    </ul> --}}

                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::guest())
                            <li><a href="{{ url('/')}}">Inicio</a></li>
                            <li><a href="{{ url('/quienes-somos')}}">Quiénes Somos</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Propiedades <span class="caret"></span></a>

                                <ul class="dropdown-menu" role="menu">
                                    @foreach($property_types_menu as $link )
                                        <li><a href="{{ url('propiedades/'.$link->slug)}}" >{{ $link->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a href="{{ url('contactar') }}" class="formulario" >Contactar</a></li>
                            {{-- <li><a href="#publicar" class="formulario" data-toggle="modal" data-selection="publicar" >Publicar Propiedad</a></li> --}}
                            {{-- <li><a href="#solicitar" class="formulario" data-toggle="modal" data-selection="solicitar" >Solicitar Propiedad</a></li> --}}
                            {{-- <li><a href="#contactar" class="formulario" data-toggle="modal" data-selection="contactar" >Contactar Agente</a></li> --}}
                            <li><a href="{{ url('login') }}">Iniciar Sesión</a></li>
                        @else
                            <li><a href="{{ url('/')}}">Inicio</a></li>
                            <li><a href="{{ url('/quienes-somos')}}">Quiénes Somos</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Propiedades <span class="caret"></span></a>

                                <ul class="dropdown-menu" role="menu">
                                    @foreach($property_types_menu as $link )
                                        <li><a href="{{ url('propiedades/'.$link->slug) }}" >{{ $link->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a href="{{ url('contactar') }}" class="formulario" >Contactar</a></li>
                            {{-- <li><a href="#publicar" class="formulario" data-toggle="modal" data-selection="publicar" >Publicar Propiedad</a></li> --}}
                            {{-- <li><a href="#solicitar" class="formulario" data-toggle="modal" data-selection="solicitar" >Solicitar Propiedad</a></li> --}}
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ ucWords(Auth::user()->name) }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">

                                    <li><a href="{{ url('ControlPanel')}}" >Acceder al Sistema</a></li>
                                    <li><a href="{{ url('ControlPanel/administradores/'.Auth::id().'/edit' )}}" >Mi Perfil</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        <li id="google_translate_element">
                             <script type="text/javascript">
                                function googleTranslateElementInit() {
                                  new google.translate.TranslateElement({pageLanguage: 'es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
                                }
                            </script>
                            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    @yield('content')

    <footer>
        <div class="container">
            <div class="row">

                <div class="col-sm-2">
                    <h4>Menú</h4>
                    <ul class="links">
                        <li><a href="{{ url('/quienes-somos') }}" class="white-link" >Quiénes Somos</a></li>
                        <li><a href="{{ url('/propiedades') }}" class="white-link" >Servicios</a></li>
                        <li><a href="{{ url('propiedades-destacadas') }}" class="white-link" >Propiedades Destacadas</a></li>
                        <li><a href="{{ url('propiedades-en-ganga') }}" class="white-link" >Gangas</a></li>
                        <li><a href="#publicar" class="white-link" data-toggle="modal" data-selection="publicar" >Publicar Propiedad</a></li>
                        <li><a href="#solicitar" class="white-link" data-toggle="modal" data-selection="solicitar" >Solicitar Propiedad</a></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <h4>Somos Momotombo Real Estate</h4>
                    <p>Momotombo Real Estate inició en Nicaragua en el año 2005 con el compromiso de contribuir al desarrollo de nuestro país ofreciendo al mercado de bienes raíces una forma diferente de ofertar y adquirir propiedades. Somos una empresa dinámica y ansiosa de retos que ofrece a nuestros clientes la mejor opción en el mercado de bienes inmuebles.</p>
                </div>
                <div class="col-sm-4">
                    <h4>Contáctanos</h4>
                    <p>Planes de Altamira, Casa #263<br>Del portón principal de Claro Villa Fontana, 1 1/2 cuadras al lago.<br>Managua, Nicaragua.</p>
                    <span class=""><b>Email:</b> <a href="mailto:info@momotomborealestate.com">info@momotomborealestate.com</a></span><br>
                    <span class=""><b>Teléfonos:</b>  <a href="tel:+50522708177">(505) 2270-8177</a> | <a href="tel:+50588834271">(505) 8883-4271</a> | <a href="tel:+13058108364">(305) 810-8364</a></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <a href="https://www.facebook.com/MomotomboRealEstate/" class="social-link" ><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/MomotomboRealE" class="social-link" ><i class="fa fa-twitter"></i></a>
                    <a href="https://www.instagram.com/momotomborealestate_ni/" class="social-link" ><i class="fa fa-instagram"></i></a>
                    {{-- <a href="#" class="social-link" ><i class="fa fa-pinterest-p"></i></a>   --}}
                </div>
                <div class="col-sm-6 power-by">
                    &copy; 2018 Momotombo Real Estate. Todos los derechos reservados | Creado por CAITE505
                </div>
            </div>
        </div>
    </footer>

    <div id="publicar" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Desea publicar una propiedad o inmueble con nuestro sitio web?</h4>
                </div>
                <div class="modal-body">

                    <div class="row" style="padding: 0 0 10px;">
                        <div class="col-sm-12">
                            <p>Si tiene una propiedad que desee vender o rentar, contactenos llenando el presente formulario, un agente en pocos momentos se contactará con usted.<br><br>Promocione su propiedad con la mejor agencia de bienes raices de Nicaragua</p>
                            {!! Form::open(['url' => 'request-information', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
                            <input type="hidden" name="message_type" value="Desea publicar un inmueble">
                                <div class="col-sm-6 pull-left">
                                    <label class="control-label">Nombre completo *</label>
                                    <div class="">
                                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'¿Cuál es su nombre?', 'required'=>'required']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <label class="control-label">Correo electrónico *</label>
                                    <div class="">
                                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder'=>'¿Cuál es su correo electrónico?', 'required'=>'required']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <label class="control-label">Celular *</label>
                                    <div class="">
                                        {!! Form::text('phone', null, ['class' => 'form-control phone_number', 'placeholder'=>'¿Cuál es su celular?', 'required'=>'required']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <label class="control-label">Tipo de propiedad</label>
                                    <div class="">
                                        {!! Form::select('property_id',$property_types, null, ['class' => 'form-control', 'placeholder'=>'Seleccionar', 'required'=>'required']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    {!! Form::label('description', 'Escribanos una breve descripción * ', ['class' => ' control-label']) !!}
                                    <div class="">
                                        {!! Form::textArea('description', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-right">
                                    {!! Form::submit('Enviar información', ['class' => 'btn btn-primary form-control ']) !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>

            </div> {{-- modal-content --}}
        </div> {{-- modal-dialog --}}
    </div> {{-- modal property --}}

    <div id="solicitar" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Qué tipo de propiedad desea comprar o rentar?</h4>
                </div>
                <div class="modal-body">

                    <div class="row" style="padding: 0 0 10px;">
                        <div class="col-sm-12">
                            <p>Si desea comprar o rentar un inmueble, contactenos llenando el presente formulario, un agente en pocos momentos se contactará con usted.<br><br>Promocione su propiedad con la mejor agencia de bienes raices de Nicaragua</p>
                            {!! Form::open(['url' => 'request-information', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
                            <input type="hidden" name="message_type" value="Deseo comprar o rentar un inmueble">
                                <div class="col-sm-6 pull-left">
                                    <label class="control-label">Nombre completo *</label>
                                    <div class="">
                                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'¿Cuál es su nombre?', 'required'=>'required']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <label class="control-label">Correo electrónico *</label>
                                    <div class="">
                                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder'=>'¿Cuál es su correo electrónico?', 'required'=>'required']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <label class="control-label">Celular *</label>
                                    <div class="">
                                        {!! Form::text('phone', null, ['class' => 'form-control phone_number', 'placeholder'=>'¿Cuál es su celular?', 'required'=>'required']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <label class="control-label">Tipo de propiedad</label>
                                    <div class="">
                                        {!! Form::select('property_id',$property_types, null, ['class' => 'form-control', 'placeholder'=>'Seleccionar', 'required'=>'required']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    {!! Form::label('description', '¿Con cuales características busca el inmueble? * ', ['class' => ' control-label']) !!}
                                    <div class="">
                                        {!! Form::textArea('description', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'']) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-right">
                                    {!! Form::submit('Solicitar información', ['class' => 'btn btn-primary form-control ']) !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>

            </div> {{-- modal-content --}}
        </div> {{-- modal-dialog --}}
    </div> {{-- modal property --}}

    @if(Session::has('success_message'))
        <script type="text/javascript">swal("Gracias por contactarnos", {!! Session::get('success_message') !!} , "success");</script>
    @endif

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>


    @yield('script')

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>



     <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5a9733e3d7591465c708210b/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</body>
</html>
