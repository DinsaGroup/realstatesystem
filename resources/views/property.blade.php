@extends('layouts.frontend')

@section('head')
@endsection

@section('content')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI43-yj5Hf3fZo8eyIrWULm_VGwbXZecs&libraries=places"  type="text/javascript"></script>
    <section id="banner" class="banner-mobil" style="background-image:url({{ url('img/slider/b20.jpg') }});
                			background-size: cover;
        					background-position: 50% 50%;
        					width: 100%;
        					margin-top: 50px">
        <div class="banner result">
            @include('frontend.search')
        </div>
    </section>

    <section style="margin-top: 50px;">
    	<div class="container">
            <div class="row">

                <div class="col-sm-12 text-center">
                    <h1><b>{{ $property->property_type->name }} en {{ ucfirst( $property->ventaRenta ) }}
                        {{-- {{ ucfirst( $property->sector ) }} --}}
                        <?php
                            // Preformar el sector
                            $sector = explode('/',$property->sector);
                            $cadena = explode(' ',$sector[0]);
                            $string = '';

                            for($i=0;$i<count($cadena);$i++){

                                if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                    $string .= $cadena[$i].' ';
                                }else{
                                    $string .= ucfirst($cadena[$i]).' ';
                                }

                            }
                            echo trim($string).', ';

                            // Preformar la ciudad
                            $sector = explode('/',$property->city);
                            $cadena = explode(' ',$sector[0]);
                            $string = '';

                            for($i=0;$i<count($cadena);$i++){

                                if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                    $string .= $cadena[$i].' ';
                                }else{
                                    $string .= ucfirst($cadena[$i]).' ';
                                }

                            }
                            echo trim($string);
                        ?>
                        {{-- {{ ucfirst($property->city ) }} --}}
                      </b></h1>
                    <span class="codigo">Código: <b>{{ $property->code }}</b> </span>
                    @if($property->ventaRenta == 'venta')
                        <span class="precio" style="margin-left: 20px">Precio de venta : <b>U$ {{ number_format($property->precioVenta,2,'.',',') }}</b></span>
                    @endif
                    @if($property->ventaRenta == 'renta')
                        <span class="precio" style="margin-left: 20px">Precio de renta : <b>U$ {{ number_format($property->precioAlquiler,2,'.',',') }}</b></span>
                    @endif
                    @if($property->ventaRenta == 'venta/renta')
                        <span class="precio" style="margin-left: 20px">Precio de venta : <b>U$ {{ number_format($property->precioVenta,2,'.',',') }}</b></span>
                        <span class="precio" style="margin-left: 20px">Precio de renta : <b>U$ {{ number_format($property->precioAlquiler,2,'.',',') }}</b></span>
                    @endif
                </div>

                

            </div>

    		<div class="row">
                <div class="col-sm-9 noPadding">

                    @if($show) {{-- Si hay fotos para mostrar --}}
                        <?php $count=0; ?>
                        {{-- <h4 class="photo_title">Fotos de la Propiedad</h4> --}}

                        <div class="col-sm-12">
                            <div class="row" style="padding: 0 10px">
                                <div id="photoCarousel" class="carousel slide" data-ride="carousel">

                                    <div class="carousel-inner">
                                        @foreach($property->photos as $photo)
                                            @if($photo->visible=='true')
                                                <?php if($count==0){ echo '<div class="item active">'; $count++; }else{ echo '<div class="item">';} ?>
                                                    <img src="{{ url('img/properties/'.$photo->filename) }}" width="100%" />
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#photoCarousel" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#photoCarousel" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                
                                </div>
                            </div>
                        </div>

                    @endif

                    <div class="col-sm-12" style="margin: 30px 0">
                        <p>{{ $description }}</p>
                    </div>

                    <div class="col-sm-12">
                        <h3>Características</h3>
                    </div>

                    @foreach($property->features as $item)
                        <div class="col-sm-3 feature">
                            <i class="fa {{ $item->icon }}"></i> {{ $item->name }}
                            @foreach($valor as $item2)
                                @if($item->id == $item2->feature_id && $property->id == $item2->property_id)
                                    ({{ $item2->valor }})
                                @endif()
                            @endforeach
                        </div>
                    @endforeach

                    <div class="col-sm-12">
                        <h3>Localización</h3>
                        <p>

                            <?php
                            // Preformar el sector
                            $sector = explode('/',$property->sector);
                            $cadena = explode(' ',$sector[0]);
                            $string = '';

                            for($i=0;$i<count($cadena);$i++){

                                if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                    $string .= $cadena[$i].' ';
                                }else{
                                    $string .= ucfirst($cadena[$i]).' ';
                                }

                            }
                            echo trim($string).', ';

                            // Preformar la ciudad
                            $sector = explode('/',$property->city);
                            $cadena = explode(' ',$sector[0]);
                            $string = '';

                            for($i=0;$i<count($cadena);$i++){

                                if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                    $string .= $cadena[$i].' ';
                                }else{
                                    $string .= ucfirst($cadena[$i]).' ';
                                }

                            }
                            echo trim($string).', ';
                        ?>
                            {{-- {{ ucfirst($property->sector) }} {{ ucfirst($property->city) }},  --}}
                            {{ ucfirst($property->country) }}
                        </p>
                        <div id="map" style="height: 330px;"></div>
                        <input id="lat" class="lat" type="hidden" name="latitud" value="{{ $property->latitude }}">
                        <input id="lng" class="lng" type="hidden" name="longitud" value="{{ $property->longitude }}">
                    </div>
                </div>

                {{-- agente --}}
                <div class="col-sm-3 agent noPadding">
                    <div class="testimonials">
                        <span class="title">CONTACTAR AGENTE</span>
                        <div class="photo">
                            <img src="{{ url('img/user/'.$property->agent->photo ) }}">
                        </div>
                        <span class="name">{{ $property->agent->name }}</span>
                        {!! Form::open(['url' => 'email', 'class' => 'form-horizontal', 'method'=>'POST']) !!}
                        {{ csrf_field() }}
                            <input type="hidden" name="agent_id" value="{{ $property->agent->id }}" >
                            <input type="hidden" name="property_code" value="{{ $property->code}}" >
                            <div class="form-group">
                                <div class="col-md-10 col-sm-offset-1">
                                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Nombre completo / Full name']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-sm-offset-1">
                                    {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Correo electrónico / Email']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-sm-offset-1">
                                    {!! Form::text('phone', null, ['class' => 'form-control phone_number', 'required' => 'required', 'placeholder'=>'Teléfono / Phone']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-sm-offset-1">
                                    {!! Form::textarea('message', null, ['class' => 'form-control','rows'=>3, 'placeholder'=>'Escribir un mensaje / Leave a message']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    {!! Form::submit('Enviar mensaje', ['class' => 'btn btn-primary form-control create']) !!}
                                </div>
                            </div>


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="title-section">
                        <h2>PROPIEDADES SIMILARES</h2>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 well">
                            <div class="">
                                <div id="similarCarousel" class="carousel slide">
                                    <div class="carousel-inner">
                                        <?php $count=0; $group=0; ?>
                                        @foreach($similars as $item)
                                        @if($count<1)
                                            <div class="item active">
                                                <div class="row-fluid">
                                            <?php $count++; ?>
                                        @else
                                            @if($group>=4)
                                            <div class="item ">
                                                <div class="row-fluid">
                                                <?php $group=0; ?>
                                            @endif
                                        @endif
                                                <a href="{{ url('propiedad/'.$item->code ) }}" class="show_now">
                                                <div class="col-sm-3">
                                                    <div class="property similars">

                                                        <div class="image">
                                                            @if(array_get($item->photos,'0.filename'))
                                                                <img src="{{ url('img/properties/'. array_get($item->photos,'0.filename') ) }}" class="photo" />
                                                            @else
                                                                <img src="{{ url('img/properties/no-img.png')  }}" class="photo" />
                                                            @endif
                                                        </div>

                                                        <span class="title">{{ $item->property_type->name }} en {{ ucfirst($item->ventaRenta) }}<br> {{ ucWords($item->sector) }}</span>
                                                        <span class="price">
                                                            @if($item->ventaRenta == 'venta')
                                                                <span class="pull-left">PV.: U$ {{ number_format($item->precioVenta,2,'.',',') }}</span>
                                                            @endif
                                                            @if($item->ventaRenta == 'renta')
                                                                <span class="pull-left">PR.: U$ {{ number_format($item->precioAlquiler,2,'.',',') }}</span>
                                                            @endif
                                                            @if($item->ventaRenta == 'venta/renta')
                                                                <span class="pull-left">PV.: U$ {{ number_format($item->precioVenta,2,'.',',') }}</span>
                                                                <span class="pull-right">PR.: U$ {{ number_format($item->precioAlquiler,2,'.',',') }}</span>
                                                            @endif
                                                        </span>

                                                        {{-- <a href="{{ url('propiedad/'.$item->code ) }}" class="show_now">Ver ahora</a> --}}
                                                    </div>
                                                </div>
                                                </a>
                                            <?php $group++; ?>

                                            @if($group>=4)
                                                </div>
                                            </div>
                                            @endif


                                        @endforeach
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#similarCarousel" data-slide="prev"><i class="fa fa-chevron-circle-left" style="margin-top: 125px"></i></a>
                                <a class="right carousel-control" href="#similarCarousel" data-slide="next"><i class="fa fa-chevron-circle-right" style="margin-top: 125px"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

    	</div>
    </section>

    <div id="full-image" style="display: none">
        <a href="#" class="close-img"><i class="fa fa-times"></i></a>
        <div class="marco">
            <img src="" class="full-img" />
        </div>
    </div>

@endsection

@section('script')

	<script>
		$(document).ready(function(){



	        $('a.close-img').click(function(e){
	                e.preventDefault();
	                $('#full-image').fadeOut('slow');
	        });

            var la = $('.lat').val();
            var lo = $('.lng').val();

            var lat = parseFloat(la);
            var lng = parseFloat(lo);

            var myLatlng = {lat: lat, lng: lng};
            console.log(myLatlng);

            var map = new google.maps.Map(document.getElementById('map'), {
              center: myLatlng,
              zoom: 12
            });

            var image = '{{ url('/img/marker-ping.png') }}';
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable: false,
                animation: google.maps.Animation.DROP,
                icon: image,
            });

            var option = {minZoom:8, maxZoom: 16};
            map.setOptions(option);

			$('#myCarousel').carousel({
                interval: 10000,
            })
            $('#simmilarCarousel').carousel({
                interval: 10000,
            })

            $('.photo-detail').click(function(){
                $('#full-image').fadeIn('slow');
                $('#full-image div img').attr('src',$(this).attr('src'));

            });
		});

	</script>

@endsection
