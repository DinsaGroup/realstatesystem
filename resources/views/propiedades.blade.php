@extends('layouts.frontend')

@section('head')
@endsection

@section('content')

	<section id="banner" class="banner-mobil" style="background-image:url({{ url('img/slider/b20.jpg') }});
                			background-size: cover;
        					background-position: 50% 50%;
        					width: 100%;
        					margin-top: 50px">
        <div class="banner result">
            @include('frontend.search')
        </div>
    </section>

    <section>
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
    				
                        <h3>{{ $title }}</h3>
                        <p>¡Felicidades! ¡Hemos encontrado ciertos inmuebles que pueden ser de tu interes!</p>
                    
    			</div>
    		</div>
    	</div>
    </section>

@endsection
@section('script')
@endsection