@extends('layouts.frontend')

@section('head')
@endsection

@section('content')


    <section id="banner" class="banner-mobil" style="height: 300px; background-image:url({{ url('img/slider/b20.jpg') }});
                			background-size: cover;
        					background-position: 50% 50%;
        					width: 100%;
        					margin-top: 50px">
        <div class="banner result">
            <h2 class="title-search">Buscar inmueble</h2>
            <button>
                <i class="fa fa-angle-down"></i>
            </button>
            @include('frontend.search')
        </div>
    </section>

    <section id='lastproperties2'>
    	<div class="container" style="width: 96%">
    		
    		<div class="row">
    			<div class="col-sm-12 noPadding">
                    

                    {{-- @if(count($properties)>0) --}}

                        <div class="col-sm-6">
                            <h3>{{ $title }}</h3>
                            <p>¡Felicidades! ¡Hemos encontrado ciertos inmuebles que pueden ser de tu interes!</p>
                        </div>
                       

                        <div class="col-sm-12 noPadding pull-left">
                        
                            @foreach($properties as $item)
                            <a href="{{ url('propiedad/'.$item->code ) }}" class="show_now">
                                <div class="col-sm-2 box-property {{ $item->property_type->name }} {{ $item->ventaRenta }}" data-type="{{ $item->ventaRenta }}">
                                    <div class="featured">

                                        <div class="image">
                                            @if(count($item->photos))
                                                <img src="{{ url('img/properties/'. array_get($item->photos,'0.filename') ) }}" class="photo" />
                                            @else
                                                <img src="{{ url('img/properties/no-img.png') }}" class="photo" />
                                            @endif
                                        </div>

                                        <span class="title">{{ $item->property_type->name }} en {{ ucfirst($item->ventaRenta) }}<br>
                                            <?php
                                                $sector = explode('/',$item->sector);
                                                $cadena = explode(' ',$sector[0]);
                                                $string = '';

                                                for($i=0;$i<count($cadena);$i++){

                                                    if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                                        $string .= $cadena[$i].' ';
                                                    }else{
                                                        $string .= ucfirst($cadena[$i]).' ';
                                                    }

                                                }
                                                echo trim($string);
                                            ?>
                                            {{-- {{ ucwords($sector[0]) }}<br>{{ $item->code }}</span> --}}
                                            <br>{{ $item->code }}</span>
                                        <span class="price">
                                            @if($item->ventaRenta == 'venta')
                                                PV. U$ {{ number_format($item->precioVenta,2,'.',',') }}
                                            @endif
                                            @if($item->ventaRenta == 'renta')
                                               PR. U$ {{ number_format($item->precioAlquiler,2,'.',',') }}
                                            @endif
                                            @if($item->ventaRenta == 'venta/renta')
                                                <span class="pull-left">PV. U$ {{ number_format($item->precioVenta,2,'.',',') }}</span>
                                                <span class="pull-right">PR. U$ {{ number_format($item->precioAlquiler,2,'.',',') }}</span>
                                            @endif

                                        </span>
                                    </div>
                                </a>
                            </div>
                            @endforeach

                        </div>
                        <div class="col-sm-12 pull-left" >
                            {{ $properties->links() }}
                        </div>

                        

    			</div>

    		</div>	
    	</div>
    </section>
    <section class="footer_padding"></section>

<div id="full-image" style="display: none">
    <a href="#" class="close-img"><i class="fa fa-times"></i></a>
    <div class="marco">
        <img src="" class="full-img" />
    </div>
</div>

@endsection

@section('script')
    <script src="{{{ asset('js/main.js') }}}" type="text/javascript"></script>
@endsection
