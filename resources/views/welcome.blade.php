@extends('layouts.frontend')

@section('head')
@endsection

@section('content')

	<div id="myCarousel" class="carousel slide" data-ride="carousel">

        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <div style="background-image:url({{ url('img/slider/b12.jpg') }});
                			background-size: cover;
        					background-position: 50% 50%;
        					width: 100%;
        					height: 75vh;" /></div>
            </div>
            <div class="item">
                <div style="background-image:url({{ url('img/slider/b15.jpg') }});
                			background-size: cover;
        					background-position: 50% 50%;
        					width: 100%;
        					height: 75vh;" /></div>
            </div>
            <div class="item">
                <div style="background-image:url({{ url('img/slider/b14.jpg') }});
                            background-size: cover;
                            background-position: 50% 50%;
                            width: 100%;
                            height: 75vh;" /></div>
            </div>
            <div class="item">
                <div style="background-image:url({{ url('img/slider/b20.jpg') }});
                            background-size: cover;
                            background-position: 50% 50%;
                            width: 100%;
                            height: 75vh;" /></div>
            </div>
            <div class="item">
                <div style="background-image:url({{ url('img/slider/b21.jpg') }});
                            background-size: cover;
                            background-position: 50% 50%;
                            width: 100%;
                            height: 75vh;" /></div>
            </div>

        </div>
    </div>

    <section id="banner">
			<div class="title-search">
				<h1>Disfruta de Nicaragua</h1>
				<h2>Encuentra la Propiedad de Tus Sueños Hoy</h2>
			</div>
        <div class="banner">
            @include('frontend.search')
        </div>
    </section>

    <section id='lastproperties'>
    	<div class="container fullscreen">
			<div class="hero-msg row">
				<h2 class="title-hero">Somos Expertos en Bienes Raíces</h2>
				<p class="desc-hero">Momotombo Real Estate es la mejor opción en venta y alquiler de inmuebles en toda Nicaragua. <br>Con más de 6,300 propiedades en lista y más de 12 años de experiencia, te mostraremos todo lo que la tierra de lagos y volcanes tiene para ofrecer. <a href="https://www.facebook.com/MomotomboRealEstate/" target="_blank">#EsUnTrato</a></p>
			</div>

            

    		<div class="row">
    			<div class="col-sm-12">
    				<div class="title-section">
	    				<h3>Propiedades Destacadas</h3>
    				</div>
    			</div>

                @if($featured_list)

        			@foreach($featured as $item)
                    <div class="col-sm-3" style="float: left;">
                        <a href="{{ url('propiedad/'.$item->property->code ) }}" class="show_now">
            				<div class="property">

        	    				<div class="image">
        		    				<img src="{{ url('img/properties/'. array_get($item->property->photos,'0.filename') ) }}" class="photo" />
        	    				</div>

        	    				<span class="title">{{ ucfirst($item->property->property_type->name) }} en {{ ucfirst($item->property->ventaRenta) }}
                                    {{-- {{ ucfirst($item->property->sector) }}, <br>{{ ucfirst($item->property->city) }} --}}
                                    <?php
                                        // Preformar el sector
                                        $sector = explode('/',$item->property->sector);
                                        $cadena = explode(' ',$sector[0]);
                                        $string = '';

                                        for($i=0;$i<count($cadena);$i++){

                                            if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                                $string .= $cadena[$i].' ';
                                            }else{
                                                $string .= ucfirst($cadena[$i]).' ';
                                            }

                                        }
                                        echo trim($string).', ';

                                        // Preformar la ciudad
                                        $sector = explode('/',$item->property->city);
                                        $cadena = explode(' ',$sector[0]);
                                        $string = '';

                                        for($i=0;$i<count($cadena);$i++){

                                            if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                                $string .= $cadena[$i].' ';
                                            }else{
                                                $string .= ucfirst($cadena[$i]).' ';
                                            }

                                        }
                                        echo trim($string);
                                    ?>
                                </span>
        	    				<span class="price">
                                    @if($item->property->ventaRenta == 'venta')
                                        <span class="pull-left"> PV.: U$ {{ number_format($item->property->precioVenta,2,'.',',') }}</span>
                                    @endif
                                    @if($item->property->ventaRenta == 'renta')
                                        <span class="pull-left"> PR.: U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }}</span>
                                    @endif
                                    @if($item->property->ventaRenta == 'venta/renta')
                                        <span class="pull-left"> PV.: U$ {{ number_format($item->property->precioVenta,2,'.',',') }}</span>
                                        <span class="pull-left"> PR.: U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }}</span>
                                    @endif

                                </span>
        	    				{{-- <a href="{{ url('propiedad/'.$item->property->code ) }}" class="show_now">Ver ahora</a> --}}
        	    			</div>
                        </a>
                    </div>
        			@endforeach

                @else

                    @foreach($featured as $item)
                    <div class="col-sm-3" style="float: left;">
                        <div class="property">

                            <div class="image">
                                <img src="{{ url('img/properties/'. array_get($item->photos,'0.filename') ) }}" class="photo" />
                            </div>

                            <span class="title">{{ ucfirst($item->property_type->name) }} en {{ ucfirst($item->ventaRenta)}}
                                {{-- {{ ucfirst($item->sector) }}, <br>{{ ucfirst($item->city) }} --}}
                                <?php
                                    // Preformar el sector
                                    $sector = explode('/',$item->sector);
                                    $cadena = explode(' ',$sector[0]);
                                    $string = '';

                                    for($i=0;$i<count($cadena);$i++){

                                        if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                            $string .= $cadena[$i].' ';
                                        }else{
                                            $string .= ucfirst($cadena[$i]).' ';
                                        }

                                    }
                                    echo trim($string).', ';

                                    // Preformar la ciudad
                                    $sector = explode('/',$item->city);
                                    $cadena = explode(' ',$sector[0]);
                                    $string = '';

                                    for($i=0;$i<count($cadena);$i++){

                                        if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                            $string .= $cadena[$i].' ';
                                        }else{
                                            $string .= ucfirst($cadena[$i]).' ';
                                        }

                                    }
                                    echo trim($string);
                                ?>
                            </span>
                            <span class="price">
                                @if($item->ventaRenta == 'venta')
                                    U$ {{ number_format($item->precioVenta,2,'.',',') }}
                                @endif
                                @if($item->ventaRenta == 'renta')
                                    U$ {{ number_format($item->precioAlquiler,2,'.',',') }}
                                @endif
                                @if($item->ventaRenta == 'venta/renta')
                                    U$ {{ number_format($item->precioVenta,2,'.',',') }}
                                @endif

                            </span>
                            <a href="{{ url('propiedad/'.$item->code ) }}" class="show_now">Ver ahora</a>
                        </div>
                    </div>
                    @endforeach

                @endif

                <div class="col-sm-12">
                    <a href="{{ url('propiedades-destacadas') }}" class="btn btn-link link" >
                        <i class="fa fa-plus"></i> Ver todas las propiedades destacadas
                    </a>

                </div>
    		</div>

    		<div class="row">

                @if($testimonials) <div class="col-sm-8"> @else <div class="col-sm-12"> @endif
                
    				<div class="gold-property">
	    				<div class="col-sm-12 noPadding">
	    					<h3 class="title">¡Ganga, Ganga!
                            <a href="{{ url('propiedades-en-ganga') }}" class="btn btn-link pull-right" >
                                <i class="fa fa-plus"></i> Ver más
                            </a>
                            </h3>
	    				</div>

	    				<div class="detail">

                        @if($gold_list)

                            @foreach($gold as $item)

                                <div class="col-sm-8 noPadding image">
                                    <img src="{{ url('img/properties/'.array_get($item->property->photos,'0.filename') ) }}" />
                                </div>

                                <div class="col-sm-4">
                                    <h3>Excelente {{ ucfirst($item->property->property_type->name) }} en {{ ucfirst($item->property->ventaRenta) }} <br>
                                        {{-- {{ ucWords($item->property->sector) }}, {{ ucfirst($item->property->city) }}  --}}
                                        <?php
                                            // Preformar el sector
                                            $sector = explode('/',$item->property->sector);
                                            $cadena = explode(' ',$sector[0]);
                                            $string = '';

                                            for($i=0;$i<count($cadena);$i++){

                                                if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                                    $string .= $cadena[$i].' ';
                                                }else{
                                                    $string .= ucfirst($cadena[$i]).' ';
                                                }

                                            }
                                            echo trim($string).', ';

                                            // Preformar la ciudad
                                            $sector = explode('/',$item->property->city);
                                            $cadena = explode(' ',$sector[0]);
                                            $string = '';

                                            for($i=0;$i<count($cadena);$i++){

                                                if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                                    $string .= $cadena[$i].' ';
                                                }else{
                                                    $string .= ucfirst($cadena[$i]).' ';
                                                }

                                            }
                                            echo trim($string);
                                        ?>

                                    </h3>
                                    <span class="id">CODIGO: # {{ $item->property->code }} </span>
                                    <span class="item"><i class="fa fa-bath"></i> 2+1 (Servicio)</span>
                                    <span class="item"><i class="fa fa-video-camera"></i> Vigilancia 24/7</span>
                                    <span class="item"><i class="fa fa-shield"></i> Muro perimetral</span>
                                    <span class="item"><i class="fa fa-home"></i> Recien construida</span>
                                    <span class="item"><i class="fa fa-snowflake-o"></i> AA Central</span>
                                    <span class="item"><i class="fa fa-building"></i> 2 Plantas</span>

                                        @if($item->property->ventaRenta == 'venta')
                                            <span class="item price"><i class="fa fa-usd"></i> {{ number_format($item->soldPrice,2,'.',',') }}</span>
                                            <span class="before">Antes</span> <span class="underline">U$ {{ number_format($item->property->precioVenta,2,'.',',') }} </span>
                                        @endif
                                        @if($item->property->ventaRenta == 'renta')
                                            <span class="item price">U$ {{ number_format($item->rentPrice,2,'.',',') }}</span>
                                            <span class="before">Antes</span> <span class="underline">U$ {{ number_format($item->property->precioAlquiler,2,'.',',') }} </span>
                                        @endif
                                        @if($item->property->ventaRenta == 'venta/renta')
                                            <span class="item price">U$ {{ number_format($item->soldPrice,2,'.',',') }}</span>
                                            <span class="before">Antes</span> <span class="underline">U$ {{ number_format($item->property->precioVenta,2,'.',',') }} </span>
                                        @endif

                                    </span>
                                    <div class="col-sm-12">
                                        <a href="{{ url('propiedad/'.$item->property->code ) }}" class="show_now">Ver ahora</a>

                                    </div>
                                </div>

                            @endforeach

                        @else

                            @foreach($gold as $item)

                                <div class="col-sm-8 noPadding image">
                                    <img src="{{ url('img/properties/'.array_get($item->photos,'0.filename') ) }}" />
                                </div>

                                <div class="col-sm-4">
                                    <h3>Excelente {{ ucfirst($item->property_type->name) }} en {{ ucfirst($item->ventaRenta) }} <br>
                                        {{-- {{ ucfirst($item->sector) }}, {{ ucfirst($item->city) }}  --}}
                                        <?php
                                            // Preformar el sector
                                            $sector = explode('/',$item->sector);
                                            $cadena = explode(' ',$sector[0]);
                                            $string = '';

                                            for($i=0;$i<count($cadena);$i++){

                                                if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                                    $string .= $cadena[$i].' ';
                                                }else{
                                                    $string .= ucfirst($cadena[$i]).' ';
                                                }

                                            }
                                            echo trim($string).', ';

                                            // Preformar la ciudad
                                            $sector = explode('/',$item->city);
                                            $cadena = explode(' ',$sector[0]);
                                            $string = '';

                                            for($i=0;$i<count($cadena);$i++){

                                                if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                                    $string .= $cadena[$i].' ';
                                                }else{
                                                    $string .= ucfirst($cadena[$i]).' ';
                                                }

                                            }
                                            echo trim($string);
                                        ?>
                                    </h3>
                                    <span class="id">CODIGO: # {{ $item->code }} </span>
                                    <span class="item"><i class="fa fa-bath"></i> 2+1 (Servicio)</span>
                                    <span class="item"><i class="fa fa-video-camera"></i> Vigilancia 24/7</span>
                                    <span class="item"><i class="fa fa-shield"></i> Muro perimetral</span>
                                    <span class="item"><i class="fa fa-home"></i> Recien construida</span>
                                    <span class="item"><i class="fa fa-snowflake-o"></i> AA Central</span>
                                    <span class="item"><i class="fa fa-building"></i> 2 Plantas</span>

                                        @if($item->ventaRenta == 'venta')
                                            <span class="item price"><i class="fa fa-usd"></i> {{ number_format($item->soldPrice,2,'.',',') }}</span>
                                            <span class="before">Antes</span> <span class="underline">U$ {{ number_format($item->precioVenta,2,'.',',') }} </span>
                                        @endif
                                        @if($item->ventaRenta == 'renta')
                                            <span class="item price">U$ {{ number_format($item->rentPrice,2,'.',',') }}</span>
                                            <span class="before">Antes</span> <span class="underline">U$ {{ number_format($item->precioAlquiler,2,'.',',') }} </span>
                                        @endif
                                        @if($item->ventaRenta == 'venta/renta')
                                            <span class="item price">U$ {{ number_format($item->soldPrice,2,'.',',') }}</span>
                                            <span class="before">Antes</span> <span class="underline">U$ {{ number_format($item->precioVenta,2,'.',',') }} </span>
                                        @endif

                                    </span>
                                    <div class="col-sm-12">
                                        <a href="{{ url('propiedad/'.$item->code ) }}" class="show_now">Ver ahora</a>

                                    </div>
                                </div>

                            @endforeach

                        @endif

		    			</div>

	    			</div>
    			</div>

                @if($testimonials)
    			<div class="col-sm-4">
    				<div class="testimonials">
	    				<span class="title">testimonios</span>
                        @foreach($testimonials as $item)
    	    				<div class="photo">
    		    				<img src="{{ url('img/user/default.jpg') }}">
    	    				</div>
    	    				<span class="name">{{ $item->user->name }}</span>
    	    				<span class="text">{{ $item->notes }}</span>
                        @endforeach
	    			</div>
    			</div>
                @endif
    		</div>

            <div class="row lastproperties">

                <div class="col-sm-12">
                    <div class="title-section">
                        <h3>Últimas Propiedades Publicadas</h3>
                    </div>
                </div>

                @foreach($last_properties as $item)

                    <div class="col-sm-3" style="float: left;">
                        <a href="{{ url('propiedad/'.$item->code ) }}" >

                        <div class="property">

                            <div class="image">
                                <img src="{{ url('img/properties/'. array_get($item->photos,'0.filename') ) }}" class="photo" />
                            </div>

                            <span class="title">{{ ucfirst($item->property_type->name) }} en {{ ucfirst($item->ventaRenta) }} <br>
                                {{-- {{ ucwords($item->sector) }}, <br>{{ ucfirst($item->city) }} --}}
                                <?php
                                    // Preformar el sector
                                    $sector = explode('/',$item->sector);
                                    $cadena = explode(' ',$sector[0]);
                                    $string = '';

                                    for($i=0;$i<count($cadena);$i++){

                                        if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                            $string .= $cadena[$i].' ';
                                        }else{
                                            $string .= ucfirst($cadena[$i]).' ';
                                        }

                                    }
                                    echo trim($string).', ';

                                    // Preformar la ciudad
                                    $sector = explode('/',$item->city);
                                    $cadena = explode(' ',$sector[0]);
                                    $string = '';

                                    for($i=0;$i<count($cadena);$i++){

                                        if($cadena[$i]=="a" || $cadena[$i]=="de" || $cadena[$i]=="en" || $cadena[$i]=="la" || $cadena[$i]=="el" || $cadena[$i]=="del"){
                                            $string .= $cadena[$i].' ';
                                        }else{
                                            $string .= ucfirst($cadena[$i]).' ';
                                        }

                                    }
                                    echo trim($string);
                                ?>
                            </span>

                            <span class="price">
                                @if($item->ventaRenta == 'venta')
                                    <span class="pull-left">PV.: U$ {{ number_format($item->precioVenta,2,'.',',') }}</span>
                                @endif
                                @if($item->ventaRenta == 'renta')
                                    <span class="pull-left">PR.: U$ {{ number_format($item->precioAlquiler,2,'.',',') }}</span>
                                @endif
                                @if($item->ventaRenta == 'venta/renta')
                                    <span class="pull-left">PV.: U$ {{ number_format($item->precioVenta,2,'.',',') }}</span>
                                    <span class="pull-right">PR.: U$ {{ number_format($item->precioAlquiler,2,'.',',') }}</span>
                                @endif
                            </span>
                        </div>
                    </div>
                    </a>
                @endforeach
            </div>

    	</div>
    </section>

@endsection

@section('script')
@endsection
