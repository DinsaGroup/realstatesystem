<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AppController@index');

Route::get('/quienes-somos', 'AppController@aboutus');

Route::get('/propiedades', 'AppController@propiedades');

Route::get('propiedades/{slug}', 'AppController@all_propiedades');

Route::get('/sitemap', 'AppController@sitemap');

Route::get('propiedad/{code}','AppController@property');

Route::get('propiedades-destacadas','AppController@featured_property');

Route::get('propiedades-en-ganga','AppController@gold_property');

Route::post('email','AppController@email');

Route::get('contactar','AppController@contactar');

// Route::post('buscar-propiedades','AppController@find_properties');
// Route::get('buscar-propiedades','AppController@find_properties');

Route::any('filtrar-resultados','AppController@filtrar_resultado');
Route::any('buscar-propiedades','AppController@filtrar_resultado');
Route::post('request-information','AppController@request_information');
// Route::post('filtrar-resultados','AppController@filtrar_resultado');

// Route::post('buscar-propiedades','AppController@find_properties_google');
// Route::get('buscar-propiedades', 'AppController@find_properties_google');


Auth::routes();

Route::group(['prefix' => 'ControlPanel', 'middleware' => ['auth', 'role:superadmin|admin|supervisor|agent'], 'namespace' => 'controlpanel'], function() {

	Route::get('/', 'DashboardController@index');

	Route::resource('super-administradores', 'SuperAdminController');

	Route::resource('administradores', 'AdminController');

	Route::resource('supervisores', 'SupervisorController');

	Route::resource('agentes', 'AgentController');

	Route::resource('clientes', 'LandOwnerController');

	Route::resource('testimonios', 'TestimonialController');

	Route::resource('cambiar-password', 'ChangePasswordController');

	Route::resource('roles-administrativos', 'RolesController');

	Route::resource('status', 'StatusesController');

	Route::resource('tipo-de-propiedades', 'PropertyTypesController');

	Route::resource('propiedades', 'PropertiesController');

	Route::get('propiedades/create/{id}', 'PropertiesController@createWithUserID');

	Route::get('propiedad/{id}/fotos/create','PhotosController@create');

	Route::get('propiedad/despublicadas','PropertiesController@despublished');

	Route::get('propiedad/vendidas','PropertiesController@vendidas');

	Route::get('propiedad/revision','PropertiesController@checked');

	Route::get('propiedad/alquiladas','PropertiesController@alquiladas');

	Route::get('propiedad/ingresadas','PropertiesController@myproperties');

	Route::resource('caracteristicas', 'FeaturesController');

	Route::resource('propiedades-destacadas', 'FeaturedController');

	Route::resource('propiedades-oro', 'GoldPropertyController');

	Route::resource('rotulos', 'SignsController');

	Route::resource('tipo-contacto', 'LandownerTypeController');

	Route::resource('unidades-de-medida', 'MeasureUnitsController');

	Route::resource('photos', 'PhotosController');

	//Import sections
	Route::post('importcontact','ImportFilesController@contactos');

	Route::post('importusers','ImportFilesController@usuarios');

	Route::post('importproperties','ImportFilesController@propiedades');

	Route::post('importcountry','ImportFilesController@country');

	Route::post('importdepartment','ImportFilesController@department');

	Route::post('importmunicipio','ImportFilesController@municipio');

	Route::post('importbarrio','ImportFilesController@barrio');

	Route::post('importphotos','ImportFilesController@photos');

	Route::post('importsigns','ImportFilesController@signs');

	Route::post('importnotes','ImportFilesController@notes');

	Route::post('importagents','ImportFilesController@agentes');

	Route::post('importcorrecciones','ImportFilesController@correcciones');

	Route::post('import-property-user','ImportFilesController@property_user');

	Route::get('importar', 'PropertiesController@importar');

	Route::post('importcedulas','ImportFilesController@cedulas');

	// Termina import sections

	Route::get('revisar-propiedades', 'PropertiesController@notChecked');

	Route::get('googlemaps/{latitude}/{longitude}','PropertiesController@googlemaps');

	Route::get('request/check-pass', 'JsonController@checkPass');

	Route::any('email/{email}','QueryController@email');

	Route::get('request/get-idclient', 'JsonController@getIdClient');

	Route::get('request/get-client', 'JsonController@getClient');

	Route::get('request/reset-client', 'JsonController@resetUsers');

	Route::get('request/get-email', 'JsonController@getEmail');

	Route::get('request/get-property', 'JsonController@getProperty');

	Route::get('request/check-testimonial', 'JsonController@checkTestimonial');

	Route::get('request/save-testimonial', 'JsonController@saveTestimonial');

	Route::get('request/property_detail', 'JsonController@getPropertyDetail');

	Route::get('request/users','JsonController@getUsers');

	Route::get('request/properties','JsonController@getProperties_by_keyword');

	Route::get('request/property/alquilada/{id}','PropertiesController@alquilada');

	Route::get('request/property/disponible/{id}','PropertiesController@disponible');

	Route::get('request/property/nodisponible/{id}','PropertiesController@nodisponible');

	Route::get('request/property/vendida/{id}','PropertiesController@vendida');

	Route::get('request/property/revisado-publicar/{id}','PropertiesController@revisado');

	Route::get('mantenimiento','MaintenanceController@index');

	Route::get('maintenace_data/{tabla}','MaintenanceController@maintenance');

	Route::get('request/delphoto','PhotosController@deletephoto');

	// autocomplete
	Route::get('request/get-property-id','AutoCompleteController@propertyId');
	Route::get('request/get-codigo','AutoCompleteController@codigo');
	Route::get('request/get-rotulo','AutoCompleteController@rotulo');
	Route::get('request/get-categoria','AutoCompleteController@categoria');
	Route::get('request/get-sector','AutoCompleteController@sector');
	Route::get('request/get-ciudad','AutoCompleteController@ciudad');
	Route::get('request/get-departamento','AutoCompleteController@departamento');
	Route::get('request/get-contacto','AutoCompleteController@contacto');

});
